package to.done.lib.database;

/**
 * Created by dipenpradhan on 5/26/14.
 */
public abstract class DBTools {

//    private void cursorToObject(@NotNull Cursor c, @NotNull Product product) {
//
//
//        if (c.getColumnIndex("outlet_product_id") != -1) {
//            product.setOutlet_product_id(c.getLong(c.getColumnIndex("outlet_product_id")));
//        }
//        if (c.getColumnIndex("outlet_id") != -1) {
//            product.setOutlet_id(c.getLong(c.getColumnIndex("outlet_id")));
//        }
//        if (c.getColumnIndex("parent_outlet_product_id") != -1) {
//            product.setParent_outlet_product_id(c.getLong(c.getColumnIndex("parent_outlet_product_id")));
//        }
//        if (c.getColumnIndex("default_outlet_product_id") != -1) {
//            product.setDefault_outlet_product_id(c.getLong(c.getColumnIndex("default_outlet_product_id")));
//        }
//        if (c.getColumnIndex("default_parent_outlet_product_id") != -1) {
//            product.setDefault_parent_outlet_product_id(c.getLong(c.getColumnIndex("default_parent_outlet_product_id")));
//        }
//        if (c.getColumnIndex("default_product_id") != -1) {
//            product.setDefault_product_id(c.getLong(c.getColumnIndex("default_product_id")));
//        }
//        if (c.getColumnIndex("default_price") != -1) {
//            product.setDefault_price(c.getDouble(c.getColumnIndex("default_price")));
//        }
//        if (c.getColumnIndex("default_name") != -1) {
//            product.setDefault_name(c.getString(c.getColumnIndex("default_name")));
//        }
//        if (c.getColumnIndex("default_selected") != -1) {
//            product.setDefault_selected(c.getLong(c.getColumnIndex("default_selected")));
//        }
//        if (c.getColumnIndex("default_description") != -1) {
//            product.setDefault_description(c.getString(c.getColumnIndex("default_description")));
//        }
//        if (c.getColumnIndex("sort_no") != -1) {
//            product.setSort_no(c.getLong(c.getColumnIndex("sort_no")));
//        }
//        if (c.getColumnIndex("id") != -1) {
//            product.setId(c.getLong(c.getColumnIndex("id")));
//        }
//        if (c.getColumnIndex("code") != -1) {
//            product.setCode(c.getString(c.getColumnIndex("code")));
//        }
//        if (c.getColumnIndex("company_id") != -1) {
//            product.setCompany_id(c.getLong(c.getColumnIndex("company_id")));
//        }
//        if (c.getColumnIndex("name") != -1) {
//            product.setName(c.getString(c.getColumnIndex("name")));
//        }
//        if (c.getColumnIndex("description") != -1) {
//            product.setDescription(c.getString(c.getColumnIndex("description")));
//        }
//        if (c.getColumnIndex("price") != -1) {
//            product.setPrice(c.getDouble(c.getColumnIndex("price")));
//        }
//        if (c.getColumnIndex("photo_url") != -1) {
//            product.setPhoto_url(c.getString(c.getColumnIndex("photo_url")));
//        }
//        if (c.getColumnIndex("category_id") != -1) {
//            product.setCategory_id(c.getLong(c.getColumnIndex("category_id")));
//        }
//
//        if (c.getColumnIndex("cust_count") != -1) {
//            product.setCust_count(c.getInt(c.getColumnIndex("cust_count")));
//        }
//    }
//
//    @NotNull
//    public  List<Product> getProductList(@NotNull DBHelper dbHelper, String sql){
//        List<Product> productList = new ArrayList<Product>();
//        Cursor c;
//
//        c = dbHelper.getWritableDatabase().rawQuery(sql, null);
//
//        if (c.getCount() > 0) {
//            c.moveToFirst();
//            while (!c.isAfterLast()) {
//
//                Product product = new Product();
//                cursorToObject(c, product);
//                productList.add(product);
//                c.moveToNext();
//            }
//        }
//        c.close();
//        return productList;
//    }
//
//
//    private  void cursorToObject(@NotNull Cursor c, @NotNull Outlet_attribute outletAttribute) {
//
//        if (c.getColumnIndex("id") != -1) {
//            outletAttribute.setId(c.getLong(c.getColumnIndex("id")));
//        }
//        if (c.getColumnIndex("created_at") != -1) {
//            outletAttribute.setCreated_at(c.getString(c.getColumnIndex("created_at")));
//        }
//        if (c.getColumnIndex("modified_at") != -1) {
//            outletAttribute.setModified_at(c.getString(c.getColumnIndex("modified_at")));
//        }
//        if (c.getColumnIndex("name") != -1) {
//            outletAttribute.setName(c.getString(c.getColumnIndex("name")));
//        }
//        if (c.getColumnIndex("value_type") != -1) {
//            outletAttribute.setValue_type(c.getString(c.getColumnIndex("value_type")));
//        }
//        if (c.getColumnIndex("outlet_attributes_group_id") != -1) {
//            outletAttribute.setOutlet_attributes_group_id(c.getLong(c.getColumnIndex("outlet_attributes_group_id")));
//        }
//        if (c.getColumnIndex("outlets_count") != -1) {
//            outletAttribute.setOutlets_count(c.getInt(c.getColumnIndex("outlets_count")));
//        }
//
//    }
//
//    @NotNull
//    public  List<Outlet_attribute> getOutletAttributesList(@NotNull DBHelper dbHelper,String sql){
//        List<Outlet_attribute> outletAttributeList = new ArrayList<Outlet_attribute>();
//        Cursor c;
//
//        c = dbHelper.getWritableDatabase().rawQuery(sql, null);
//
//        if (c.getCount() > 0) {
//            c.moveToFirst();
//            while (!c.isAfterLast()) {
//
//                Outlet_attribute outletAttribute= new Outlet_attribute();
//                cursorToObject(c,outletAttribute);
//                outletAttributeList.add(outletAttribute);
//                c.moveToNext();
//            }
//        }
//        c.close();
//        return outletAttributeList;
//    }
//
//
//
//    private  void cursorToObject(@NotNull Cursor c, @NotNull Product_attribute productAttribute) {
//
//        if (c.getColumnIndex("id") != -1) {
//            productAttribute.setId(c.getLong(c.getColumnIndex("id")));
//        }
//        if (c.getColumnIndex("created_at") != -1) {
//            productAttribute.setCreated_at(c.getString(c.getColumnIndex("created_at")));
//        }
//        if (c.getColumnIndex("modified_at") != -1) {
//            productAttribute.setModified_at(c.getString(c.getColumnIndex("modified_at")));
//        }
//        if (c.getColumnIndex("name") != -1) {
//            productAttribute.setName(c.getString(c.getColumnIndex("name")));
//        }
//        if (c.getColumnIndex("value_type") != -1) {
//            productAttribute.setValue_type(c.getString(c.getColumnIndex("value_type")));
//        }
//        if (c.getColumnIndex("product_attribute_group_id") != -1) {
//            productAttribute.setProduct_attribute_group_id(c.getLong(c.getColumnIndex("product_attribute_group_id")));
//        }
//        if (c.getColumnIndex("products_count") != -1) {
//            productAttribute.setProducts_count(c.getInt(c.getColumnIndex("products_count")));
//        }
//
//    }
//
//    @NotNull
//    public  List<Product_attribute> getProductAttributesList(@NotNull DBHelper dbHelper,String sql){
//        List<Product_attribute> productAttributeList = new ArrayList<Product_attribute>();
//        Cursor c;
//
//        c = dbHelper.getWritableDatabase().rawQuery(sql, null);
//
//        if (c.getCount() > 0) {
//            c.moveToFirst();
//            while (!c.isAfterLast()) {
//
//                Product_attribute productAttribute= new Product_attribute();
//                cursorToObject(c,productAttribute);
//                productAttributeList.add(productAttribute);
//                c.moveToNext();
//            }
//        }
//        c.close();
//        return productAttributeList;
//    }
//
//    private  void cursorToObject(@NotNull Cursor c, @NotNull Outlet_timing outletTiming) {
//
//        if (c.getColumnIndex("id") != -1) {
//            outletTiming.setId(c.getLong(c.getColumnIndex("id")));
//        }
//        if (c.getColumnIndex("created_at") != -1) {
//            outletTiming.setCreated_at(c.getString(c.getColumnIndex("created_at")));
//        }
//        if (c.getColumnIndex("modified_at") != -1) {
//            outletTiming.setModified_at(c.getString(c.getColumnIndex("modified_at")));
//        }
//        if (c.getColumnIndex("open_time") != -1) {
//            outletTiming.setOpen_time(c.getString(c.getColumnIndex("open_time")));
//        }
//        if (c.getColumnIndex("close_time") != -1) {
//            outletTiming.setClose_time(c.getString(c.getColumnIndex("close_time")));
//        }
//        if (c.getColumnIndex("day_of_week") != -1) {
//            outletTiming.setDay_of_week(c.getString(c.getColumnIndex("day_of_week")));
//        }
//        if (c.getColumnIndex("outlet_id") != -1) {
//            outletTiming.setOutlet_id(c.getLong(c.getColumnIndex("outlet_id")));
//        }
//        if (c.getColumnIndex("duration") != -1) {
//            outletTiming.setDuration(c.getString(c.getColumnIndex("duration")));
//        }
//        if (c.getColumnIndex("row_status") != -1) {
//            outletTiming.setRow_status(c.getString(c.getColumnIndex("row_status")));
//        }
//    }
//
//    @NotNull
//    public  List<Outlet_timing> getOutletTimingList(@NotNull DBHelper dbHelper, String sql){
//        List<Outlet_timing> outletTimingList= new ArrayList<Outlet_timing>();
//        Cursor c;
//
//        c = dbHelper.getWritableDatabase().rawQuery(sql, null);
//
//        if (c.getCount() > 0) {
//            c.moveToFirst();
//            while (!c.isAfterLast()) {
//
//                Outlet_timing outletTiming= new Outlet_timing();
//                cursorToObject(c,outletTiming);
//                outletTimingList.add(outletTiming);
//                c.moveToNext();
//            }
//        }
//        c.close();
//        return outletTimingList;
//    }
//
////private void cursorToObject(Cursor c,Order_Details detail){
////    if(c.getColumnIndex("id") != -1){
////        detail.setId(c.getType(c.getColumnIndex("id")));
////    }
////}
//
//
//    private  void cursorToObject(@NotNull Cursor c, @NotNull Category category) {
//
//        if (c.getColumnIndex("id") != -1) {
//            category.setId(c.getLong(c.getColumnIndex("id")));
//        }
//        if (c.getColumnIndex("created_at") != -1) {
//            category.setCreated_at(c.getString(c.getColumnIndex("created_at")));
//        }
//        if (c.getColumnIndex("modified_at") != -1) {
//            category.setModified_at(c.getString(c.getColumnIndex("modified_at")));
//        }
//        if (c.getColumnIndex("name") != -1) {
//            category.setName(c.getString(c.getColumnIndex("name")));
//        }
//        if (c.getColumnIndex("description") != -1) {
//            category.setDescription(c.getString(c.getColumnIndex("description")));
//        }
//        if (c.getColumnIndex("parent_category_id") != -1) {
//            category.setParent_category_id(c.getLong(c.getColumnIndex("parent_category_id")));
//        }
//        if (c.getColumnIndex("company_id") != -1) {
//            category.setCompany_id(c.getLong(c.getColumnIndex("company_id")));
//        }
//        if (c.getColumnIndex("selection_min") != -1) {
//            category.setSelection_min(c.getLong(c.getColumnIndex("selection_min")));
//        }
//        if (c.getColumnIndex("selection_max") != -1) {
//            category.setSelection_max(c.getLong(c.getColumnIndex("selection_max")));
//        }
//        if (c.getColumnIndex("photo_url") != -1) {
//            category.setPhoto_url(c.getString(c.getColumnIndex("photo_url")));
//        }
//        if (c.getColumnIndex("display") != -1) {
//            category.setDisplay(c.getString(c.getColumnIndex("display")));
//        }
//        if (c.getColumnIndex("row_status") != -1) {
//            category.setRow_status(c.getString(c.getColumnIndex("row_status")));
//        }
//        if (c.getColumnIndex("products_count") != -1) {
//            category.setProducts_count(c.getInt(c.getColumnIndex("products_count")));
//        }
//    }
//
//    @NotNull
//    public  List<Category> getCategoryList(@NotNull DBHelper dbHelper, String sql){
//        List<Category> categoryList= new ArrayList<Category>();
//        Cursor c;
//
//        c = dbHelper.getWritableDatabase().rawQuery(sql, null);
//
//        if (c.getCount() > 0) {
//            c.moveToFirst();
//            while (!c.isAfterLast()) {
//
//                Category category= new Category();
//                cursorToObject(c,category);
//                categoryList.add(category);
//                c.moveToNext();
//            }
//        }
//        c.close();
//        return categoryList;
//    }


}
