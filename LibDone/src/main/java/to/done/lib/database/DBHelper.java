package to.done.lib.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import to.done.lib.R;
import to.done.lib.entity.Address;
import to.done.lib.entity.Banner;
import to.done.lib.entity.BuyingLead;
import to.done.lib.entity.Category;
import to.done.lib.entity.Companydetail;
import to.done.lib.entity.Contact_Person;
import to.done.lib.entity.FOB;
import to.done.lib.entity.Link;
import to.done.lib.entity.MOQ;
import to.done.lib.entity.PayT;
import to.done.lib.entity.Product;
import to.done.lib.entity.ProductsForCompany;

/**
 * Database helper class used to manage the creation and upgrading of your database. This class also usually provides
 * the DAOs used by the other classes.
 */
public class DBHelper extends OrmLiteSqliteOpenHelper {

	// name of the database file for your application -- change to something appropriate for your app
	private static final String DATABASE_NAME = "ting_tong.db";
	// any time you make changes to your database objects, you may have to increase the database version
	private static final int DATABASE_VERSION = 3;

	public DBHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION, R.raw.ormlite_config);
	}

	/**
	 * This is called when the database is first created. Usually you should call createTable statements here to create
	 * the tables that will store your data.
	 */
	@Override
	public void onCreate(SQLiteDatabase db, ConnectionSource connectionSource) {
		try {
			Log.i(DBHelper.class.getName(), "onCreate");

			TableUtils.createTable(connectionSource, Companydetail.class);
            TableUtils.createTable(connectionSource, Product.class);
            TableUtils.createTable(connectionSource, Address.class);
            TableUtils.createTable(connectionSource, Link.class);
			TableUtils.createTable(connectionSource, Contact_Person.class);
            TableUtils.createTable(connectionSource, MOQ.class);
            TableUtils.createTable(connectionSource, FOB.class);
            TableUtils.createTable(connectionSource, PayT.class);
            TableUtils.createTable(connectionSource, Category.class);
//
            TableUtils.createTable(connectionSource,Banner.class);
            TableUtils.createTable(connectionSource,ProductsForCompany.class);
            TableUtils.createTable(connectionSource,BuyingLead.class);
//            TableUtils.createTable(connectionSource,Product_category_mapping.class);
//            TableUtils.createTable(connectionSource,Outlet_product_mapping.class);
//            TableUtils.createTable(connectionSource,Product_attribute.class);
//            TableUtils.createTable(connectionSource,Product_attribute_mapping.class);
//            TableUtils.createTable(connectionSource,Product_attribute_group.class);
//            TableUtils.createTable(connectionSource, CacheForData.class);
//            TableUtils.createTable(connectionSource, User.class);
//            TableUtils.createTable(connectionSource, Address.class);
//            TableUtils.createTable(connectionSource, Order.class);
//            TableUtils.createTable(connectionSource, Order_Details.class);
//            TableUtils.createTable(connectionSource, Area.class);
//            TableUtils.createTable(connectionSource, Subarea.class);
//            TableUtils.createTable(connectionSource, Applied_extra_charge.class);
//            TableUtils.createTable(connectionSource, Applied_offer.class);


        } catch (Exception e) {
			Log.e(DBHelper.class.getName(), "Can't create database", e);
			throw new RuntimeException(e);
		}


	}

	/**
	 * This is called when your application is upgraded and it has a higher version number. This allows you to adjust
	 * the various data to match the new version number.
	 */
	@Override
	public void onUpgrade(SQLiteDatabase db, ConnectionSource connectionSource, int oldVersion, int newVersion) {
		try {
//			Log.i(DBHelper.class.getName(), "onUpgrade");
//			TableUtils.dropTable(connectionSource, Outlet.class, true);
//			// after we drop the old databases, we create the new ones
//			onCreate(db, connectionSource);
//		} catch (SQLException e) {
//			Log.e(DBHelper.class.getName(), "Can't drop databases", e);
//			throw new RuntimeException(e);
        if(oldVersion==1){
            TableUtils.createTable(connectionSource,BuyingLead.class);

        }
		}catch (Exception e) {
//			Log.e(DBHelper.class.getName(), "Can't drop databases", e);
			throw new RuntimeException(e);
        }
	}



	/**
	 * Close the database connections and clear any cached DAOs.
	 */
	@Override
	public void close() {
		super.close();

	}
}
