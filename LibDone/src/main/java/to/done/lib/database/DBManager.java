package to.done.lib.database;

import android.content.Context;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.GenericRawResults;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import to.done.lib.entity.Address;
import to.done.lib.entity.Banner;
import to.done.lib.entity.BuyingLead;
import to.done.lib.entity.Category;
import to.done.lib.entity.CategoryDataArray;
import to.done.lib.entity.CompanyDataArray;
import to.done.lib.entity.CompanyDetailforProductSearch;
import to.done.lib.entity.Companydetail;
import to.done.lib.entity.Contact_Person;
import to.done.lib.entity.FOB;
import to.done.lib.entity.Link;
import to.done.lib.entity.MOQ;
import to.done.lib.entity.PayT;
import to.done.lib.entity.Product;
import to.done.lib.entity.ProductDataArray;
import to.done.lib.entity.ProductsForCompany;
import to.done.lib.utils.Toolbox;

/**
 * Created by dipen on 10/4/14.insertPr
 */
public final class DBManager extends DBTools {

    private static DBHelper dbHelper;


    public DBManager(Context context) {
        dbHelper = new DBHelper(context);
    }


private Dao<Companydetail,Long>companyDao;
    private Dao<Product,Long>productDao;
    private Dao<Address,Long>addressDao;
    private Dao<Link,Long>linkDao;
    private Dao<Contact_Person,Long>contactsDao;
    private Dao<MOQ,Long>moqDao;
    private Dao<FOB,Long>fobDao;
    private Dao<PayT,Long>paytDao;
    private Dao<Category,Long>categoryDao;
    private Dao<Banner,Long>bannerDao;
    private Dao<ProductsForCompany,Long>productsForCompaniesDAO;
    private Dao<BuyingLead,Long>buyingLeadDao;

    private Dao<Companydetail, Long> getCompanyDao() throws SQLException {
        if (companyDao == null) {
            companyDao = dbHelper.getDao(Companydetail.class);
        }
        return companyDao;
    }
    private Dao<BuyingLead, Long> getBuyingLeadDao() throws SQLException {
        if (buyingLeadDao == null) {
            buyingLeadDao = dbHelper.getDao(BuyingLead.class);
        }
        return buyingLeadDao;
    }
    private Dao<ProductsForCompany, Long> getProdForComp() throws SQLException {
        if (productsForCompaniesDAO == null) {
            productsForCompaniesDAO = dbHelper.getDao(ProductsForCompany.class);
        }
        return productsForCompaniesDAO;
    }
    private Dao<Product, Long> getProductDao() throws SQLException {
        if (productDao == null) {
            productDao = dbHelper.getDao(Product.class);
        }
        return productDao;
    }
    private Dao<Category, Long> getCategoryDao() throws SQLException {
        if (categoryDao == null) {
            categoryDao = dbHelper.getDao(Category.class);
        }
        return categoryDao;
    }
    private Dao<Contact_Person, Long> getContactsDao() throws SQLException {
        if (contactsDao == null) {
            contactsDao = dbHelper.getDao(Contact_Person.class);
        }
        return contactsDao;
    }
    private Dao<Address, Long> getAddressDao() throws SQLException {
        if (addressDao == null) {
            addressDao = dbHelper.getDao(Address.class);
        }
        return addressDao;
    }
    private Dao<Link, Long> getLinksDao() throws SQLException {
        if (linkDao == null) {
            linkDao = dbHelper.getDao(Link.class);
        }
        return linkDao;
    }
    private Dao<MOQ, Long> getMoqDao() throws SQLException {
        if (moqDao == null) {
            moqDao = dbHelper.getDao(MOQ.class);
        }
        return moqDao;
    }
    private Dao<FOB,Long> getFobDao() throws SQLException {
        if (fobDao == null) {
            fobDao = dbHelper.getDao(FOB.class);
        }
        return fobDao;
    }

    private Dao<PayT,Long> getPaytDao() throws SQLException {
        if (paytDao == null) {
            paytDao = dbHelper.getDao(PayT.class);
        }
        return paytDao;
    }

    private Dao<Banner, Long> getBannerDao() throws SQLException {
        if (bannerDao == null) {
            bannerDao = dbHelper.getDao(Banner.class);
        }
        return bannerDao;
    }

    public void insertCompanies(List<CompanyDataArray> cdArray)throws SQLException{
        if(cdArray==null || cdArray.size()==0) return;
        for(CompanyDataArray cda:cdArray){
            for(Companydetail cd:cda.getCompanydetails()){
                insertCompany(cd,cda.getEmail(),cda.getPhone(),cda.getCallnos(),cda.getTelephonefax(),cda.getMobile());
               insertAddress(cda.getAddress(),cd.getId());
                insertLinks(cda.getLinks(),cd.getId());
                insertCompanyContactPerson(cda.getContact());
            }
            for(Product p:cda.getProducts())
            insertProducts(p);
        }
}
public void insertProducts(Product prods){
   try {
       if (prods == null) return;

           getProductDao().createOrUpdate(prods);

   }catch (Exception e){
       e.printStackTrace();
   }
}
    public void insertCompany(Companydetail comp,String email,String phone,String call,String telfax,String mob) {
        try {
            comp.setEmails(email);
            comp.setPhoneNos(phone);
            comp.setCallnos(call);
            comp.setTelephonefax(telfax);
            comp.setMobile(mob);
            getCompanyDao().createOrUpdate(comp);
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }

    public void insertBuyingLeads(List<BuyingLead>leads){
        try {
            if (leads == null || leads.size() == 0) return;
            for (BuyingLead b : leads) {
                getBuyingLeadDao().createOrUpdate(b);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    public List<BuyingLead> getBuyLeads(){
        try{
            List<BuyingLead>leads=getBuyingLeadDao().queryForAll();
            return leads;

        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public void insertBanners(List<CategoryDataArray>catsDataArray){
        if(catsDataArray==null || catsDataArray.size()==0) return;

      try {
          for(CategoryDataArray cd:catsDataArray) {
              for (Banner b : cd.getBanner()) {
                  getBannerDao().createOrUpdate(b);
              }
          }
      }catch (Exception e){
          e.printStackTrace();
      }
    }


    public void insertProducts(List<ProductDataArray>products)throws SQLException{
       try {
           Toolbox.writeToLog("Inserting product status 1");
           if (products == null || products.size() == 0) return;
           for (ProductDataArray pda : products) {
               Toolbox.writeToLog("Inserting product status 2");
               for (Product comp : pda.getProducts()) {
                   comp.setIs_for_list(1);
                   Toolbox.writeToLog("Inserting product status " + getProductDao().createOrUpdate(comp));
               }
           }
       }catch (Exception e){
           e.printStackTrace();
       }
    }
    public void insertProductsForCompany(List<ProductDataArray>products)throws SQLException{
        try {

            if (products == null || products.size() == 0) return;
            for (ProductDataArray pda : products) {

                for (Product comp : pda.getProducts()) {

                    ProductsForCompany p=new ProductsForCompany();
                    p.setId(comp.getId());
                    p.setName(comp.getName());
                    p.setDescription(comp.getDescription());
                    p.setCompanyId(comp.getCompanyId());
                    p.setImageUrl(comp.getImageUrl());
                    p.setRowStatus(comp.getRowStatus());
                    updateCompanyDetails(comp.getCompanyId(),pda.getMobile(),pda.getTelephonefax(),pda.getCallnos());
                    Toolbox.writeToLog("Inserting product status " + getProdForComp().createOrUpdate(p));
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void updateCompanyDetails(Long id,String mob,String tel,String call){
        try {
            List<Companydetail> companies = getCompanyDao().queryForEq("id", id);
            if(companies!=null && companies.size()>0){
                Companydetail c=companies.get(0);
                c.setMobile(mob);
                c.setTelephonefax(tel);
                c.setCallnos(call);
                getCompanyDao().update(c);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        }

    public List<Product>getProductsForCompany(Long company_id){
        try {
            if (company_id == null) return null;
            List<ProductsForCompany> productsForCompanies = new ArrayList<ProductsForCompany>();
            productsForCompanies.addAll(getProdForComp().queryForEq("companyId", company_id));
            List<Product>prods=new ArrayList<Product>();
            if(productsForCompanies!=null && productsForCompanies.size()>0){

                for(ProductsForCompany pr:productsForCompanies){
                prods.add(getProduct(pr));
            }
            return prods;
            }
            else{
                return null;
            }
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
        }

    private Product getProduct(ProductsForCompany pForComp){
        if(pForComp==null) return null;
        final Product p=new Product();
        p.setId(pForComp.getId());
        p.setCompanyId(pForComp.getCompanyId());
        p.setName(pForComp.getName());
        p.setDescription(pForComp.getDescription());
        p.setImageUrl(pForComp.getImageUrl());
        p.setSortNo(pForComp.getSortNo());
        return p;
    }

    public void insertCompanyDetailsForProduct(List<ProductDataArray>compsWithFullDetails){
        try{
            if(compsWithFullDetails==null || compsWithFullDetails.size()==0) return;
            for(ProductDataArray prod:compsWithFullDetails) {
                for (CompanyDetailforProductSearch cmpf : prod.getCompanydetails()) {
                    Toolbox.writeToLog("Cmpf is ..."+cmpf.toString());
                    Companydetail company =new Companydetail();
                    company.setId(cmpf.getId());
                    company.setName(cmpf.getName());
                    company.setDescription(cmpf.getDescription());
                    company.setPhoneNos(cmpf.getPhone());
                    company.setEmails(cmpf.getEmail());
                    company.setAddressId(cmpf.getAddressId());
                    company.setPaymentStatus(cmpf.getPaymentStatus());
                    company.setRating(cmpf.getRating().toString());
                    company.setSupplier(cmpf.getSupplier());
                    company.setImageUrl(cmpf.getImageUrl());
                    company.setVerification(cmpf.getVerification());
                    Address addr = new Address();
                    addr.setId(cmpf.getAddressId());
                    addr.setFlat(cmpf.getFlat());
                    addr.setBuilding(cmpf.getBuilding());
                    addr.setStreet(cmpf.getStreet());
                    addr.setCity(cmpf.getCity());
                    addr.setState(cmpf.getState());
                    addr.setCountry(cmpf.getCountry());
                    addr.setCityId(cmpf.getCityId());
                    addr.setStateId(cmpf.getStateId());
                    addr.setCountry(cmpf.getCountry());
                    addr.setArea_name(cmpf.getAreaName());
                    company.setAddress(addr);

                    insertCompany(company, cmpf.getEmails(), cmpf.getPhone(), cmpf.getCallnos(), cmpf.getTelephonefax(), cmpf.getMobile());
                    Toolbox.writeToLog("Company is ..."+company.toString());
                    insertAddress(addr, company.getId());
                }

                for(Link link:prod.getLinks()){
                    insertLink(link,link.getCompany_id());
                }


                    insertCompanyContactPerson(prod.getContact());

            }

        }catch(Exception e){
        e.printStackTrace();
        }
    }

    public void insertMoq(List<MOQ>moqList){
        try{
            if(moqList!=null && moqList.size()>0){
                for(MOQ m:moqList){
                    if(m==null)continue;
                    getMoqDao().createOrUpdate(m);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    public void insertfob(List<FOB>fobList){
        try{
            if(fobList!=null && fobList.size()>0){
                for(FOB m:fobList){
                    if(m==null)continue;
                    getFobDao().createOrUpdate(m);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void insertPayt(List<PayT>fobList){
        try{
            if(fobList!=null && fobList.size()>0){
                for(PayT m:fobList){
                    if(m==null)continue;
                    getPaytDao().createOrUpdate(m);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public FOB getFOBForProduct(Long product_id){
        try{
            List<FOB>results= getFobDao().queryForEq("product_id",product_id);
            return results.get(0);
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public MOQ getMOQForProduct(Long product_id){
        try{
            List<MOQ>results= getMoqDao().queryForEq("product_id",product_id);
            return results.get(0);
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public List<PayT> getPaytForProduct(Long product_id){
        try{
            List<PayT>results= getPaytDao().queryForEq("product_id",product_id);
            return results;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }
public void insertAddress(Address addr,Long company_id)throws SQLException{
    if(addr==null || company_id==null) return;
    addr.setCompany_id(company_id);
    Toolbox.writeToLog("Inserting address status "+getAddressDao().createOrUpdate(addr));
}

    public void insertCategories(List<CategoryDataArray>catsDataArray){

        try{

            for(CategoryDataArray cd:catsDataArray){
                for(Category c: cd.getCategories()){
                    getCategoryDao().createOrUpdate(c);
                    //Toolbox.writeToLog("Inserted company.."+c.getCategoryName());
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public List<Category> getParentCategories(){
        try{

            return getCategoryDao().queryBuilder().where().isNull("parent_category_id").query();
            //return getCategoryDao().queryForEq("parent_category_id",null);
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public List<Category> getChildCategories(Long parent_cat_id){
        try{
            return getCategoryDao().queryForEq("parent_category_id",parent_cat_id);
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public List<Category> getChildCategories(){
        try{
            return getCategoryDao().queryBuilder().where().isNotNull("parent_category_id").query();
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public void insertLinks(List<Link>links,Long company_id)throws SQLException{
        if(links!=null && links.size()>0 && company_id!=null){
            for(Link comp:links){
                comp.setCompany_id(company_id);
                Toolbox.writeToLog("Inserting links status "+getLinksDao().createOrUpdate(comp));
            }
        }
    }
    public void insertLink(Link links,Long company_id)throws SQLException{
        if(links!=null && company_id!=null){

                links.setCompany_id(company_id);
                Toolbox.writeToLog("Inserting links status "+getLinksDao().createOrUpdate(links));

        }
    }
    public void insertCompanyContactPerson(List<Contact_Person>contacts){
        if(contacts!=null && contacts.size()>0){
            try{
                for(Contact_Person cp:contacts){
                  getContactsDao().createOrUpdate(cp);
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    public void clearTable(String tableName, Long Id) {
        String query = "delete from " + tableName + " where outlet_id=" + Id;
    }
    public void clearCategoryTables(){
        try {
            TableUtils.clearTable(dbHelper.getConnectionSource(), Category.class);
            // TableUtils.clearTable(dbHelper.getConnectionSource(), Product.class);
//            TableUtils.clearTable(dbHelper.getConnectionSource(), Address.class);
//            TableUtils.clearTable(dbHelper.getConnectionSource(), Link.class);
//            TableUtils.clearTable(dbHelper.getConnectionSource(), Product.class);
//            TableUtils.clearTable(dbHelper.getConnectionSource(), Product_attribute_mapping.class);
//            TableUtils.clearTable(dbHelper.getConnectionSource(), Product_attribute_group.class);
            // TableUtils.clearTable(dbHelper.getConnectionSource(),CacheForData.class);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void clearBuyLeads(){
        try {
            TableUtils.clearTable(dbHelper.getConnectionSource(), BuyingLead.class);
        }catch (Exception e){
            e.printStackTrace();
        }
        }
    public void clearCompanyRelatedTables(){
        try {
            TableUtils.clearTable(dbHelper.getConnectionSource(), Companydetail.class);
            TableUtils.clearTable(dbHelper.getConnectionSource(), Contact_Person.class);
            TableUtils.clearTable(dbHelper.getConnectionSource(), Address.class);
            TableUtils.clearTable(dbHelper.getConnectionSource(), Link.class);
            TableUtils.clearTable(dbHelper.getConnectionSource(), Product.class);
            TableUtils.clearTable(dbHelper.getConnectionSource(), FOB.class);
            TableUtils.clearTable(dbHelper.getConnectionSource(), MOQ.class);
             TableUtils.clearTable(dbHelper.getConnectionSource(),PayT.class);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public List<Companydetail>getAllCompanies(){
        try{
           //getCompanyDao().queryBuilder().orderBy("sortNo",false).query();
            List<Companydetail>companies= getCompanyDao().queryBuilder().orderBy("sortNo",true).query();//getCompanyDao().queryBuilder().query();
            if(companies!=null && companies.size()>0){
                for(Companydetail com:companies) {
                    List<Address>address=new ArrayList<Address>(getAddressDao().queryForEq("id", com.getAddressId()));
                    if(address.size()>0) com.setAddress(address.get(0));
                    List<Link>links=new ArrayList<Link>(getLinksDao().queryForEq("company_id",com.getId()));
                    com.setLinks(links);
                }
            }
            return companies;
    }catch (SQLException e){
            e.printStackTrace();
            return null;
        }
    }
    public List<Product>getAllProducts(){
        try{
           // getProductDao().queryBuilder().orderBy("sort_no",true);
            String query="select * from product order by sortNo asc";
            GenericRawResults<Product> rawResults = productDao.queryRaw(query, getProductDao().getRawRowMapper());
            List<Product> prodList = rawResults.getResults();
            rawResults.close();
            Toolbox.writeToLog("Producs are/..."+prodList);
            if(prodList!=null && prodList.size()>0)
            return prodList;//Builder().query();
            else return null;
        }catch (SQLException e){
            e.printStackTrace();
            return null;
        }
    }


    public Companydetail getCompanyById(Long company_id){
        try {
            if (company_id == null) return null;
            Companydetail com=getCompanyDao().queryForId(company_id);
            List<Address>address=new ArrayList<Address>(getAddressDao().queryForEq("id", com.getAddressId()));
            if(address!=null && address.size()>0) com.setAddress(address.get(0));
            List<Link>links=new ArrayList<Link>(getLinksDao().queryForEq("company_id",com.getId()));
            com.setLinks(links);
            List<Product>products=new ArrayList<Product>(getProductDao().queryForEq("companyId",com.getId()));
            com.setProducts(products);
            return com;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }


    public List<Contact_Person> getContactsForCompany(Long copany_id){
        if(copany_id==null) return null;
        try{
           List<Contact_Person>contacts= getContactsDao().queryForEq("company_id",copany_id);
            return contacts;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }
public Banner[] getBanners(){
    try {
        List<Banner> banners = getBannerDao().queryForAll();
      if(banners==null || banners.size()==0)
        return null;
        else return banners.toArray(new Banner[banners.size()]);
    }catch (Exception e){
        e.printStackTrace();

    }
    return null;
    }

    public Product getProductById(Long product_id){
        try{
            if(product_id==null || product_id.longValue()==-1) return null;
            Product p=getProductDao().queryForId(product_id);
            return p;

        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

public int getCountOfProductsForCompany(Long company_id){
    try {
        String query = "select count(*) from product where company_id=?";

        GenericRawResults<String[]> rawResults =
                getProductDao().queryRaw(query, company_id.toString());
        int count= Integer.parseInt(rawResults.getFirstResult()[0]);
        return count;
    }catch (Exception e){
        e.printStackTrace();
        return 0;
    }
 }

}