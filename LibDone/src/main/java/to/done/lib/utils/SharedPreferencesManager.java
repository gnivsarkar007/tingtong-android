package to.done.lib.utils;

import android.content.Context;
import android.content.SharedPreferences;

import org.jetbrains.annotations.NotNull;

import to.done.lib.Constants;

/**
 * Created by dipenpradhan on 4/25/14.
 */
public final class SharedPreferencesManager {

    private static SharedPreferences sharedPreferences;

    private SharedPreferencesManager() {
    }

    public static void writeString(@NotNull Context context,String key, String value){
        getSharedPreferencesEditor(context).putString(key,value).commit();
    }

    public static void writeInt(@NotNull Context context,String key, int value){
        getSharedPreferencesEditor(context).putInt(key,value).commit();
    }

    public static void writeBoolean(@NotNull Context context,String key, boolean value){
        getSharedPreferencesEditor(context).putBoolean(key,value).commit();
    }

    public static void writeLong(@NotNull Context context,String key, long value){
        getSharedPreferencesEditor(context).putLong(key,value).commit();
    }

    public static void writeFloat(@NotNull Context context,String key, float value){
        getSharedPreferencesEditor(context).putFloat(key,value).commit();
    }

    public static SharedPreferences getSharedPreferences(@NotNull Context context) {
        if(sharedPreferences==null){
            sharedPreferences = context.getSharedPreferences(Constants.PREFS_NAME,Context.MODE_PRIVATE);
        }
        return sharedPreferences;
    }
    public static SharedPreferences.Editor getSharedPreferencesEditor(@NotNull Context context){

        return getSharedPreferences(context).edit();
    }

}
