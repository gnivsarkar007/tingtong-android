package to.done.lib.utils;

import android.content.Context;
import android.util.Log;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import org.jetbrains.annotations.Nullable;

import to.done.lib.R;

/**
 * Created by viva on 19/6/14.
 */
public class GoogleAnalyticsManager {

    public static GoogleAnalytics googleAnalytics;
    public static final String TAG="GoogleAnalyticsManager";
    @Nullable
    public static synchronized Tracker getTracker(Context context) {
        Tracker tracker = null;
        if (googleAnalytics == null)
            googleAnalytics = GoogleAnalytics.getInstance(context);

        tracker = googleAnalytics.newTracker(R.xml.google_analytics_global_tracker);
        return tracker;
    }

    public static void sendScreenView(Context context, String screenName) {
        Tracker tracker = getTracker(context);
        if (tracker != null) {
            tracker.setScreenName(screenName);


            tracker.send(new HitBuilders.AppViewBuilder().build());
            Log.e(TAG, "sendScreenView()-->"+screenName);
        }
    }
}
