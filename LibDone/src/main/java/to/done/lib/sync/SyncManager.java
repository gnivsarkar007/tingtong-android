package to.done.lib.sync;

import android.app.Activity;
import android.content.SharedPreferences;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import to.done.lib.Constants;
import to.done.lib.database.DBManager;
import to.done.lib.entity.NewCompanyRegReq;
import to.done.lib.entity.ProductDataArray;
import to.done.lib.entity.ReqCompanyByCategory;
import to.done.lib.entity.ReqCompanyCategory;
import to.done.lib.entity.RequestEntityByName;
import to.done.lib.entity.RequestEntityyById;
import to.done.lib.entity.ResponseBuyLeads;
import to.done.lib.entity.ResponseCompanyCategory;
import to.done.lib.entity.ResponseSearchByProductName;
import to.done.lib.entity.ResponseSearchCompanyByName;
import to.done.lib.entity.UserBuyRequest;
import to.done.lib.entity.UserEnquiryRequest;
import to.done.lib.entity.UserEnquiryResp;
import to.done.lib.entity.gcm.RequestGCMRegistration;
import to.done.lib.entity.gcm.ResponseGCMRegistration;
import to.done.lib.utils.SharedPreferencesManager;
import to.done.lib.utils.Toolbox;
import to.done.lib.webservice.HttpHelper;



/**
 * Created by dipen on 29/11/13.
 */
public class SyncManager {


    //    private Context context;
//    private static final Gson gson = new Gson();
//    private static final Gson gsonWOExpose =  new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
    private static final String SERVER_ERROR = "Server Error";
    private static final ObjectMapper objMapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false).configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY,true);
    private static String tag;
//
//    private SyncListener syncListener;
//
//    public Context getContext() {
//        return context;
//    }
//
//    public void setContext(Context context) {
//        this.context = context;
//    }
//
//    public SyncListener getSyncListener() {
//        return syncListener;
//    }
//
//    public void setSyncListener(SyncListener syncListener) {
//        this.syncListener = syncListener;
//    }
//


//    public static void getClosestSubareas(final Activity context, double latitude, double longitude, final SyncListener syncListener) {
//        sendSyncStartEvent(syncListener, context);
//
//
//        ResponseHandler responseHandler = new ResponseHandler() {
//            @Override
//            public void handleStringResponse(String url, String response, long requestTimestamp) {
//                try {
//                    final RespSolrSubareas resp = objMapper.readValue(response, RespSolrSubareas.class);
//
//                    if (resp.getResponseHeader().getStatus() != RESPONSE_CODE_SUCCESS) {
//                        sendSyncFailureEvent(url, syncListener, context, resp.getError().getMsg(), requestTimestamp);
//                        return;
//                    }
//
//
//                    sendSyncSuccessEvent(url, syncListener, context, resp, requestTimestamp);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                    sendSyncFailureEvent(url, syncListener, context, SERVER_ERROR, requestTimestamp);
//
//                }
//            }
//        };
//
//        AsyncJsonGet asyncJsonGet = new AsyncJsonGet(context, URL_CLOSEST_SUBAREA + latitude + "," + longitude, HttpHelper.EncodingType.PLAIN_TEXT, responseHandler);
//        asyncJsonGet.setDownloadListener(buildDownloadListener(syncListener, context));
//        if (Toolbox.isInternetAvailable(context))
//            asyncJsonGet.execute();
//        else {
//            sendSyncFailureEvent(URL_CLOSEST_SUBAREA, syncListener, context, ERROR_MESSAGE_INTERNET_UNAVAILABLE, 0);
//        }
//    }

    public static void registerDevice(final Activity context, final SyncListener syncListener) {
        sendSyncStartEvent(syncListener, context);

        ResponseHandler responseHandler = new ResponseHandler() {
            @Override
            public void handleStringResponse(String url, String response, long requestTimestamp) {
                try {

                    final ResponseGCMRegistration resp = objMapper.readValue(response, ResponseGCMRegistration.class);
                    if (resp != null && resp.getResponseCode() != Constants.RESPONSE_CODE_SUCCESS) {
                        sendSyncFailureEvent(url, syncListener, context, resp.getResponseMsg(), requestTimestamp);
                        return;
                    }

                    sendSyncSuccessEvent(url, syncListener, context, resp, requestTimestamp);
                } catch (Exception e) {
                    e.printStackTrace();
                    sendSyncFailureEvent(url, syncListener, context, SERVER_ERROR, requestTimestamp);

                }
            }
        };
        SharedPreferences sharedPreferences = SharedPreferencesManager.getSharedPreferences(context);
        String token = sharedPreferences.getString(Constants.PREFS_GCM_ID_KEY, "");
        String uid = sharedPreferences.getString(Constants.PREFS_GCM_UUID_KEY, "");


        RequestGCMRegistration req = new RequestGCMRegistration(token, uid, Constants.getDeviceName(), Constants.getdevicePlatform(), Constants.getDeviceVersion());
        try {
            AsyncJsonPost asyncJsonPost = new AsyncJsonPost(context, Constants.URL_REGISTER_DEVICE, objMapper.writeValueAsString(req), HttpHelper.EncodingType.PLAIN_TEXT, responseHandler);
            asyncJsonPost.setDownloadListener(buildDownloadListener(syncListener, context));
            asyncJsonPost.execute();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

    public static void getCompanyCategories(final Activity context, final SyncListener syncListener,final DBManager db) {
        final SharedPreferences sharedPreferences = SharedPreferencesManager.getSharedPreferences(context);
        sendSyncStartEvent(syncListener, context);

        ResponseHandler responseHandler = new ResponseHandler() {
            @Override
            public void handleStringResponse(String url, String response, long requestTimestamp) {
                try {

                    final ResponseCompanyCategory resp = objMapper.readValue(response, ResponseCompanyCategory.class);
                    if (resp != null && resp.getResponseCode() != Constants.RESPONSE_CODE_SUCCESS) {
                        sendSyncFailureEvent(url, syncListener, context, resp.getResponseMsg(), requestTimestamp);
                        return;
                    }else if(resp != null && resp.getResponseCode() == Constants.RESPONSE_CODE_SUCCESS && resp.getDataArray()==null){
                        sendSyncSuccessEvent(url,syncListener,context,response,requestTimestamp);
                    }else if(resp != null && resp.getResponseCode() == Constants.RESPONSE_CODE_SUCCESS && resp.getDataArray()!=null){
                        Toolbox.writeToLog("Response.."+resp.getDataArray().get(0).getCategories());
                        if(resp.getDataArray().get(0).getCategories()!=null && resp.getDataArray().get(0).getCategories().size()>0) {
                            db.clearCategoryTables();
                            db.insertCategories(resp.getDataArray());
                            sharedPreferences.edit().putString(Constants.PREFS_CATEGORY_VERSION,(resp.getDataArray().get(0).getVersion().get(0).getVersion())).commit();
                        }
                        db.insertBanners(resp.getDataArray());
                        sharedPreferences.edit().putString(Constants.PREFS_LEADS_COUNT,(resp.getDataArray().get(0).getLeads().get(0).getLeads())).commit();
                        double version = Double.valueOf(sharedPreferences.getString(Constants.PREFS_CATEGORY_VERSION, "0.00"));
                        String leads=sharedPreferences.getString(Constants.PREFS_LEADS_COUNT,"");
                        Toolbox.writeToLog("Categories version is...."+version+" ///leads are.."+leads);
                        sendSyncSuccessEvent(url,syncListener,context,response,requestTimestamp);
                    }else{

                        sendSyncFailureEvent(url, syncListener, context, SERVER_ERROR, requestTimestamp);

                    }
                    return;

                    //sendSyncSuccessEvent(url, syncListener, context, resp, requestTimestamp);
                } catch (Exception e) {
                    e.printStackTrace();
                    sendSyncFailureEvent(url, syncListener, context, SERVER_ERROR, requestTimestamp);

                }
            }
        };
        // sharedPreferences = SharedPreferencesManager.getSharedPreferences(context);
       double version = Double.valueOf(sharedPreferences.getString(Constants.PREFS_CATEGORY_VERSION, "0.00"));
        //String uid = sharedPreferences.getString(Constants.PREFS_GCM_UUID_KEY, "");
        Toolbox.writeToLog("Categories version is...."+version);

        ReqCompanyCategory req= new ReqCompanyCategory();
        req.setVersion(version);
        try {
            AsyncJsonPost asyncJsonPost = new AsyncJsonPost(context, Constants.URL_CATEGORY, objMapper.writeValueAsString(req), HttpHelper.EncodingType.PLAIN_TEXT, responseHandler);
            asyncJsonPost.setDownloadListener(buildDownloadListener(syncListener, context));
            asyncJsonPost.execute();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

public static void getCompanyByName(final String key,final SyncListener syncListener,final Activity context,final DBManager db) throws JsonProcessingException {
        sendSyncStartEvent(syncListener, context);
        ResponseHandler responseHandler = new ResponseHandler() {
            @Override
            public void handleStringResponse(String url, String response, long requestTimestamp) {
                try {
                    final ResponseSearchCompanyByName resp = objMapper.readValue(response, ResponseSearchCompanyByName.class);
                    db.clearCompanyRelatedTables();
                    if (resp!=null&&resp.getResponseCode() != Constants.RESPONSE_CODE_SUCCESS) {
                        db.clearCompanyRelatedTables();
                        sendSyncFailureEvent(url, syncListener, context, resp.getResponseMsg(), requestTimestamp);
                        return;
                    }
                    if(resp.getDataArray()!=null && resp.getResponseCode()==Constants.RESPONSE_CODE_SUCCESS){
                        db.clearCompanyRelatedTables();
                        db.insertCompanies(resp.getDataArray());
                        sendSyncSuccessEvent(url, syncListener, context, resp, requestTimestamp);
                        return;

                    }else if(resp.getDataArray()==null && resp.getResponseCode()==Constants.RESPONSE_CODE_SUCCESS)
                    {
                        db.clearCompanyRelatedTables();
                        resp.setResponseMsg("Data not found for your search");
                        sendSyncSuccessEvent(url, syncListener, context, resp, requestTimestamp);
                        return;
                        //sendSyncFailureEvent(url, syncListener, context, resp.getResponseMsg(), requestTimestamp);
                    }
                    else{
                        db.clearCompanyRelatedTables();
                        sendSyncFailureEvent(url, syncListener, context, Constants.ERROR_MESSAGE_GENERIC, requestTimestamp);

                    }
                  //  sendSyncSuccessEvent(url, syncListener, context, resp, requestTimestamp);
                    return;
                } catch (Exception e) {
                    e.printStackTrace();
                    db.clearCompanyRelatedTables();
                    sendSyncFailureEvent(url, syncListener, context, "Local database error", requestTimestamp);

                }
            }
        };
    RequestEntityByName req = new RequestEntityByName();
    req.setKey(key.toLowerCase());
        AsyncJsonPost asyncJsonPost = new AsyncJsonPost(context,Constants.URL_COMPANY_BY_NAME, objMapper.writeValueAsString(req), HttpHelper.EncodingType.PLAIN_TEXT, responseHandler);
    asyncJsonPost.setDownloadListener(buildDownloadListener(syncListener, context));
        if (Toolbox.isInternetAvailable(context))
            asyncJsonPost.execute();
        else {
            sendSyncFailureEvent(Constants.URL_COMPANY_BY_NAME, syncListener, context, Constants.ERROR_MESSAGE_INTERNET_UNAVAILABLE, 0);
        }
}
    public static void getCompanyByCategory(final Long cat_id,final SyncListener syncListener,final Activity context,final DBManager db)  {
        sendSyncStartEvent(syncListener, context);
        ResponseHandler responseHandler = new ResponseHandler() {
            @Override
            public void handleStringResponse(String url, String response, long requestTimestamp) {
                try {
                    final ResponseSearchCompanyByName resp = objMapper.readValue(response, ResponseSearchCompanyByName.class);

                    if (resp!=null&&resp.getResponseCode() != Constants.RESPONSE_CODE_SUCCESS) {
                        db.clearCompanyRelatedTables();
                        sendSyncFailureEvent(url, syncListener, context, resp.getResponseMsg(), requestTimestamp);
                        return;
                    }
                    if(resp.getDataArray()!=null && resp.getResponseCode()==Constants.RESPONSE_CODE_SUCCESS){
                        db.clearCompanyRelatedTables();
                        db.insertCompanies(resp.getDataArray());
                        sendSyncSuccessEvent(url, syncListener, context, resp, requestTimestamp);
                        return;

                    }else if(resp.getDataArray()==null && resp.getResponseCode()==Constants.RESPONSE_CODE_SUCCESS)
                    {
                        db.clearCompanyRelatedTables();
                        sendSyncSuccessEvent(url, syncListener, context, resp, requestTimestamp);
                        return;
                        //sendSyncFailureEvent(url, syncListener, context, resp.getResponseMsg(), requestTimestamp);
                    }
                    else{
                        sendSyncFailureEvent(url, syncListener, context, Constants.ERROR_MESSAGE_GENERIC, requestTimestamp);

                    }
                    //  sendSyncSuccessEvent(url, syncListener, context, resp, requestTimestamp);
                    return;
                } catch (Exception e) {
                    e.printStackTrace();
                    db.clearCompanyRelatedTables();
                    sendSyncFailureEvent(url, syncListener, context, "Local database error", requestTimestamp);

                }
            }
        };
        try {
            ReqCompanyByCategory req = new ReqCompanyByCategory();
            req.setCatId(cat_id);
            AsyncJsonPost asyncJsonPost = new AsyncJsonPost(context, Constants.URL_COMP_By_CAT, objMapper.writeValueAsString(req), HttpHelper.EncodingType.PLAIN_TEXT, responseHandler);
            asyncJsonPost.setDownloadListener(buildDownloadListener(syncListener, context));
            if (Toolbox.isInternetAvailable(context))
                asyncJsonPost.execute();
            else {
                sendSyncFailureEvent(Constants.URL_COMP_By_CAT, syncListener, context, Constants.ERROR_MESSAGE_INTERNET_UNAVAILABLE, 0);
            }
        }catch (Exception e){
            e.printStackTrace();
            e.printStackTrace();
            db.clearCompanyRelatedTables();
            sendSyncFailureEvent(Constants.URL_COMPANY_BY_NAME, syncListener, context, "Local database error", 0);
        }
    }



    public static void sendUserEnquiry(final UserEnquiryRequest req,final SyncListener syncListener,final Activity context,final DBManager db) {
        sendSyncStartEvent(syncListener, context);
        ResponseHandler responseHandler = new ResponseHandler() {
            @Override
            public void handleStringResponse(String url, String response, long requestTimestamp) {
                try {
                    final UserEnquiryResp resp = objMapper.readValue(response, UserEnquiryResp.class);

                    if (resp!=null&&resp.getResponseCode() != Constants.RESPONSE_CODE_SUCCESS) {
                        sendSyncFailureEvent(url, syncListener, context, resp.getResponseMsg(), requestTimestamp);
                        return;
                    }
                    if(resp.getDataArray()!=null && resp.getResponseCode()==Constants.RESPONSE_CODE_SUCCESS){
                        sendSyncSuccessEvent(url, syncListener, context, resp, requestTimestamp);
                        return;
                    }else if(resp.getDataArray()==null && resp.getResponseCode()==Constants.RESPONSE_CODE_SUCCESS)
                    {
                       // db.clearCompanyRelatedTables();
                        sendSyncSuccessEvent(url, syncListener, context, resp, requestTimestamp);
                        return;
                        //sendSyncFailureEvent(url, syncListener, context, resp.getResponseMsg(), requestTimestamp);
                    }else{
                        sendSyncFailureEvent(Constants.URL_SEND_ENQUIRY,syncListener,context,"Something went wrong,please try again..",requestTimestamp);
                        return;
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    sendSyncFailureEvent(url, syncListener, context, "Local database error", requestTimestamp);

                }
            }
        };
        try {
            AsyncJsonPost asyncJsonPost = new AsyncJsonPost(context, Constants.URL_SEND_ENQUIRY, objMapper.writeValueAsString(req), HttpHelper.EncodingType.PLAIN_TEXT, responseHandler);
            asyncJsonPost.setDownloadListener(buildDownloadListener(syncListener, context));
            if (Toolbox.isInternetAvailable(context))
                asyncJsonPost.execute();
            else {
                sendSyncFailureEvent(Constants.URL_SEND_ENQUIRY, syncListener, context, Constants.ERROR_MESSAGE_INTERNET_UNAVAILABLE, 0);
            }
        }catch (Exception e){
            e.printStackTrace();
        }    //sendSyncFailureEvent(Constants.URL_SEND_ENQUIRY, syncListener, context, Constants.ERROR_MESSAGE_GENERIC, 0);

    }

    public static void sendNewCompRequest(final NewCompanyRegReq req,final SyncListener syncListener,final Activity context,final DBManager db) {
        sendSyncStartEvent(syncListener, context);
        ResponseHandler responseHandler = new ResponseHandler() {
            @Override
            public void handleStringResponse(String url, String response, long requestTimestamp) {
                try {
                    final UserEnquiryResp resp = objMapper.readValue(response, UserEnquiryResp.class);

                    if (resp!=null&&resp.getResponseCode() != Constants.RESPONSE_CODE_SUCCESS) {
                        sendSyncFailureEvent(url, syncListener, context, resp.getResponseMsg(), requestTimestamp);
                        return;
                    }
                    if(resp.getDataArray()!=null && resp.getResponseCode()==Constants.RESPONSE_CODE_SUCCESS){
                        sendSyncSuccessEvent(url, syncListener, context, resp, requestTimestamp);
                        return;
                    }else if(resp.getDataArray()==null && resp.getResponseCode()==Constants.RESPONSE_CODE_SUCCESS)
                    {
                        // db.clearCompanyRelatedTables();
                        sendSyncSuccessEvent(url, syncListener, context, resp, requestTimestamp);
                        return;
                        //sendSyncFailureEvent(url, syncListener, context, resp.getResponseMsg(), requestTimestamp);
                    }else{
                        sendSyncFailureEvent(Constants.URL_NEW_COMP_REQ,syncListener,context,"Something went wrong,please try again..",requestTimestamp);
                        return;
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    sendSyncFailureEvent(url, syncListener, context, "Local database error", requestTimestamp);

                }
            }
        };
        try {
            AsyncJsonPost asyncJsonPost = new AsyncJsonPost(context, Constants.URL_NEW_COMP_REQ, objMapper.writeValueAsString(req), HttpHelper.EncodingType.PLAIN_TEXT, responseHandler);
            asyncJsonPost.setDownloadListener(buildDownloadListener(syncListener, context));
            if (Toolbox.isInternetAvailable(context))
                asyncJsonPost.execute();
            else {
                sendSyncFailureEvent(Constants.URL_SEND_ENQUIRY, syncListener, context, Constants.ERROR_MESSAGE_INTERNET_UNAVAILABLE, 0);
            }
        }catch (Exception e){
            e.printStackTrace();
        }    //sendSyncFailureEvent(Constants.URL_SEND_ENQUIRY, syncListener, context, Constants.ERROR_MESSAGE_GENERIC, 0);

    }

    public static void sendUserBuyRequest(final UserBuyRequest req,final SyncListener syncListener,final Activity context,final DBManager db) {
        sendSyncStartEvent(syncListener, context);
        ResponseHandler responseHandler = new ResponseHandler() {
            @Override
            public void handleStringResponse(String url, String response, long requestTimestamp) {
                try {
                    final UserEnquiryResp resp = objMapper.readValue(response, UserEnquiryResp.class);

                    if (resp!=null&&resp.getResponseCode() != Constants.RESPONSE_CODE_SUCCESS) {
                        sendSyncFailureEvent(url, syncListener, context, resp.getResponseMsg(), requestTimestamp);
                        return;
                    }
                    if(resp.getDataArray()!=null && resp.getResponseCode()==Constants.RESPONSE_CODE_SUCCESS){
                        sendSyncSuccessEvent(url, syncListener, context, resp, requestTimestamp);
                        return;
                    }else if(resp.getDataArray()==null && resp.getResponseCode()==Constants.RESPONSE_CODE_SUCCESS)
                    {
                        // db.clearCompanyRelatedTables();
                        sendSyncSuccessEvent(url, syncListener, context, resp, requestTimestamp);
                        return;
                        //sendSyncFailureEvent(url, syncListener, context, resp.getResponseMsg(), requestTimestamp);
                    }else{
                        sendSyncFailureEvent(Constants.URL_BUY_REQUEST,syncListener,context,"Something went wrong,please try again..",requestTimestamp);
                        return;
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    sendSyncFailureEvent(url, syncListener, context, "Local database error", requestTimestamp);

                }
            }
        };
        try {
            AsyncJsonPost asyncJsonPost = new AsyncJsonPost(context, Constants.URL_BUY_REQUEST, objMapper.writeValueAsString(req), HttpHelper.EncodingType.PLAIN_TEXT, responseHandler);
            asyncJsonPost.setDownloadListener(buildDownloadListener(syncListener, context));
            if (Toolbox.isInternetAvailable(context))
                asyncJsonPost.execute();
            else {
                sendSyncFailureEvent(Constants.URL_BUY_REQUEST, syncListener, context, Constants.ERROR_MESSAGE_INTERNET_UNAVAILABLE, 0);
            }
        }catch (Exception e){
            e.printStackTrace();
        }    //sendSyncFailureEvent(Constants.URL_SEND_ENQUIRY, syncListener, context, Constants.ERROR_MESSAGE_GENERIC, 0);

    }

    public static void getProductByName(final String key,final SyncListener syncListener,final Activity context,final DBManager db) throws JsonProcessingException {
        sendSyncStartEvent(syncListener, context);
        ResponseHandler responseHandler = new ResponseHandler() {
            @Override
            public void handleStringResponse(String url, String response, long requestTimestamp) {
                try {
                    final ResponseSearchByProductName resp = objMapper.readValue(response, ResponseSearchByProductName.class);

                    if (resp != null && resp.getResponseCode() != Constants.RESPONSE_CODE_SUCCESS) {
                        db.clearCompanyRelatedTables();
                        sendSyncFailureEvent(url, syncListener, context, resp.getResponseMsg(), requestTimestamp);
                        return;
                    }
                    if (resp.getResponseCode() == Constants.RESPONSE_CODE_SUCCESS) {
                        if (resp.getDataArray() != null) {
                            db.clearCompanyRelatedTables();
                            Toolbox.writeToLog("Size of datarrray " + resp.getDataArray().size());
                            db.insertProducts(resp.getDataArray());
                            db.insertCompanyDetailsForProduct(resp.getDataArray());
                            for(ProductDataArray pd:resp.getDataArray()){
                                db.insertfob(pd.getFobs());
                                db.insertMoq(pd.getMoqs());
                                db.insertPayt(pd.getPayT());
                            }
                            sendSyncSuccessEvent(url, syncListener, context, resp, requestTimestamp);
                            return;

                        }
                        else if(resp.getDataArray()==null){
                            db.clearCompanyRelatedTables();
                            resp.setResponseMsg("Results not found for your search");
                            sendSyncSuccessEvent(url, syncListener, context, resp, requestTimestamp);
                            return;
                        }
                        else {
                            db.clearCompanyRelatedTables();
                            sendSyncSuccessEvent(url, syncListener, context, resp, requestTimestamp);
                            return;
                        }

                    }
                    sendSyncFailureEvent(url, syncListener, context, SERVER_ERROR, requestTimestamp);
                } catch (Exception e) {
                    e.printStackTrace();
                    sendSyncFailureEvent(url, syncListener, context, SERVER_ERROR, requestTimestamp);

                }
            }
        };

        RequestEntityByName req = new RequestEntityByName();
        req.setKey(key);//.toLowerCase()
        AsyncJsonPost asyncJsonPost = new AsyncJsonPost(context, Constants.URL_PROD_BY_NAME, objMapper.writeValueAsString(req), HttpHelper.EncodingType.PLAIN_TEXT, responseHandler);
        asyncJsonPost.setDownloadListener(buildDownloadListener(syncListener, context));
        if (Toolbox.isInternetAvailable(context))
            asyncJsonPost.execute();
        else {
            sendSyncFailureEvent(Constants.URL_PROD_BY_NAME, syncListener, context, Constants.ERROR_MESSAGE_INTERNET_UNAVAILABLE, 0);
        }
    }

    public static void getProductsByCompanyId(final Long id,final SyncListener syncListener,final Activity context,final DBManager db){
        sendSyncStartEvent(syncListener, context);
        ResponseHandler responseHandler = new ResponseHandler() {
            @Override
            public void handleStringResponse(String url, String response, long requestTimestamp) {
                try {
                    final ResponseSearchByProductName resp = objMapper.readValue(response, ResponseSearchByProductName.class);

                    if (resp != null && resp.getResponseCode() != Constants.RESPONSE_CODE_SUCCESS) {
                        //db.clearCompanyRelatedTables();
                        sendSyncFailureEvent(url, syncListener, context, resp.getResponseMsg(), requestTimestamp);
                        return;
                    }
                    if (resp.getResponseCode() == Constants.RESPONSE_CODE_SUCCESS) {
                        if (resp.getDataArray() != null) {
                       //     db.clearCompanyRelatedTables();
                            Toolbox.writeToLog("Size of datarrray " + resp.getDataArray().size());
                            db.insertProductsForCompany(resp.getDataArray());
                         //   db.insertCompanyDetailsForProduct(resp.getDataArray());
                            for(ProductDataArray pd:resp.getDataArray()){
                                db.insertfob(pd.getFobs());
                                db.insertMoq(pd.getMoqs());
                                db.insertPayt(pd.getPayT());

                            }
                            sendSyncSuccessEvent(url, syncListener, context, resp, requestTimestamp);
                            return;

                        } else {
                           // db.clearCompanyRelatedTables();
                            sendSyncSuccessEvent(url, syncListener, context, resp, requestTimestamp);
                            return;
                        }

                    }
                    sendSyncFailureEvent(url, syncListener, context, SERVER_ERROR, requestTimestamp);
                } catch (Exception e) {
                    e.printStackTrace();
                    sendSyncFailureEvent(url, syncListener, context, SERVER_ERROR, requestTimestamp);

                }
            }
        };

        try {
            RequestEntityyById req = new RequestEntityyById();
            req.setId(id);//.toLowerCase()
            AsyncJsonPost asyncJsonPost = new AsyncJsonPost(context, Constants.URL_PROD_FOR_COMPANY, objMapper.writeValueAsString(req), HttpHelper.EncodingType.PLAIN_TEXT, responseHandler);
            asyncJsonPost.setDownloadListener(buildDownloadListener(syncListener, context));
            if (Toolbox.isInternetAvailable(context))
                asyncJsonPost.execute();
            else {
                sendSyncFailureEvent(Constants.URL_PROD_FOR_COMPANY, syncListener, context, Constants.ERROR_MESSAGE_INTERNET_UNAVAILABLE, 0);
            }
        }catch (Exception e){
            e.printStackTrace();
            sendSyncFailureEvent(Constants.URL_PROD_FOR_COMPANY, syncListener, context, Constants.ERROR_MESSAGE_GENERIC, 0);
        }
    }

    public static void getBuyingLeads(final SyncListener syncListener,final Activity context,final DBManager db) {
        sendSyncStartEvent(syncListener, context);
        ResponseHandler responseHandler = new ResponseHandler() {
            @Override
            public void handleStringResponse(String url, String response, long requestTimestamp) {
                try {
                    final ResponseBuyLeads resp = objMapper.readValue(response, ResponseBuyLeads.class);

                    if (resp != null && resp.getResponseCode() != Constants.RESPONSE_CODE_SUCCESS) {

                        sendSyncFailureEvent(url, syncListener, context, resp.getResponseMsg(), requestTimestamp);
                        return;
                    }
                    if (resp.getResponseCode() == Constants.RESPONSE_CODE_SUCCESS) {
                        if (resp.getDataArray() != null) {
                            db.clearBuyLeads();
                            db.insertBuyingLeads(resp.getDataArray().get(0).getBuyingLeads());
                            sendSyncSuccessEvent(url, syncListener, context, resp, requestTimestamp);
                            return;

                        } else {
                            // db.clearCompanyRelatedTables();
                            sendSyncSuccessEvent(url, syncListener, context, resp, requestTimestamp);
                            return;
                        }

                    }
                    sendSyncFailureEvent(url, syncListener, context, SERVER_ERROR, requestTimestamp);
                } catch (Exception e) {
                    e.printStackTrace();
                    sendSyncFailureEvent(url, syncListener, context, Constants.ERROR_MESSAGE_GENERIC, requestTimestamp);

                }
            }
        };

        try {
            AsyncJsonPost asyncJsonPost = new AsyncJsonPost(context, Constants.URL_BUY_LEADS, null, HttpHelper.EncodingType.PLAIN_TEXT, responseHandler);
            asyncJsonPost.setDownloadListener(buildDownloadListener(syncListener, context));
            if (Toolbox.isInternetAvailable(context))
                asyncJsonPost.execute();
            else {
                sendSyncFailureEvent(Constants.URL_BUY_LEADS, syncListener, context, Constants.ERROR_MESSAGE_INTERNET_UNAVAILABLE, 0);
            }
        }catch (Exception e){
            e.printStackTrace();
            sendSyncFailureEvent(Constants.URL_BUY_LEADS, syncListener, context, Constants.ERROR_MESSAGE_GENERIC, 0);
        }
    }

    private static void sendSyncStartEvent(@Nullable final SyncListener syncListener, @Nullable Activity context) {
        if (syncListener != null && context != null) {
            context.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    syncListener.onSyncStart();
                }
            });
        }
    }


    private static void sendSyncFailureEvent(final String url, @Nullable final SyncListener syncListener, @Nullable Activity context, final String message, final long requestTimestamp) {
        if (syncListener != null && context != null) {

            context.runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    syncListener.onSyncFailure(url, message, requestTimestamp);
                }
            });
        }
    }

    private static void sendSyncSuccessEvent(final String url, @Nullable final SyncListener syncListener, @Nullable Activity context, final Object response, final long requestTimestamp) {
        if (syncListener != null && context != null) {
            context.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    float timeTaken = (System.currentTimeMillis() - requestTimestamp) / 1000f;
                    Toolbox.writeToLog(url + " Total time = " + timeTaken + " seconds");
                    syncListener.onSyncSuccess(url, response, requestTimestamp);
                }
            });
        }
    }

    @NotNull
    private static DownloadListener buildDownloadListener(final SyncListener syncListener, final Activity context) {

        return new DownloadListener() {
            @Override
            public void onDownloadStart(String url, long requestTimestamp) {

            }

            @Override
            public void onDownloadSuccess(String url, String response, int httpStatusCode, long requestTimestamp) {

            }

            @Override
            public void onDownloadSuccess(String url, byte[] response, long requestTimestamp) {

            }

            @Override
            public void onDownloadFail(String url, Exception e, String reason, long requestTimestamp) {
                sendSyncFailureEvent(url, syncListener, context, reason, requestTimestamp);
            }
        };

    }

    public static void readFileFromAssets(final Activity context,final String fileName,SyncListener listener) {
        // TODO Auto-generated method stub
        sendSyncStartEvent(listener,context);
        List<String>countries=new ArrayList<String>();
        StringBuffer sb = new StringBuffer();
        BufferedReader br=null;
        try {
            br = new BufferedReader(new InputStreamReader(context.getAssets().open(fileName)));
            String temp;
            while ((temp = br.readLine()) != null) {
                sb.append(temp);
                countries.add(temp);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                br.close(); // stop reading
            } catch (IOException e) {
                e.printStackTrace();
                sendSyncFailureEvent(null,listener,context,"Couldnt read file.",0l);
            }
        }
        Toolbox.writeToLog("countries read.."+sb.toString());
        sendSyncSuccessEvent(null, listener, context, countries, 0l);
//        return sb.toString();
    }


}