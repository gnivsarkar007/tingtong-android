package to.done.lib.sync;

/**
 * Created by dipen on 29/11/13.
 */
public interface ResponseHandler {

    void handleStringResponse(String url, String response, long requestTime);

}
