package to.done.lib.sync;

/**
 * Created by dipen on 29/11/13.
 */
public interface SyncListener {

    void onSyncStart();
    void onSyncProgress(float percentProgress,long requestTimestamp);
    void onSyncSuccess(String url,Object responseObject,long requestTimestamp);
    void onSyncFailure(String url,String reason,long requestTimestamp);


}
