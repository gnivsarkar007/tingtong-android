package to.done.lib.cart;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by dipenpradhan on 4/24/14.
 */
public class Cart {

    private static Cart cart;

    private List<CartListener> cartListeners=new LinkedList<CartListener>();
    boolean isPreorder;
    Date orderDate;
    String delivery_type;

    String couponCode;

    public String getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }

//    private Comparator<Product> productComparator =new Comparator<Product>() {
//        @Override
//        public int compare(Product product, Product product2) {
//            int result=product.getName().compareTo(product2.getName());
//
//            if(result==0){
//                if(product.getAdditionalProperties().get("custString")!=null && product2.getAdditionalProperties().get("custString")!=null) {
//                    return product.getAdditionalProperties().get("custString").toString().compareTo(product2.getAdditionalProperties().get("custString").toString());
//                }
//                else{
//                    return 0;
//                }
//            }
//            else{
//                return result;
//            }
//        }
//    };
//    private Comparator<Customization> customizationComparator =new Comparator<Customization>() {
//        @Override
//        public int compare(Customization customization, Customization customization2) {
//            return customization.getOutlet_product_id().compareTo(customization2.getOutlet_product_id());
//        }
//    };
//    public RespCheckOrder getRespCheckOrder() {
//        return respCheckOrder;
//    }
//
//    public void setRespCheckOrder(RespCheckOrder respCheckOrder) {
//        this.respCheckOrder = respCheckOrder;
//    }
//
//    public void clearCart(){
//
//        this.orders.clear();
//        sendCartEvent();
//        this.orderDate=null;
//        delivery_type="";
//        this.user=null;
//        this.respCheckOrder= new RespCheckOrder();
//    }
//
//    public User getUser() {
//        return user;
//    }
//
//    public void setUser(User user) {
//        this.user = user;
//    }
//
//    public String getDelivery_type() {
//        return delivery_type;
//    }
//
//    public void setDelivery_type(String delivery_type) {
//        this.delivery_type = delivery_type;
//    }
//
//    public Date getOrderDate() {
//        return orderDate;
//    }
//
//    public void setOrderDate(Date orderDate) {
//        this.orderDate = orderDate;
//    }
//
//    public boolean isPreorder() {
//        return isPreorder;
//    }
//
//    public void setPreorder(boolean isPreorder) {
//        this.isPreorder = isPreorder;
//    }
//
//    public static Cart getInstance() {
//
//        if(cart==null){
//            cart=new Cart();
//
//        }
//
//        return cart;
//    }
//
//    public synchronized int getProductsCount(long outletId){
//
//        if(orders.containsKey(outletId)){
//            return orders.get(outletId).getProducts().size();
//        }
//
//        return 0;
//    }
//
//    public synchronized int getProductQtyIgnoreCustomization(long outletId, long outlet_product_id){
//        int count=0;
//        if(orders.containsKey(outletId)){
//            for(Product p: orders.get(outletId).getProducts()){
//
//                if(p.getOutlet_product_id().longValue()==outlet_product_id){
//                    count++;
//                }
//            }
//
//        }
//
//        return count;
//
//    }
//    public synchronized int getProductQtyWithCustomization(long outletId, Product product){
//        int count=0;
//        if(orders.containsKey(outletId)){
//
//            for(Product p: orders.get(outletId).getProducts()){
//
//                if(p.getOutlet_product_id().longValue()==product.getOutlet_product_id()){
//                    int matchCount=0;
//
//                    for(Customization c:product.getCustomization()){
//
//                        for(Customization c2:p.getCustomization()){
//                            if(c.getOutlet_product_id().longValue()==c2.getOutlet_product_id().longValue()){
//                                matchCount++;
//                            }
//                        }
//
//                    }
//
//                    int requiredMatchCount;
//
//                    if(product.getCustomization().size()>p.getCustomization().size()){
//                        requiredMatchCount=product.getCustomization().size();
//                    }
//                    else{
//                        requiredMatchCount=p.getCustomization().size();
//                    }
//
//                    if(matchCount==requiredMatchCount){
//                        count++;
//                    }
//
//                }
//            }
//
//        }
//
//        return count;
//
//    }
//    public synchronized List<Product> getProductsWithoutDuplicate(long outletId){
//
//        List<Product> productList=new ArrayList<Product>();
//
//
//            if (orders.containsKey(outletId)) {
//
//                List<Product> cartProducts=new ArrayList<Product>(orders.get(outletId).getProducts());
//                List<Product> cartProducts2=new ArrayList<Product>(orders.get(outletId).getProducts());
//
//
//                while(cartProducts2.size()>0) {
//
//
//                    for (Product p : cartProducts) {
//
//                        ListIterator<Product> iterator = cartProducts2.listIterator();
//                        Product productToAdd = null;
//                        while (iterator.hasNext()) {
//                            Product p2 = iterator.next();
//                            int matchCount = 0;
//
//                            if (p.getCustomization().size()==0){
//                                if(p2.getCustomization().size()==0){
//                                    if(p.getOutlet_product_id()==p2.getOutlet_product_id()){
//                                        productToAdd = p;
//                                        iterator.remove();
//                                    }
//                                }
//                            }
//                            else{
//                                for (Customization c : p.getCustomization()) {
//
//                                    for (Customization c2 : p2.getCustomization()) {
//
//                                        if (c.getOutlet_product_id().longValue() == c2.getOutlet_product_id().longValue()) {
//                                            matchCount++;
//
//                                        }
//                                    }
//                                }
//
//
//                                int requiredMatchCount;
//                                if (p.getCustomization().size() > p2.getCustomization().size()) {
//                                    requiredMatchCount = p.getCustomization().size();
//                                } else {
//                                    requiredMatchCount = p2.getCustomization().size();
//                                }
//
//                                if (matchCount == requiredMatchCount) {
//                                    productToAdd = p;
//                                    iterator.remove();
//                                }
//
//                            }
//
//                            if (productToAdd != null) {
//                                if(!productList.contains(productToAdd))
//                                productList.add(productToAdd);
//                            }
//
//                        }
//                    }
//                }
//
//
//
//        }
//
//
//
//        for(Product p : productList){
//            if(p.getCustomization()!=null) {
//                StringBuilder custStringBuilder=new StringBuilder();
//
//                Collections.sort(p.getCustomization(), customizationComparator);
//
//                for (Customization c : p.getCustomization()){
//                    custStringBuilder.append(c.getOutlet_product_id());
//                    custStringBuilder.append(":");
//                }
//                p.getAdditionalProperties().put("custString",custStringBuilder.toString());
//
//            }
//
//        }
//
//        Collections.sort(productList, productComparator);
//
//        return productList;
//
//    }
//
//    public synchronized int getOrdersCount(){
//
//        return orders.keySet().size();
//    }
//
//    public synchronized Map<Long, Order> getOrders() {
//        return orders;
//    }
//
//
//    public synchronized double getTotal(){
//
//        double total=0d;
//
//        for(long outletId:orders.keySet()){
//            Order o=orders.get(outletId);
//
//            for(Product p:o.getProducts()){
//                for(Customization c:p.getCustomization()){
//                    if(c.getPrice()!=null)
//                       total+=c.getPrice().doubleValue();
//                }
//                total+=p.getPrice();
//            }
//
//        }
//        return total;
//    }
//
//    public synchronized Order getFirstOrder(){
//
//        for(long outletId:orders.keySet()){
//            Order o=orders.get(outletId);
//
//            return o;
//        }
//        return null;
//    }
//
//
//
//    public synchronized void addToCart(long companyId, long outletId, Product product){
////        if(mHandler==null)
////                mHandler= new Handler();
//        double total=product.getPrice();
//
//        if(product.getCustomization()!=null && product.getCustomization().size()>0) {
//            for (Customization c : product.getCustomization()) {
//                total += c.getPrice();
//            }
//        }
//        if(orders.containsKey(outletId)){
//            Order o=orders.get(outletId);
//            o.getProducts().add(product);
//            o.setTotal(total + o.getTotal().doubleValue());
//        }
//        else {
//            Order o = new Order(companyId, outletId);
//            o.getProducts().add(product);
//            orders.put(outletId, o);
//
//            o.setTotal(total + o.getTotal().doubleValue());
//        }
//        sendCartEvent();
//    }
//
//    public synchronized Product removeFromCart(long outletId, Product product){
//        Product productToRemove = null;
//        if(orders.containsKey(outletId)){
//            Order o=orders.get(outletId);
//
//            if(!o.getProducts().remove(product)) {
//               productToRemove = null;
//                for (Product p : o.getProducts()) {
//                    if (p.getOutlet_product_id().longValue() == product.getOutlet_product_id().longValue()) {
//                        productToRemove = p;
//                        productToRemove.setCustomization(p.getCustomization());
//                        productToRemove.setPrice(p.getPrice_with_customization());
//                    }
//                }
//                o.getProducts().remove(productToRemove);
//
//            }
//
//            if(o.getProducts().size()==0){
//                orders.remove(outletId);
//            }
//        }
//        sendCartEvent();
//        return productToRemove;
//    }
//    public synchronized void removeAllMatching(long outletId, Product product){
//
//        if(orders.containsKey(outletId)){
//            Order o=orders.get(outletId);
//
//            ListIterator<Product> iterator=o.getProducts().listIterator();
//
//            while(iterator.hasNext()){
//                Product p2=iterator.next();
//                int matchCount=0;
//                for(Customization c:product.getCustomization()){
//
//                    for(Customization c2:p2.getCustomization()){
//
//                        if(c.getOutlet_product_id().longValue()==c2.getOutlet_product_id().longValue()){
//                            matchCount++;
//                        }
//
//                    }
//
//                }
//
//                if(matchCount>0){
//                if(product.getCustomization().size()==matchCount){
//                    iterator.remove();
//                }}
//                else{
//                  if(product.getCustomization().size()==0 && p2.getCustomization().size()==0){
//                      if(product.getOutlet_product_id()==p2.getOutlet_product_id())
//                      iterator.remove();
//                  }
//                }
//
//            }
//
//            if(o.getProducts().size()==0){
//                orders.remove(outletId);
//            }
//        }
//        sendCartEvent();
//        Toolbox.writeToLog(orders.toString());
//
//    }
//
//    public void updateCartItem(){
//
//    }
//
//    public void addCartListener(CartListener cartListener){
//
//        cartListeners.add(cartListener);
//    }
//    public void removeCartListener(CartListener cartListener){
//
//        cartListeners.remove(cartListener);
//    }
//
//    public void sendCartEvent(){
//        for(CartListener cl:cartListeners){
//            cl.onCartChange();
//        }
//    }
//
//    @Override
//    public String toString() {
//        return "Cart{" +
//                "orders=" + orders +
//                '}';
//    }
//    Runnable cartEvent= new Runnable() {
//        @Override
//        public void run() {
//
//        }
//    };
}
