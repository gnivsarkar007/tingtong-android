package to.done.lib.ui;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.ScaleAnimation;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class LoadingViewWithThread extends View implements Runnable {

    public final int INITIAL_RADIUS = 10;
    ScaleAnimation scaleAnimation;
    Handler handler;
    @NotNull
    public String colorValue[] = new String[]{"#f13a49", "#00b6f1", "#ff8800", "#68b861", "#1d1d1d"};
    int counter = 0;
    int radius = 0;
    boolean drawingFlag;
    @NotNull
    Bitmap bmplogo[] = new Bitmap[colorValue.length];
    Bitmap bmpDoneLogo;
    @Nullable
    Thread t;
    boolean stopThread;
    long currentTime;

    public boolean isDrawingFlag() {
        return drawingFlag;
    }

    public void setDrawingFlag(boolean drawingFlag) {
        this.drawingFlag = drawingFlag;
    }

    public LoadingViewWithThread(@NotNull Context context, AttributeSet attrs) {
        super(context, attrs);
        // TODO Auto-generated constructor stub
    }

    public LoadingViewWithThread(Context context) {
        super(context);
        float pixels = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                150, getResources().getDisplayMetrics());
        ViewGroup.LayoutParams lp = new ViewGroup.LayoutParams((int) pixels, (int) pixels);
        setLayoutParams(lp);

    }


    @Override
    protected void onDraw(@NotNull Canvas canvas) {
        // TODO Auto-generated method stub
        super.onDraw(canvas);

        Paint p = new Paint();
        p.setAntiAlias(true);
        p.setColor(Color.WHITE);
        p.setStrokeWidth(4);
        canvas.drawCircle(getWidth() / 2, getHeight() / 2, (getWidth() / 4) - 2, p);

        int counter1 = counter - 1;
        if (counter1 < 0)
            counter1 = colorValue.length - 1;
        p.setStyle(Paint.Style.FILL);
        p.setColor(Color.parseColor(colorValue[counter1]));
        canvas.drawCircle(getWidth() / 2, getHeight() / 2, (getWidth() / 2) - 5, p);

        p.setColor(Color.parseColor(colorValue[counter]));
        canvas.drawCircle(getWidth() / 2, getHeight() / 2, radius - 5, p);

        if (!stopThread && isDrawingFlag())
            canvas.drawBitmap(bmplogo[counter], (getWidth() / 2) - (bmplogo[counter].getWidth() / 2), (getHeight() / 2) - (bmplogo[counter].getHeight() / 2), p);
        else {
            if (!isDrawingFlag())
                stopThread = false;
            canvas.drawBitmap(bmpDoneLogo, (getWidth() / 2) - (bmplogo[counter].getWidth() / 2), (getHeight() / 2) - (bmplogo[counter].getHeight() / 2), p);
        }

    }

    public void startLoadingAnimation() {
//        radius = INITIAL_RADIUS;
//        bmplogo[0] = BitmapFactory.decodeResource(getResources(), R.drawable.loading_food_1);
//        bmplogo[1] = BitmapFactory.decodeResource(getResources(), R.drawable.loading_food_2);
//        bmplogo[2] = BitmapFactory.decodeResource(getResources(), R.drawable.loading_food_3);
//        bmplogo[3] = BitmapFactory.decodeResource(getResources(), R.drawable.loading_food_4);
//        bmplogo[4] = BitmapFactory.decodeResource(getResources(), R.drawable.loading_food_5);
        t = null;
        stopThread = false;
        setDrawingFlag(true);

        handler = new Handler() {
            public void handleMessage(Message msg) {
                radius += 1;
                if (radius > getWidth() / 2) {
                    radius = INITIAL_RADIUS;
                    counter++;
                    if (counter >= colorValue.length)
                        counter = 0;
                }
                postInvalidate();
            }
        };
        t = new Thread(this);
        t.start();
    }

    public void stopAnimation() {
      //  bmpDoneLogo = BitmapFactory.decodeResource(getResources(), R.drawable.logo_shape);
        stopThread = true;
        currentTime = System.currentTimeMillis();
    }

    @Override
    public void run() {
        // TODO Auto-generated method stub
        while (drawingFlag) {
            try {
                if ((System.currentTimeMillis() - currentTime) >= 2000 && radius >= (getWidth() / 2 - 2) && stopThread) {
                    setDrawingFlag(false);
                }
                Thread.sleep(8);
                handler.sendMessage(handler.obtainMessage());
            } catch (Exception e) {
                Log.d("Chandni", "Exception e=" + e);
            }
        }
    }

}
