package to.done.lib.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;

import org.jetbrains.annotations.NotNull;

import java.util.LinkedList;

import to.done.lib.R;

/**
 * Created by dipen on 4/12/13.
 */
public class FontEditText extends EditText{

    private String fontNameFromXML;
    private int type;

    public FontEditText(@NotNull Context context, @NotNull AttributeSet attrs, int defStyle) {
        this(context,attrs);
    }

    public FontEditText(@NotNull Context context, @NotNull AttributeSet attrs) {
        super(context, attrs);

        if (!isInEditMode()) {
            TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.font);
            fontNameFromXML = a.getString(R.styleable.font_fontName);
            if(fontNameFromXML!=null)
                setTypeface(FontManager.getTypeface(getContext(), fontNameFromXML));

        }
    }

    public FontEditText(@NotNull Context context) {
        this(context,null);
    }


    @Override
    public void onEditorAction(int actionCode) {
        super.onEditorAction(actionCode);
        if(actionCode== EditorInfo.IME_ACTION_DONE)
        {
//            setSelection(getText().length());
//			setEnabled(false);
            cancelEdit(0);
        }
    }


    @Override
    public boolean onKeyPreIme(int keyCode, @NotNull KeyEvent event) {

        if(keyCode==KeyEvent.KEYCODE_BACK && event.getAction()==KeyEvent.ACTION_UP){
//            setSelection(getText().length());
//			setEnabled(false);
            cancelEdit(0);
//
            return false;
        }

        return super.onKeyPreIme(keyCode, event);
    }





    @Override
    protected void onFocusChanged(boolean focused, int direction,
                                  Rect previouslyFocusedRect) {


        if(!focused)
        {
//			setEnabled(false);
            cancelEdit(0);
        }
        else
        {

        }

        super.onFocusChanged(focused, direction, previouslyFocusedRect);
    }

    @NotNull
    private LinkedList<EditCancelListener> editCancelListenerList=new LinkedList<EditCancelListener>();

    public void addEditCancelListener(EditCancelListener listener){
        editCancelListenerList.add(listener);
    }

    public boolean removeEditCancelListener(EditCancelListener listener){
        return editCancelListenerList.remove(listener);
    }
    public void cancelEdit(int cancelCode) {
        for(EditCancelListener ecl: editCancelListenerList){
            ecl.onCancelEvent(this,cancelCode);
        }

    }


    public int getType() {
        return type;
    }
    public void setType(int type) {
        this.type = type;
    }


    public interface EditCancelListener {

        public void onCancelEvent(FontEditText editText, int cancelCode);

    }

}
