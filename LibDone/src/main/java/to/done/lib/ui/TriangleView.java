package to.done.lib.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.util.AttributeSet;
import android.view.View;

import org.jetbrains.annotations.NotNull;

import to.done.lib.R;

/**
 * Created by dipenpradhan on 4/17/14.
 */
public class TriangleView extends View {

    private static final int LEFT=1,RIGHT=2,TOP=3,BOTTOM=4;
    private int color,baseLocation;

    public TriangleView(Context context) {
        super(context);
    }

    public TriangleView(@NotNull Context context, @NotNull AttributeSet attrs) {
        super(context, attrs);

        if (!isInEditMode()) {
            TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.TriangleView);
            color = a.getColor(R.styleable.TriangleView_triangleColor,0);
            a.recycle();
        }
    }

    public TriangleView(@NotNull Context context, @NotNull AttributeSet attrs, int defStyleAttr) {
        this(context, attrs);
    }

    protected void onDraw(@NotNull Canvas canvas) {
        super.onDraw(canvas);


        if(isInEditMode())
            return;

        Paint paint = new Paint();

//        paint.setColor(android.graphics.Color.BLACK);
//        canvas.drawPaint(paint);

        paint.setStrokeWidth(1);
        paint.setColor(color);
        paint.setStyle(Paint.Style.FILL_AND_STROKE);
        paint.setAntiAlias(true);

        Point a = new Point(0, 0);
        Point b = new Point(getWidth(),0);
        Point c = new Point((int)(0.5f*getWidth()),getHeight());

        Path path = new Path();
        path.setFillType(Path.FillType.EVEN_ODD);
        path.moveTo(a.x,a.y);
        path.lineTo(b.x, b.y);
        path.lineTo(c.x, c.y);
        path.lineTo(a.x, a.y);
        path.close();

        canvas.drawPath(path, paint);
    }
}
