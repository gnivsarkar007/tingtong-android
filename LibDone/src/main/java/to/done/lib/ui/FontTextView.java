package to.done.lib.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.TextView;

import org.jetbrains.annotations.NotNull;

import to.done.lib.R;

/**
 * Created by dipen on 4/12/13.
 */
public class FontTextView extends TextView{


    public FontTextView(@NotNull Context context, @NotNull AttributeSet attrs, int defStyle) {
        this(context, attrs);
    }

    public FontTextView(@NotNull Context context, @NotNull AttributeSet attrs) {
        super(context, attrs);
        String fontNameFromXML=null;
        if (!isInEditMode()) {
            TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.font);
            fontNameFromXML = a.getString(R.styleable.font_fontName);

            if(fontNameFromXML!=null)
                setTypeface(FontManager.getTypeface(getContext(), fontNameFromXML));
        a.recycle();
        }
    }

    public FontTextView(@NotNull Context context) {
        this(context,null);
    }



}
