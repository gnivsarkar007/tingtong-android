package to.done.lib.ui;

import android.content.Context;
import android.graphics.Typeface;

import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Map;
/**
 * @author Dipen Pradhan - Movivation Labs Pvt Ltd. (dipen@movivation.com)
 */
 
public class FontManager {
 
    private static Typeface rupeeTypeFace;
    private static final String RUPEE_FONT_PATH="fonts/rupee.ttf";
    private static final String PATH_FONT="fonts/";
    @NotNull
    private static Map<String,Typeface> fonts=new HashMap<String, Typeface>();
    public static Typeface getRupeeTypeFace(@NotNull Context context) {

        if(rupeeTypeFace==null)
        {
            rupeeTypeFace=Typeface.createFromAsset(context.getAssets(), RUPEE_FONT_PATH);
        }

        return rupeeTypeFace;
    }
    public static Typeface getTypeface(@NotNull Context context, String fontNameFromXML) {
        if(fonts.containsKey(fontNameFromXML)){
            return fonts.get(fontNameFromXML);
        }
        else
        {
            fonts.put(fontNameFromXML,Typeface.createFromAsset(context.getAssets(), PATH_FONT+fontNameFromXML));

            return fonts.get(fontNameFromXML);
        }


    }


}