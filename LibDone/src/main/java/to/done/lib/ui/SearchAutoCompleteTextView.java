package to.done.lib.ui;

import android.content.Context;
import android.util.AttributeSet;

import org.jetbrains.annotations.NotNull;

/**
 * Created by dipen on 16/4/14.
 */
public class SearchAutoCompleteTextView extends FontAutoCompleteTextView {

    public SearchAutoCompleteTextView(@NotNull Context context) {
        super(context);
    }

    public SearchAutoCompleteTextView(@NotNull Context context, @NotNull AttributeSet attrs) {
        super(context, attrs);
    }

    public SearchAutoCompleteTextView(@NotNull Context context, @NotNull AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void performFiltering(CharSequence text, int keyCode) {

    }


}
