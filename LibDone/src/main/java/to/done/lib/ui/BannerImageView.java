package to.done.lib.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;

import to.done.lib.entity.Banner;
import to.done.lib.images.util.ImageFetcher;

/**
 * Created by HP on 10/11/2014.
 */
public class BannerImageView extends ImageView {
    int fadeInDuration = 500; // Configure time values here
    int timeBetween = 3000;
    int fadeOutDuration = 1000;
    boolean forever;
    ImageFetcher fetcher;
    public BannerImageView(Context context) {
        super(context);
        init();

    }
private void init(){
    fetcher=new ImageFetcher(this.getContext(),300,30);
//    fetcher.setLoadingImage(R.color.transparent);
    fetcher.setImageFadeIn(true);
}
    public BannerImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public BannerImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public int getFadeInDuration() {
        return fadeInDuration;
    }

    public void setFadeInDuration(int fadeInDuration) {
        this.fadeInDuration = fadeInDuration;
    }

    public int getTimeBetween() {
        return timeBetween;
    }

    public void setTimeBetween(int timeBetween) {
        this.timeBetween = timeBetween;
    }

    public int getFadeOutDuration() {
        return fadeOutDuration;
    }

    public void setFadeOutDuration(int fadeOutDuration) {
        this.fadeOutDuration = fadeOutDuration;
    }


    public boolean isForever() {
        return forever;
    }

    public void setForever(boolean forever) {
        this.forever = forever;
    }

    public void animate(final Banner images[], final int imageIndex){

        //imageView <-- The View which displays the images
        //images[] <-- Holds R references to the images to display
        //imageIndex <-- index of the first image to show in images[]
        //forever <-- If equals true then after the last image it starts all over again with the first image resulting in an infinite loop. You have been warned.



        this.setVisibility(View.INVISIBLE); //Visible of invisible by default
//        this.setImageResource(images[imageIndex]);
        if(images[imageIndex]!=null) {
            fetcher.loadImage(images[imageIndex].getBannerlink(), this);
            this.setVisibility(View.VISIBLE);
        }
            this.setTag(images[imageIndex]);

        Animation fadeIn = new AlphaAnimation(0, 1);
        fadeIn.setInterpolator(new DecelerateInterpolator()); //Add this
        fadeIn.setDuration(fadeInDuration);

        Animation fadeOut = new AlphaAnimation(1, 0);
        fadeOut.setInterpolator(new AccelerateInterpolator()); //and this
        fadeOut.setStartOffset(fadeInDuration + timeBetween);
        fadeOut.setDuration(fadeOutDuration);

        AnimationSet animation = new AnimationSet(false); //change to false
        animation.addAnimation(fadeIn);
        animation.addAnimation(fadeOut);
        animation.setRepeatCount(1);
        this.setAnimation(animation);

        animation.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationEnd(Animation animation) {
                if(images.length -1 > imageIndex){
                    animate(images, imageIndex + 1); // Calls itself until it gets to the end of the array.
                }
                else{
                    if(forever == true){
                        animate(images, 0); //Calls itself to start the animation all over again in a loop if forever = true
                    }
                }

            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onAnimationStart(Animation animation) {
                // TODO Auto-generated method stub

            }
        });
    }
}
