package to.done.lib.ui;

/**
 * Created by dipen on 2/12/13.
 */
public interface DialogListener {

    void onPositiveBtnClick();
    void onNegativeBtnClick();
    void onMiddleBtnClick();
}
