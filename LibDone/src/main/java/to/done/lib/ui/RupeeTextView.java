package to.done.lib.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

import org.jetbrains.annotations.NotNull;

/**
 * Created by dipen on 4/12/13.
 */
public class RupeeTextView extends TextView{

    public RupeeTextView(@NotNull Context context) {
        super(context);
        init(context);
    }

    public RupeeTextView(@NotNull Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public RupeeTextView(@NotNull Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }


    private void init(@NotNull Context context){
        setTypeface(FontManager.getRupeeTypeFace(context));
        setText("`");
    }

}
