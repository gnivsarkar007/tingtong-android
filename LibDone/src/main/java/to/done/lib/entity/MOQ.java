package to.done.lib.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by HP on 9/14/2014.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "moq_qty",
        "moq",
        "product_id"
})
@DatabaseTable
public class MOQ {
    @JsonProperty("id")
    @DatabaseField(id=true)
    Long id;
    @JsonProperty("moq")
    @DatabaseField
    String moq;
    @JsonProperty("product_id")
    @DatabaseField
    Long product_id;
    @JsonProperty("moq_qty")
    @DatabaseField
    private String moqQty;

    @JsonProperty("moq_qty")
    public String getMoqQty() {
        return moqQty;
    }

    @JsonProperty("moq_qty")
    public void setMoqQty(String moqQty) {
        this.moqQty = moqQty;
    }

    @JsonProperty("id")
    public Long getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Long id) {
        this.id = id;
    }

    @JsonProperty("moq")
    public String getMoq() {
        return moq;
    }
    @JsonProperty("moq")
    public void setMoq(String moq) {
        this.moq = moq;
    }
    @JsonProperty("product_id")
    public Long getProduct_id() {
        return product_id;
    }
    @JsonProperty("product_id")
    public void setProduct_id(Long product_id) {
        this.product_id = product_id;
    }
}
