package to.done.lib.entity;

/**
 * Created by HP on 8/10/2014.
 */

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "name",
        "description",
        "rating",
        "image_url",
        "sort_no",
        "payment_status",
        "row_status",
        "latitude",
        "longitude",
        "address_id",
        "date_created",
        "date_modified",
        "addressid",
        "flat",
        "building",
        "street",
        "landmark",
        "pincode",
        "state_id",
        "city_id",
        "country_id",
        "city",
        "state",
        "country",
        "mobile",
        "email",
        "area_name",
        "linkid",
        "link_url",
        "link_type"
})
public class CompanyDetailforProductSearch extends Companydetail{

    @Override
    public String toString() {
        return "CompanyDetailforProductSearch{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", callnos='" + callnos + '\'' +
                ", phone='" + phone
                ;
    }

    @JsonProperty("id")
    private Long id;
    @JsonProperty("name")
    private String name;
    @JsonProperty("description")
    private String description;
    @JsonProperty("rating")
    private Float rating;
    @JsonProperty("image_url")
    private String imageUrl;
    @JsonProperty("sort_no")
    private Long sortNo;
    @JsonProperty("payment_status")
    private String paymentStatus;
    @JsonProperty("row_status")
    private String rowStatus;
    @JsonProperty("latitude")
    private Double latitude;
    @JsonProperty("longitude")
    private Double longitude;
    @JsonProperty("address_id")
    private Long addressId;
    @JsonProperty("doj")
    private String doj;
    @JsonProperty("expirydate")
    private String expirydate;
    @JsonProperty("verification")
    private String verification;
    @JsonProperty("rated")
    private String rated;
    @JsonProperty("supplier")
    private String supplier;
    @JsonProperty("date_created")
    private Object dateCreated;
    @JsonProperty("date_modified")
    private String dateModified;
    @JsonProperty("addressid")
    private Long addressid;
    @JsonProperty("flat")
    private String flat;
    @JsonProperty("building")
    private String building;
    @JsonProperty("street")
    private String street;
    @JsonProperty("landmark")
    private String landmark;
    @JsonProperty("pincode")
    private String pincode;
    @JsonProperty("area_name")
    private String areaName;
    @JsonProperty("state_id")
    private Long stateId;
    @JsonProperty("city_id")
    private Long cityId;
    @JsonProperty("country_id")
    private Long countryId;
    @JsonProperty("city")
    private String city;
    @JsonProperty("state")
    private String state;
    @JsonProperty("country")
    private String country;
    @JsonProperty("callnos")
    private String callnos;
    @JsonProperty("phone")
    private String phone;
    @JsonProperty("email")
    private String email;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     *
     * @return
     * The id
     */
    @JsonProperty("id")
    public Long getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    @JsonProperty("id")
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The name
     */
    @JsonProperty("name")
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The description
     */
    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    /**
     *
     * @param description
     * The description
     */
    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     *
     * @return
     * The rating
     */
    @JsonProperty("rating")
    public Float getRating() {
        return rating;
    }

    /**
     *
     * @param rating
     * The rating
     */
    @JsonProperty("rating")
    public void setRating(Float rating) {
        this.rating = rating;
    }

    /**
     *
     * @return
     * The imageUrl
     */
    @JsonProperty("image_url")
    public String getImageUrl() {
        return imageUrl;
    }

    /**
     *
     * @param imageUrl
     * The image_url
     */
    @JsonProperty("image_url")
    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    /**
     *
     * @return
     * The sortNo
     */
    @JsonProperty("sort_no")
    public Long getSortNo() {
        return sortNo;
    }

    /**
     *
     * @param sortNo
     * The sort_no
     */
    @JsonProperty("sort_no")
    public void setSortNo(Long sortNo) {
        this.sortNo = sortNo;
    }

    /**
     *
     * @return
     * The paymentStatus
     */
    @JsonProperty("payment_status")
    public String getPaymentStatus() {
        return paymentStatus;
    }

    /**
     *
     * @param paymentStatus
     * The payment_status
     */
    @JsonProperty("payment_status")
    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    /**
     *
     * @return
     * The rowStatus
     */
    @JsonProperty("row_status")
    public String getRowStatus() {
        return rowStatus;
    }

    /**
     *
     * @param rowStatus
     * The row_status
     */
    @JsonProperty("row_status")
    public void setRowStatus(String rowStatus) {
        this.rowStatus = rowStatus;
    }

    /**
     *
     * @return
     * The latitude
     */
    @JsonProperty("latitude")
    public Double getLatitude() {
        return latitude;
    }

    /**
     *
     * @param latitude
     * The latitude
     */
    @JsonProperty("latitude")
    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    /**
     *
     * @return
     * The longitude
     */
    @JsonProperty("longitude")
    public Double getLongitude() {
        return longitude;
    }

    /**
     *
     * @param longitude
     * The longitude
     */
    @JsonProperty("longitude")
    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    /**
     *
     * @return
     * The addressId
     */
    @JsonProperty("address_id")
    public Long getAddressId() {
        return addressId;
    }

    /**
     *
     * @param addressId
     * The address_id
     */
    @JsonProperty("address_id")
    public void setAddressId(Long addressId) {
        this.addressId = addressId;
    }

    /**
     *
     * @return
     * The doj
     */
    @JsonProperty("doj")
    public String getDoj() {
        return doj;
    }

    /**
     *
     * @param doj
     * The doj
     */
    @JsonProperty("doj")
    public void setDoj(String doj) {
        this.doj = doj;
    }

    /**
     *
     * @return
     * The expirydate
     */
    @JsonProperty("expirydate")
    public String getExpirydate() {
        return expirydate;
    }

    /**
     *
     * @param expirydate
     * The expirydate
     */
    @JsonProperty("expirydate")
    public void setExpirydate(String expirydate) {
        this.expirydate = expirydate;
    }

    /**
     *
     * @return
     * The verification
     */
    @JsonProperty("verification")
    public String getVerification() {
        return verification;
    }

    /**
     *
     * @param verification
     * The verification
     */
    @JsonProperty("verification")
    public void setVerification(String verification) {
        this.verification = verification;
    }

    /**
     *
     * @return
     * The rated
     */
    @JsonProperty("rated")
    public String getRated() {
        return rated;
    }

    /**
     *
     * @param rated
     * The rated
     */
    @JsonProperty("rated")
    public void setRated(String rated) {
        this.rated = rated;
    }

    /**
     *
     * @return
     * The supplier
     */
    @JsonProperty("supplier")
    public String getSupplier() {
        return supplier;
    }

    /**
     *
     * @param supplier
     * The supplier
     */
    @JsonProperty("supplier")
    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    /**
     *
     * @return
     * The dateCreated
     */
    @JsonProperty("date_created")
    public Object getDateCreated() {
        return dateCreated;
    }

    /**
     *
     * @param dateCreated
     * The date_created
     */
    @JsonProperty("date_created")
    public void setDateCreated(Object dateCreated) {
        this.dateCreated = dateCreated;
    }

    /**
     *
     * @return
     * The dateModified
     */
    @JsonProperty("date_modified")
    public String getDateModified() {
        return dateModified;
    }

    /**
     *
     * @param dateModified
     * The date_modified
     */
    @JsonProperty("date_modified")
    public void setDateModified(String dateModified) {
        this.dateModified = dateModified;
    }

    /**
     *
     * @return
     * The addressid
     */
    @JsonProperty("addressid")
    public Long getAddressid() {
        return addressid;
    }

    /**
     *
     * @param addressid
     * The addressid
     */
    @JsonProperty("addressid")
    public void setAddressid(Long addressid) {
        this.addressid = addressid;
    }

    /**
     *
     * @return
     * The flat
     */
    @JsonProperty("flat")
    public String getFlat() {
        return flat;
    }

    /**
     *
     * @param flat
     * The flat
     */
    @JsonProperty("flat")
    public void setFlat(String flat) {
        this.flat = flat;
    }

    /**
     *
     * @return
     * The building
     */
    @JsonProperty("building")
    public String getBuilding() {
        return building;
    }

    /**
     *
     * @param building
     * The building
     */
    @JsonProperty("building")
    public void setBuilding(String building) {
        this.building = building;
    }

    /**
     *
     * @return
     * The street
     */
    @JsonProperty("street")
    public String getStreet() {
        return street;
    }

    /**
     *
     * @param street
     * The street
     */
    @JsonProperty("street")
    public void setStreet(String street) {
        this.street = street;
    }

    /**
     *
     * @return
     * The landmark
     */
    @JsonProperty("landmark")
    public String getLandmark() {
        return landmark;
    }

    /**
     *
     * @param landmark
     * The landmark
     */
    @JsonProperty("landmark")
    public void setLandmark(String landmark) {
        this.landmark = landmark;
    }

    /**
     *
     * @return
     * The pincode
     */
    @JsonProperty("pincode")
    public String getPincode() {
        return pincode;
    }

    /**
     *
     * @param pincode
     * The pincode
     */
    @JsonProperty("pincode")
    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    /**
     *
     * @return
     * The areaName
     */
    @JsonProperty("area_name")
    public String getAreaName() {
        return areaName;
    }

    /**
     *
     * @param areaName
     * The area_name
     */
    @JsonProperty("area_name")
    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    /**
     *
     * @return
     * The stateId
     */
    @JsonProperty("state_id")
    public Long getStateId() {
        return stateId;
    }

    /**
     *
     * @param stateId
     * The state_id
     */
    @JsonProperty("state_id")
    public void setStateId(Long stateId) {
        this.stateId = stateId;
    }

    /**
     *
     * @return
     * The cityId
     */
    @JsonProperty("city_id")
    public Long getCityId() {
        return cityId;
    }

    /**
     *
     * @param cityId
     * The city_id
     */
    @JsonProperty("city_id")
    public void setCityId(Long cityId) {
        this.cityId = cityId;
    }

    /**
     *
     * @return
     * The countryId
     */
    @JsonProperty("country_id")
    public Long getCountryId() {
        return countryId;
    }

    /**
     *
     * @param countryId
     * The country_id
     */
    @JsonProperty("country_id")
    public void setCountryId(Long countryId) {
        this.countryId = countryId;
    }

    /**
     *
     * @return
     * The city
     */
    @JsonProperty("city")
    public String getCity() {
        return city;
    }

    /**
     *
     * @param city
     * The city
     */
    @JsonProperty("city")
    public void setCity(String city) {
        this.city = city;
    }

    /**
     *
     * @return
     * The state
     */
    @JsonProperty("state")
    public String getState() {
        return state;
    }

    /**
     *
     * @param state
     * The state
     */
    @JsonProperty("state")
    public void setState(String state) {
        this.state = state;
    }

    /**
     *
     * @return
     * The country
     */
    @JsonProperty("country")
    public String getCountry() {
        return country;
    }

    /**
     *
     * @param country
     * The country
     */
    @JsonProperty("country")
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     *
     * @return
     * The callnos
     */
    @JsonProperty("callnos")
    public String getCallnos() {
        return callnos;
    }

    /**
     *
     * @param callnos
     * The callnos
     */
    @JsonProperty("callnos")
    public void setCallnos(String callnos) {
        this.callnos = callnos;
    }

    /**
     *
     * @return
     * The phone
     */
    @JsonProperty("phone")
    public String getPhone() {
        return phone;
    }

    /**
     *
     * @param phone
     * The phone
     */
    @JsonProperty("phone")
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     *
     * @return
     * The email
     */
    @JsonProperty("email")
    public String getEmail() {
        return email;
    }

    /**
     *
     * @param email
     * The email
     */
    @JsonProperty("email")
    public void setEmail(String email) {
        this.email = email;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
