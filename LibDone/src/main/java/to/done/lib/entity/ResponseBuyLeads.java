package to.done.lib.entity;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by gauravnivsarkar on 11/29/14.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "responseCode",
        "responseMsg",
        "DataArray"
})
public class ResponseBuyLeads {
    @JsonProperty("responseCode")
    private Long responseCode;
    @JsonProperty("responseMsg")
    private String responseMsg;
    @JsonProperty("DataArray")
    private List<BuyingLeadDataArray> DataArray = new ArrayList<BuyingLeadDataArray>();
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     *
     * @return
     * The responseCode
     */
    @JsonProperty("responseCode")
    public Long getResponseCode() {
        return responseCode;
    }

    /**
     *
     * @param responseCode
     * The responseCode
     */
    @JsonProperty("responseCode")
    public void setResponseCode(Long responseCode) {
        this.responseCode = responseCode;
    }

    /**
     *
     * @return
     * The responseMsg
     */
    @JsonProperty("responseMsg")
    public String getResponseMsg() {
        return responseMsg;
    }

    /**
     *
     * @param responseMsg
     * The responseMsg
     */
    @JsonProperty("responseMsg")
    public void setResponseMsg(String responseMsg) {
        this.responseMsg = responseMsg;
    }

    /**
     *
     * @return
     * The DataArray
     */
    @JsonProperty("DataArray")
    public List<BuyingLeadDataArray> getDataArray() {
        return DataArray;
    }

    /**
     *
     * @param DataArray
     * The DataArray
     */
    @JsonProperty("DataArray")
    public void setDataArray(List<BuyingLeadDataArray> DataArray) {
        this.DataArray = DataArray;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
}
