package to.done.lib.entity;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "link_url",
        "link_type",
        "row_status",
        "date_created",
        "date_modified"
})
@DatabaseTable
public class Link {
    @DatabaseField(id=true)
    @JsonProperty("id")
    private Long id;
    @DatabaseField
    @JsonProperty("link_url")
    private String linkUrl;
    @DatabaseField
    @JsonProperty("link_type")
    private String linkType;
    @DatabaseField
    @JsonProperty("row_status")
    private String rowStatus;
    @NotNull
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    @JsonIgnore
    @JsonProperty("company_id")
    @DatabaseField
    private Long company_id;

    @JsonProperty("company_id")
    public Long getCompany_id() {
        return company_id;
    }

    @JsonProperty("company_id")
    public void setCompany_id(Long company_id) {
        this.company_id = company_id;
    }

    @JsonProperty("id")
    public Long getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Long id) {
        this.id = id;
    }

    @JsonProperty("link_url")
    public String getLinkUrl() {
        return linkUrl;
    }

    @JsonProperty("link_url")
    public void setLinkUrl(String linkUrl) {
        this.linkUrl = linkUrl;
    }

    @JsonProperty("link_type")
    public String getLinkType() {
        return linkType;
    }

    @JsonProperty("link_type")
    public void setLinkType(String linkType) {
        this.linkType = linkType;
    }

    @JsonProperty("row_status")
    public String getRowStatus() {
        return rowStatus;
    }

    @JsonProperty("row_status")
    public void setRowStatus(String rowStatus) {
        this.rowStatus = rowStatus;
    }

    @NotNull
    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}