package to.done.lib.entity;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by HP on 7/21/2014.
 */
@JsonPropertyOrder({
        "id",
        "name",
        "description",
        "rating",
        "image_url",
        "sort_no",
        "payment_status",
        "row_status",
        "latitude",
        "longitude",
        "address_id",
        "doj",
        "expirydate",
        "verification",
        "rated",
        "supplier",
        "date_created",
        "date_modified"
})
@DatabaseTable
public class Companydetail {
    @DatabaseField(id=true)
    @JsonProperty("id")
    private Long id;
    @DatabaseField
    @JsonProperty("name")
    private String name;
    @DatabaseField
    @JsonProperty("image_url")
    private String imageUrl;
    @DatabaseField
    @JsonProperty("description")
    private String description;

    @DatabaseField
    @JsonProperty("row_status")
    private String rowStatus;
    @DatabaseField
    @JsonProperty("payment_status")
    private String paymentStatus;
    @DatabaseField
    @JsonProperty("latitude")
    private Double latitude;
    @DatabaseField
    @JsonProperty("longitude")
    private Double longitude;
    @DatabaseField
    @JsonProperty("sort_no")
    private Long sortNo;
    @DatabaseField
    @JsonProperty("address_id")
    private Long addressId;
    @JsonProperty("address")
    private Address address;
    @JsonProperty("products")
    private List<Product> products = new ArrayList<Product>();
    @DatabaseField
    @JsonProperty("phone")
    private String phoneNos;// = new ArrayList<String>();
    @DatabaseField
    @JsonProperty("email")
    private String emails ;//= new ArrayList<String>();
    @JsonProperty("links")
    private List<Link> links = new ArrayList<Link>();
    @DatabaseField
    @JsonProperty("contact_name")
    private String contactPerson;
    @DatabaseField
    @JsonProperty("verification")
    private String verification;
    @DatabaseField
    @JsonProperty("rated")
    private String rated;
    @DatabaseField
    @JsonProperty("rating")
    private Float rating;
    @DatabaseField
    @JsonProperty("supplier")
    String supplier;

    @DatabaseField
    @JsonProperty("callnos")
    private String callnos;
    @DatabaseField
    @JsonProperty("telephonefax")
    private String telephonefax;
    @DatabaseField
    @JsonProperty("mobile")
    private String mobile;

    @JsonProperty("callnos")
    public String getCallnos() {
        return callnos;
    }
    @JsonProperty("callnos")
    public void setCallnos(String callnos) {
        this.callnos = callnos;
    }

    @JsonProperty("telephonefax")
    public String getTelephonefax() {
        return telephonefax;
    }
    @JsonProperty("telephonefax")
    public void setTelephonefax(String telephonefax) {
        this.telephonefax = telephonefax;
    }

    @JsonProperty("mobile")
    public String getMobile() {
        return mobile;
    }
    @JsonProperty("mobile")
    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    @NotNull
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    @JsonProperty("verification")
    public String getVerification() {
        return verification;
    }

    @JsonProperty("verification")
    public void setVerification(String verification) {
        this.verification = verification;
    }

    @JsonProperty("rated")
    public String getRated() {
        return rated;
    }

    @JsonProperty("rated")
    public void setRated(String rated) {
        this.rated = rated;
    }

    @JsonProperty("supplier")
    public String getSupplier() {
        return supplier;
    }

    @JsonProperty("supplier")
    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    @JsonProperty("id")
    public Long getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Long id) {
        this.id = id;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("image_url")
    public String getImageUrl() {
        return imageUrl;
    }

    @JsonProperty("image_url")
    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
    @JsonProperty("rating")
    public Float getRating() {
        return rating;
    }
    @JsonProperty("rating")
    public void setRating(String rating) {
        this.rating = Float.parseFloat(rating);
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }


    @JsonProperty("row_status")
    public String getRowStatus() {
        return rowStatus;
    }

    @JsonProperty("row_status")
    public void setRowStatus(String rowStatus) {
        this.rowStatus = rowStatus;
    }

    @JsonProperty("payment_status")
    public String getPaymentStatus() {
        return paymentStatus;
    }

    @JsonProperty("payment_status")
    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    @JsonProperty("contact_name")
    public String getContactPerson() {
        return contactPerson;
    }

    @JsonProperty("contact_name")
    public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }

    @JsonProperty("latitude")
    public Double getLatitude() {
        return latitude;
    }

    @JsonProperty("latitude")
    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    @JsonProperty("longitude")
    public Double getLongitude() {
        return longitude;
    }

    @JsonProperty("longitude")
    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    @JsonProperty("sort_no")
    public Long getSortNo() {
        return sortNo;
    }

    @JsonProperty("sort_no")
    public void setSortNo(Long sortNo) {
        this.sortNo = sortNo;
    }

    @JsonProperty("address_id")
    public Long getAddressId() {
        return addressId;
    }

    @JsonProperty("address_id")
    public void setAddressId(Long addressId) {
        this.addressId = addressId;
    }

    @JsonProperty("address")
    public Address getAddress() {
        return address;
    }

    @JsonProperty("address")
    public void setAddress(Address address) {
        this.address = address;
    }

    @JsonProperty("products")
    public List<Product> getProducts() {
        return products;
    }

    @JsonProperty("products")
    public void setProducts(List<Product> products) {
        this.products = products;
    }

    @JsonProperty("phone")
    public String getPhoneNos() {
        return phoneNos;
    }

    @JsonProperty("phone")
    public void setPhoneNos(String phoneNos) {
        this.phoneNos = phoneNos;
    }

    @JsonProperty("email")
    public String getEmails() {
        return emails;
    }

    @JsonProperty("email")
    public void setEmails(String emails) {
        this.emails = emails;
    }

    @JsonProperty("links")
    public List<Link> getLinks() {
        return links;
    }

    @JsonProperty("links")
    public void setLinks(List<Link> links) {
        this.links = links;
    }

    @NotNull
    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return "Companydetail{" +
                ", name='" + name + '\'' +
                ", paymentStatus='" + paymentStatus + '\'' +
                ", phoneNos='" + phoneNos + '\'' +
                ", callnos='" + callnos + '\'' +
                ", telephonefax='" + telephonefax + '\'' +
                ", mobile='" + mobile
                ;
    }
}





