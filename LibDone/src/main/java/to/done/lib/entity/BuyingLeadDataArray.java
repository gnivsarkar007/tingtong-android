package to.done.lib.entity;

/**
 * Created by gauravnivsarkar on 11/29/14.
 */

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "buying_leads"
})
public class BuyingLeadDataArray {
    @JsonProperty("buying_leads")
    private List<BuyingLead> buyingLeads = new ArrayList<BuyingLead>();
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     *
     * @return
     * The buyingLeads
     */
    @JsonProperty("buying_leads")
    public List<BuyingLead> getBuyingLeads() {
        return buyingLeads;
    }

    /**
     *
     * @param buyingLeads
     * The buying_leads
     */
    @JsonProperty("buying_leads")
    public void setBuyingLeads(List<BuyingLead> buyingLeads) {
        this.buyingLeads = buyingLeads;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}



