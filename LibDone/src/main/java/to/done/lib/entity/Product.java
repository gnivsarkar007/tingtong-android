package to.done.lib.entity;

/**
 * Created by HP on 7/21/2014.
 */

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "name",
        "description",
        "image_url",
        "row_status",
        "date_created",
        "date_modified",
        "company_id",
        "sort_no"
})
@DatabaseTable
public class Product {
    @DatabaseField(id=true)
    @JsonProperty("id")
    private Long id;
    @DatabaseField
    @JsonProperty("name")
    private String name;
    @DatabaseField
    @JsonProperty("description")
    private String description;
    @DatabaseField
    @JsonProperty("image_url")
    private String imageUrl;
    @DatabaseField
    @JsonProperty("row_status")
    private String rowStatus;

    @DatabaseField
    @JsonProperty("company_id")
    private Long companyId;
    @DatabaseField
    @JsonProperty("sort_no")
    private Long sortNo;
    @DatabaseField(defaultValue = "0")
    @JsonIgnore
    Integer is_for_list;

    public Integer getIs_for_list() {
        return is_for_list;
    }

    public void setIs_for_list(Integer is_for_list) {
        this.is_for_list = is_for_list;
    }

    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("id")
    public Long getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Long id) {
        this.id = id;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    @JsonProperty("image_url")
    public String getImageUrl() {
        return imageUrl;
    }

    @JsonProperty("image_url")
    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @JsonProperty("row_status")
    public String getRowStatus() {
        return rowStatus;
    }

    @JsonProperty("row_status")
    public void setRowStatus(String rowStatus) {
        this.rowStatus = rowStatus;
    }

    @JsonProperty("company_id")
    public Long getCompanyId() {
        return companyId;
    }

    @JsonProperty("company_id")
    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    @JsonProperty("sort_no")
    public Long getSortNo() {
        return sortNo;
    }

    @JsonProperty("sort_no")
    public void setSortNo(Long sortNo) {
        this.sortNo = sortNo;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}