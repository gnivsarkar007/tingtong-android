package to.done.lib.entity;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "flat",
        "building",
        "street",
        "landmark",
        "pincode",
        "city_id",
        "city",
        "state_id",
        "state",
        "country_id",
        "country",
        "date_created",
        "date_modified"
})
@DatabaseTable
public class Address {
@DatabaseField(id=true)
    @JsonProperty("id")
    private Long id;
    @DatabaseField
    @JsonProperty("flat")
    private String flat;
    @DatabaseField
    @JsonProperty("building")
    private String building;
    @DatabaseField
    @JsonProperty("street")
    private String street;
    @DatabaseField
    @JsonProperty("landmark")
    private String landmark;
    @DatabaseField
    @JsonProperty("pincode")
    private String pincode;
    @DatabaseField
    @JsonProperty("city_id")
    private Long cityId;
    @DatabaseField
    @JsonProperty("city")
    private String city;
    @DatabaseField
    @JsonProperty("state_id")
    private Long stateId;
    @DatabaseField
    @JsonProperty("state")
    private String state;
    @DatabaseField
    @JsonProperty("country_id")
    private Long countryId;
    @DatabaseField
    @JsonProperty("country")
    private String country;
    @DatabaseField
    @JsonProperty("area_name")
    private String area_name;
    @DatabaseField
    @JsonIgnore
    private Long company_id;

    public Long getCompany_id() {
        return company_id;
    }

    public void setCompany_id(Long company_id) {
        this.company_id = company_id;
    }

    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("id")
    public Long getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Long id) {
        this.id = id;
    }

    @JsonProperty("flat")
    public String getFlat() {
        return flat;
    }

    @JsonProperty("flat")
    public void setFlat(String flat) {
        this.flat = flat;
    }

    @JsonProperty("building")
    public String getBuilding() {
        return building;
    }

    @JsonProperty("building")
    public void setBuilding(String building) {
        this.building = building;
    }

    @JsonProperty("street")
    public String getStreet() {
        return street;
    }

    @JsonProperty("street")
    public void setStreet(String street) {
        this.street = street;
    }

    @JsonProperty("landmark")
    public String getLandmark() {
        return landmark;
    }

    @JsonProperty("landmark")
    public void setLandmark(String landmark) {
        this.landmark = landmark;
    }

    @JsonProperty("pincode")
    public String getPincode() {
        return pincode;
    }

    @JsonProperty("pincode")
    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    @JsonProperty("city_id")
    public Long getCityId() {
        return cityId;
    }

    @JsonProperty("city_id")
    public void setCityId(Long cityId) {
        this.cityId = cityId;
    }

    @JsonProperty("city")
    public String getCity() {
        return city;
    }

    @JsonProperty("city")
    public void setCity(String city) {
        this.city = city;
    }

    @JsonProperty("state_id")
    public Long getStateId() {
        return stateId;
    }

    @JsonProperty("state_id")
    public void setStateId(Long stateId) {
        this.stateId = stateId;
    }

    @JsonProperty("state")
    public String getState() {
        return state;
    }

    @JsonProperty("state")
    public void setState(String state) {
        this.state = state;
    }

    @JsonProperty("country_id")
    public Long getCountryId() {
        return countryId;
    }

    @JsonProperty("country_id")
    public void setCountryId(Long countryId) {
        this.countryId = countryId;
    }

    @JsonProperty("country")
    public String getCountry() {
        return country;
    }

    @JsonProperty("country")
    public void setCountry(String country) {
        this.country = country;
    }
    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
    @JsonProperty("area_name")
    public String getArea_name() {
        return area_name;
    }
    @JsonProperty("area_name")
    public void setArea_name(String area_name) {
        this.area_name = area_name;
    }

    @Override
    public String toString() {
        StringBuilder build=new StringBuilder();
        if(flat!=null){
            build.append(flat+",");
        }
        if(building!=null){
            build.append(building+",");
        }
        if(street!=null){
            build.append(street+",");
        }
        if(area_name!=null){
            build.append(area_name+",");
        }
        if(landmark!=null){
            build.append(landmark+",");
        }
        if(city!=null){
            build.append(city+",");
        }
        if(pincode!=null){
            build.append(pincode+",");
        }

        if(state!=null){
            build.append(state+",");
        }
        if(country!=null){
            build.append(country+".");
        }
        return build.toString();
    }
}