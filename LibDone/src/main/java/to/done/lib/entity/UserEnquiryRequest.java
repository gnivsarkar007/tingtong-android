package to.done.lib.entity;

/**
 * Created by HP on 9/3/2014.
 */

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "user_name",
        "user_mobile",
        "user_email",
        "user_subject",
        "user_msg",
        "company_id"
})
public class UserEnquiryRequest {

    @JsonProperty("user_name")
    private String userName;
    @JsonProperty("user_mobile")
    private String userMobile;
    @JsonProperty("user_email")
    private String userEmail;
    @JsonProperty("user_subject")
    private String userSubject;
    @JsonProperty("user_msg")
    private String userMsg;
    @JsonProperty("company_id")
    private String companyId;
    @JsonProperty("prod_name")
    private String prod_name;

    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("prod_name")
    public String getProd_name() {
        return prod_name;
    }
    @JsonProperty("prod_name")
    public void setProd_name(String prod_name) {
        this.prod_name = prod_name;
    }

    @JsonProperty("user_name")
    public String getUserName() {
        return userName;
    }

    @JsonProperty("user_name")
    public void setUserName(String userName) {
        this.userName = userName;
    }

    @JsonProperty("user_mobile")
    public String getUserMobile() {
        return userMobile;
    }

    @JsonProperty("user_mobile")
    public void setUserMobile(String userMobile) {
        this.userMobile = userMobile;
    }

    @JsonProperty("user_email")
    public String getUserEmail() {
        return userEmail;
    }

    @JsonProperty("user_email")
    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    @JsonProperty("user_subject")
    public String getUserSubject() {
        return userSubject;
    }

    @JsonProperty("user_subject")
    public void setUserSubject(String userSubject) {
        this.userSubject = userSubject;
    }

    @JsonProperty("user_msg")
    public String getUserMsg() {
        return userMsg;
    }

    @JsonProperty("user_msg")
    public void setUserMsg(String userMsg) {
        this.userMsg = userMsg;
    }

    @JsonProperty("company_id")
    public String getCompanyId() {
        return companyId;
    }

    @JsonProperty("company_id")
    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
