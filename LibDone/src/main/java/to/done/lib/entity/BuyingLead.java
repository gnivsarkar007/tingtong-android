package to.done.lib.entity;

/**
 * Created by gauravnivsarkar on 11/29/14.
 */

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "product_name",
        "product_qty",
        "description",
        "payment_term",
        "port_of_delivery",
        "packing_detail",
        "phone_number",
        "email_id",
        "name",
        "company_name",
        "address",
        "date_created",
        "date_modified"
})
@DatabaseTable
public class BuyingLead {
    @DatabaseField(id = true)
    @JsonProperty("id")
    private Long id;
    @DatabaseField
    @JsonProperty("product_name")
    private String productName;
    @DatabaseField
    @JsonProperty("product_qty")
    private String productQty;
    @DatabaseField
    @JsonProperty("description")
    private String description;
    @DatabaseField
    @JsonProperty("payment_term")
    private String paymentTerm;
    @DatabaseField
    @JsonProperty("port_of_delivery")
    private String portOfDelivery;
    @DatabaseField
    @JsonProperty("packing_detail")
    private String packingDetail;
    @DatabaseField
    @JsonProperty("phone_number")
    private String phoneNumber;
    @DatabaseField
    @JsonProperty("email_id")
    private String emailId;
    @DatabaseField
    @JsonProperty("name")
    private String name;
    @DatabaseField
    @JsonProperty("company_name")
    private String companyName;
    @DatabaseField
    @JsonProperty("address")
    private String address;
    @DatabaseField
    @JsonProperty("date_created")
    private String dateCreated;
    @DatabaseField
    @JsonProperty("date_modified")
    private String dateModified;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     *
     * @return
     * The id
     */
    @JsonProperty("id")
    public Long getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    @JsonProperty("id")
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The productName
     */
    @JsonProperty("product_name")
    public String getProductName() {
        return productName;
    }

    /**
     *
     * @param productName
     * The product_name
     */
    @JsonProperty("product_name")
    public void setProductName(String productName) {
        this.productName = productName;
    }

    /**
     *
     * @return
     * The productQty
     */
    @JsonProperty("product_qty")
    public String getProductQty() {
        return productQty;
    }

    /**
     *
     * @param productQty
     * The product_qty
     */
    @JsonProperty("product_qty")
    public void setProductQty(String productQty) {
        this.productQty = productQty;
    }

    /**
     *
     * @return
     * The description
     */
    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    /**
     *
     * @param description
     * The description
     */
    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     *
     * @return
     * The paymentTerm
     */
    @JsonProperty("payment_term")
    public String getPaymentTerm() {
        return paymentTerm;
    }

    /**
     *
     * @param paymentTerm
     * The payment_term
     */
    @JsonProperty("payment_term")
    public void setPaymentTerm(String paymentTerm) {
        this.paymentTerm = paymentTerm;
    }

    /**
     *
     * @return
     * The portOfDelivery
     */
    @JsonProperty("port_of_delivery")
    public String getPortOfDelivery() {
        return portOfDelivery;
    }

    /**
     *
     * @param portOfDelivery
     * The port_of_delivery
     */
    @JsonProperty("port_of_delivery")
    public void setPortOfDelivery(String portOfDelivery) {
        this.portOfDelivery = portOfDelivery;
    }

    /**
     *
     * @return
     * The packingDetail
     */
    @JsonProperty("packing_detail")
    public String getPackingDetail() {
        return packingDetail;
    }

    /**
     *
     * @param packingDetail
     * The packing_detail
     */
    @JsonProperty("packing_detail")
    public void setPackingDetail(String packingDetail) {
        this.packingDetail = packingDetail;
    }

    /**
     *
     * @return
     * The phoneNumber
     */
    @JsonProperty("phone_number")
    public String getPhoneNumber() {
        return phoneNumber;
    }

    /**
     *
     * @param phoneNumber
     * The phone_number
     */
    @JsonProperty("phone_number")
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    /**
     *
     * @return
     * The emailId
     */
    @JsonProperty("email_id")
    public String getEmailId() {
        return emailId;
    }

    /**
     *
     * @param emailId
     * The email_id
     */
    @JsonProperty("email_id")
    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    /**
     *
     * @return
     * The name
     */
    @JsonProperty("name")
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The companyName
     */
    @JsonProperty("company_name")
    public String getCompanyName() {
        return companyName;
    }

    /**
     *
     * @param companyName
     * The company_name
     */
    @JsonProperty("company_name")
    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    /**
     *
     * @return
     * The address
     */
    @JsonProperty("address")
    public String getAddress() {
        return address;
    }

    /**
     *
     * @param address
     * The address
     */
    @JsonProperty("address")
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     *
     * @return
     * The dateCreated
     */
    @JsonProperty("date_created")
    public String getDateCreated() {
        return dateCreated;
    }

    /**
     *
     * @param dateCreated
     * The date_created
     */
    @JsonProperty("date_created")
    public void setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated;
    }

    /**
     *
     * @return
     * The dateModified
     */
    @JsonProperty("date_modified")
    public String getDateModified() {
        return dateModified;
    }

    /**
     *
     * @param dateModified
     * The date_modified
     */
    @JsonProperty("date_modified")
    public void setDateModified(String dateModified) {
        this.dateModified = dateModified;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
