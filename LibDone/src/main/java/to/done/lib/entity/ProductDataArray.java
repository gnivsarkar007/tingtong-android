package to.done.lib.entity;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by HP on 8/10/2014.
 */
@JsonPropertyOrder({
        "products",
        "companydetails",
        "moqs",
        "fobs",
        "pay_t",
        "callnos",
        "telephonefax",
        "mobile"
})
public class ProductDataArray {
    @JsonProperty("products")
    private List<Product> products = new ArrayList<Product>();
    @JsonProperty("companydetails")
    List<CompanyDetailforProductSearch>companies=new ArrayList<CompanyDetailforProductSearch>();
    @JsonProperty("links")
    private List<Link> links = new ArrayList<Link>();
    @JsonProperty("moqs")
    private List<MOQ> moqs = new ArrayList<MOQ>();
    @JsonProperty("fobs")
    private List<FOB> fobs = new ArrayList<FOB>();
    @JsonProperty("pay_t")
    private List<PayT> payT = new ArrayList<PayT>();
    @JsonProperty("callnos")
    private String callnos;
    @JsonProperty("telephonefax")
    private String telephonefax;
    @JsonProperty("mobile")
    private String mobile;
    @JsonProperty("contact")
    private List<Contact_Person> contact = new ArrayList<Contact_Person>();

    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     *
     * @return
     * The products
     */
    @JsonProperty("products")
    public List<Product> getProducts() {
        return products;
    }

    /**
     *
     * @param products
     * The products
     */
    @JsonProperty("products")
    public void setProducts(List<Product> products) {
        this.products = products;
    }

    /**
     *
     * @return
     * The companies
     */
    @JsonProperty("companydetails")
    public List<CompanyDetailforProductSearch> getCompanydetails() {
        return companies;
    }

    /**
     *
     * @param companies
     * The companies
     */
    @JsonProperty("companydetails")
    public void setCompanydetails(List<CompanyDetailforProductSearch> products) {
        this.companies = products;
    }


    /**
     *
     * @return
     * The links
     */
    @JsonProperty("links")
    public List<Link> getLinks() {
        return links;
    }

    /**
     *
     * @param links
     * The links
     */
    @JsonProperty("links")
    public void setLinks(List<Link> links) {
        this.links = links;
    }

    /**
     *
     * @return
     * The moqs
     */
    @JsonProperty("moqs")
    public List<MOQ> getMoqs() {
        return moqs;
    }

    /**
     *
     * @param moqs
     * The moqs
     */
    @JsonProperty("moqs")
    public void setMoqs(List<MOQ> moqs) {
        this.moqs = moqs;
    }

    /**
     *
     * @return
     * The fobs
     */
    @JsonProperty("fobs")
    public List<FOB> getFobs() {
        return fobs;
    }

    /**
     *
     * @param fobs
     * The fobs
     */
    @JsonProperty("fobs")
    public void setFobs(List<FOB> fobs) {
        this.fobs = fobs;
    }

    /**
     *
     * @return
     * The payT
     */
    @JsonProperty("pay_t")
    public List<PayT> getPayT() {
        return payT;
    }

    /**
     *
     * @param payT
     * The pay_t
     */
    @JsonProperty("pay_t")
    public void setPayT(List<PayT> payT) {
        this.payT = payT;
    }

    /**
     *
     * @return
     * The callnos
     */
    @JsonProperty("callnos")
    public String getCallnos() {
        return callnos;
    }

    /**
     *
     * @param callnos
     * The callnos
     */
    @JsonProperty("callnos")
    public void setCallnos(String callnos) {
        this.callnos = callnos;
    }

    /**
     *
     * @return
     * The telephonefax
     */
    @JsonProperty("telephonefax")
    public String getTelephonefax() {
        return telephonefax;
    }

    /**
     *
     * @param telephonefax
     * The telephonefax
     */
    @JsonProperty("telephonefax")
    public void setTelephonefax(String telephonefax) {
        this.telephonefax = telephonefax;
    }

    /**
     *
     * @return
     * The mobile
     */
    @JsonProperty("mobile")
    public String getMobile() {
        return mobile;
    }

    /**
     *
     * @param mobile
     * The mobile
     */
    @JsonProperty("mobile")
    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    /**
     *
     * @return
     * The contact
     */
    @JsonProperty("contact")
    public List<Contact_Person> getContact() {
        return contact;
    }

    /**
     *
     * @param contact
     * The contact
     */
    @JsonProperty("contact")
    public void setContact(List<Contact_Person> contact) {
        this.contact = contact;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
