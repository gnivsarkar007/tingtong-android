package to.done.lib.entity;

/**
 * Created by HP on 9/7/2014.
 */

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "product_name",
        "product_qty",
        "description",
        "payment_term",
        "port_of_delivery",
        "packing_detail",
        "phone_number",
        "email_id",
        "name",
        "company_name",
        "address"
})
public class UserBuyRequest {

    @JsonProperty("product_name")
    private String productName;
    @JsonProperty("product_qty")
    private String productQty;
    @JsonProperty("description")
    private String description;
    @JsonProperty("payment_term")
    private String paymentTerm;
    @JsonProperty("port_of_delivery")
    private String portOfDelivery;
    @JsonProperty("packing_detail")
    private String packingDetail;
    @JsonProperty("phone_number")
    private String phoneNumber;
    @JsonProperty("email_id")
    private String emailId;
    @JsonProperty("name")
    private String name;
    @JsonProperty("company_name")
    private String companyName;
    @JsonProperty("address")
    private String address;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("product_name")
    public String getProductName() {
        return productName;
    }

    @JsonProperty("product_name")
    public void setProductName(String productName) {
        this.productName = productName;
    }

    @JsonProperty("product_qty")
    public String getProductQty() {
        return productQty;
    }

    @JsonProperty("product_qty")
    public void setProductQty(String productQty) {
        this.productQty = productQty;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    @JsonProperty("payment_term")
    public String getPaymentTerm() {
        return paymentTerm;
    }

    @JsonProperty("payment_term")
    public void setPaymentTerm(String paymentTerm) {
        this.paymentTerm = paymentTerm;
    }

    @JsonProperty("port_of_delivery")
    public String getPortOfDelivery() {
        return portOfDelivery;
    }

    @JsonProperty("port_of_delivery")
    public void setPortOfDelivery(String portOfDelivery) {
        this.portOfDelivery = portOfDelivery;
    }

    @JsonProperty("packing_detail")
    public String getPackingDetail() {
        return packingDetail;
    }

    @JsonProperty("packing_detail")
    public void setPackingDetail(String packingDetail) {
        this.packingDetail = packingDetail;
    }

    @JsonProperty("phone_number")
    public String getPhoneNumber() {
        return phoneNumber;
    }

    @JsonProperty("phone_number")
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @JsonProperty("email_id")
    public String getEmailId() {
        return emailId;
    }

    @JsonProperty("email_id")
    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("company_name")
    public String getCompanyName() {
        return companyName;
    }

    @JsonProperty("company_name")
    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    @JsonProperty("address")
    public String getAddress() {
        return address;
    }

    @JsonProperty("address")
    public void setAddress(String address) {
        this.address = address;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
