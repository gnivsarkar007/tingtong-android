package to.done.lib.entity;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by HP on 8/24/2014.
 */

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "fob_name",
        "price",
        "price_value",
        "product_id"
})
@DatabaseTable
public class FOB {
    @DatabaseField(id = true)
    @JsonProperty("id")
    private String id;
    @DatabaseField
    @JsonProperty("fob_name")
    private String fobName;
    @DatabaseField
    @JsonProperty("price")
    private String price;
    @DatabaseField
    @JsonProperty("product_id")
    private String product_id;
    @DatabaseField
    @JsonProperty("price_value")
    Double price_value;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Double getPrice_value() {
        return price_value;
    }

    public void setPrice_value(Double price_value) {
        this.price_value = price_value;
    }

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("fob_name")
    public String getFobName() {
        return fobName;
    }

    @JsonProperty("fob_name")
    public void setFobName(String fobName) {
        this.fobName = fobName;
    }

    @JsonProperty("price")
    public String getPrice() {
        return price;
    }

    @JsonProperty("price")
    public void setPrice(String price) {
        this.price = price;
    }

    @JsonProperty("product_id")
    public String getProductId() {
        return product_id;
    }

    @JsonProperty("product_id")
    public void setProductId(String productId) {
        this.product_id = productId;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}