package to.done.lib.entity;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by HP on 9/20/2014.
 */
@JsonPropertyOrder({
        "id",
        "category_name",
        "parent_category_id",
        "image_url",
        "date_created",
        "date_modified"
})
@DatabaseTable
public class Category {
@DatabaseField(id = true)
    @JsonProperty("id")
    private Long id;
    @DatabaseField
    @JsonProperty("category_name")
    private String categoryName;
    @DatabaseField
    @JsonProperty("parent_category_id")
    private String parent_category_id;
    @DatabaseField
    @JsonProperty("image_url")
    private String image_url;
    @DatabaseField
    @JsonProperty("date_created")
    private String dateCreated;
    @DatabaseField
    @JsonProperty("date_modified")
    private String dateModified;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("id")
    public Long getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Long id) {
        this.id = id;
    }

    @JsonProperty("category_name")
    public String getCategoryName() {
        return categoryName;
    }

    @JsonProperty("category_name")
    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    @JsonProperty("parent_category_id")
    public String getParentCategoryId() {
        return parent_category_id;
    }

    @JsonProperty("parent_category_id")
    public void setParentCategoryId(String parentCategoryId) {
        this.parent_category_id = parentCategoryId;
    }

    @JsonProperty("image_url")
    public String getImageUrl() {
        return image_url;
    }

    @JsonProperty("image_url")
    public void setImageUrl(String imageUrl) {
        this.image_url = imageUrl;
    }

    @JsonProperty("date_created")
    public Object getDateCreated() {
        return dateCreated;
    }

    @JsonProperty("date_created")
    public void setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated;
    }

    @JsonProperty("date_modified")
    public String getDateModified() {
        return dateModified;
    }

    @JsonProperty("date_modified")
    public void setDateModified(String dateModified) {
        this.dateModified = dateModified;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return categoryName;
    }
}
