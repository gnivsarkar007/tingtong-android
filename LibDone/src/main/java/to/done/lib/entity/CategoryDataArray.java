package to.done.lib.entity;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by HP on 9/20/2014.
 */
@JsonPropertyOrder({
        "categories",
        "version",
        "banner"
})
public class CategoryDataArray {

    @JsonProperty("categories")
    private List<Category> categories = new ArrayList<Category>();
    @JsonProperty("version")
    private List<Version> version = new ArrayList<Version>();

    @JsonProperty("banner")
    private List<Banner> banner = new ArrayList<Banner>();
    @JsonProperty("leads")
    private List<Leads> leads = new ArrayList<Leads>();
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("categories")
    public List<Category> getCategories() {
        return categories;
    }

    @JsonProperty("categories")
    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    @JsonProperty("version")
    public List<Version> getVersion() {
        return version;
    }

    @JsonProperty("version")
    public void setVersion(List<Version> version) {
        this.version = version;
    }

    @JsonProperty("banner")
    public List<Banner> getBanner() {
        return banner;
    }

    @JsonProperty("banner")
    public void setBanner(List<Banner> banner) {
        this.banner = banner;
    }

@JsonProperty("leads")
    public List<Leads> getLeads() {
        return leads;
    }


    @JsonProperty("leads")
    public void setLeads(List<Leads> leads) {
        this.leads = leads;
    }
    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
}
