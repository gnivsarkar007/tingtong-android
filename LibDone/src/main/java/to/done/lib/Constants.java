package to.done.lib;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.text.Html;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import to.done.lib.entity.Product;
import to.done.lib.images.util.ImageFetcher;
import to.done.lib.utils.Toolbox;


/**
 * Created by dipenpradhan on 4/7/14.
 */
public final class Constants {


    private Constants() {

    }
    public static Dialog imgDialog = null;

    public static void createDialogWithImage(Context context,int dialog_layout,int img_view,int text_title,int text_descr,int send_button,
                                             String url,String title,String description,ImageFetcher fetcher,boolean showDescr,View.OnClickListener listener,Product prod) {
        try {
//

            ImageView img = null;
            TextView descr=null,titleV=null;
            Button send=null;
            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            if (imgDialog == null || !imgDialog.getContext().equals(context)) {
                imgDialog = new Dialog(context);
                imgDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

                lp.copyFrom(imgDialog.getWindow().getAttributes());
                lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                lp.height = WindowManager.LayoutParams.MATCH_PARENT;
                imgDialog.setContentView(dialog_layout);
                imgDialog.setCancelable(true);


                Log.d("New dialog", "new Dialog()");
            }
            img = (ImageView) imgDialog.findViewById(img_view);
            descr= (TextView) imgDialog.findViewById(text_descr);
            titleV= (TextView) imgDialog.findViewById(text_title);
            send= (Button) imgDialog.findViewById(send_button);
            send.setTag(prod);
            send.setOnClickListener(listener);
            fetcher.loadImage(url,img);
            if(showDescr) {
                descr.setVisibility(View.VISIBLE);
                descr.setText(Html.fromHtml(description));
            }
                else descr.setVisibility(View.GONE);
            titleV.setText(title);
            imgDialog.show();
            imgDialog.getWindow().setAttributes(lp);
        }catch (Exception e){
            Toast.makeText(context,"An Error Occured"+e.toString(), Toast.LENGTH_LONG).show();
        }

    }

    public static Dialog imgDialog2 = null;

    public static void createDialogWithImage2(Context context,int dialog_layout,int img_view,int text_title,
                                             String url,int text_descr,String title,String description,ImageFetcher fetcher,boolean showDescr) {
        try {
//
//            Intent intent = new Intent();
//            intent.setAction(Intent.ACTION_VIEW);
//            intent.setDataAndType(Uri.parse(url), "image/*");
//            context.startActivity(intent);
            ImageView img = null;
            TextView descr=null,titleV=null;
            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            if (imgDialog2 == null || !imgDialog2.getContext().equals(context)) {
                imgDialog2 = new Dialog(context);
                imgDialog2.requestWindowFeature(Window.FEATURE_NO_TITLE);

//                lp.copyFrom(imgDialog2.getWindow().getAttributes());
//                lp.width = WindowManager.LayoutParams.MATCH_PARENT;
//                lp.height = WindowManager.LayoutParams.MATCH_PARENT;
                imgDialog2.setContentView(dialog_layout);
                imgDialog2.setCancelable(true);


                Log.d("New dialog", "new Dialog()");
            }
            img = (ImageView) imgDialog2.findViewById(img_view);
            //descr= (TextView) imgDialog.findViewById(text_descr);
            titleV= (TextView) imgDialog2.findViewById(text_title);
            fetcher.loadImage(url,img);
//            if(showDescr) {
//                descr.setVisibility(View.VISIBLE);
//                descr.setText(Html.fromHtml(description));
//            }
//            else descr.setVisibility(View.GONE);
            titleV.setText(title);
            imgDialog2.show();
            //imgDialog2.getWindow().setAttributes(lp);
        }catch (Exception e){
            e.printStackTrace();
            Toast.makeText(context,"An Error Occured"+e.toString(), Toast.LENGTH_LONG).show();
        }

    }



    public static final String[] MONTHS_LONG = new String[]{"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
    public static final String[] MONTHS_SHORT = new String[]{"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};

    public static final String TAG = "TINGTONG";

    public static final String PREFS_NAME = "ting_prefs";

    public static final String PREFS_GCM_ID_KEY = "GCM_ID";
    public static final String PREFS_GCM_UUID_KEY = "GCM_UUID";

    public static final String PREFS_DBVERSION = "db_version";
    public static final String PREFS_PUSH_TOKEN = "push_token";
    public static final String PREFS_DEVICE_ID = "device_id";
    public static final String PREFS_THEME_ID = "theme_id";
    public static final String PREFS_APP_VERSION = "app_version";
    public static final String PREFS_SELECTED_AREA = "selected_area";
    public static final String PREFS_SELECTED_SUBAREA = "selected_subarea";
    public static final String PREFS_SELECTED_SUBAREA_ID = "selected_subarea_id";
    public static final String PREFS_LEADS_COUNT = "leads_count";
    public static final String PREFS_TAND_ACCEPTED="t_and_c";
    public static final String PREFS_CATEGORY_VERSION = "cats_version";
    public static final String PREFS_SELECTED_CITY_ID = "selected_city_id";
    public static final String PREFS_SELECTED_STATE_ID = "selected_state_id";
    public static final String PREFS_IS_DEVICE_REGISTERED = "is_device_registered";

    public static final String PREFS_LAST_USED_MOBILE = "last_used_mobile";
    public static final String PREFS_LAST_USED_NAME = "last_used_name";
    public static final String PREFS_LAST_USED_EMAIL = "last_used_email";
    public static final String PREFS_LAST_USED_ADDRESS = "last_used_address";

    public static String INTENT_EXTRA_COMPANY_PAGE="INTENT_EXTRA_COMPANY_PAGE";
    public static String INTENT_EXTRA_PRODUCT_PAGE="INTENT_EXTRA_PRODUCT_PAGE";
    public static String INTENT_EXTRA_ENTITY_KEY="INTENT_EXTRA_ENTITY_KEY";

    //http:///companyByName
    public static final String BASE_IP_LIVE = "tingtongindia.com";//"180.149.244.173";//"fs.done.to";//"180.149.244.173";http://111.91.42.83:8080/ting/splash_screen.png
    public static final String BASE_IP_LOCAL = "111.91.42.83";//http://192.168.56.1:8080/TingTong/rest
    public static final String BASE_IP = BASE_IP_LIVE;//BASE_IP_PRAYAG;//BASE_IP_WEB_WERKS;

public static final String SPLASH_URL="http://" +BASE_IP+"/ting/splash_screen.png";
//    public static final String BASE_URL_SOLR = "http://" + BASE_IP + ":8983/solr/";
//    public static final String URL_CLOSEST_SUBAREA = BASE_URL_SOLR + "subareas/select?wt=json&q=*:*&sfield=latlng&sort=geodist()%20asc&fl=dist:geodist(),subarea_name,id,area_id,area_name,city_id,city_name,state_id,latlng&start=0&rows=1&pt=";
//    public static final String URL_SEARCH_SUBAREA = BASE_URL_SOLR + "subareas/spell?wt=json&fl=id,subarea_name,area_id,area_name,city_id,city_name,state_id,latlng&q=subarea_name:";
//
//
    public static final String BASE_URL = "http://" + BASE_IP + "/Tingtong2/index.php/webservices";//"http://180.149.244.173:5000/";http://chekout.movivation.com:5000/";
    public static final String URL_COMPANY_BY_NAME=BASE_URL+"/companyByName";
    public static final String URL_PROD_BY_NAME=BASE_URL+"/getMatchingproducts";
    public static final String URL_SEND_ENQUIRY=BASE_URL+"/getEnquiryFromUser";
    public static final String URL_BUY_REQUEST=BASE_URL+"/getUserBuyRequest";
    public static final String URL_CATEGORY=BASE_URL+"/getCompanyCategory";
    public static final String URL_NEW_COMP_REQ=BASE_URL+"/getNewCompanyRequest";
    public static final String URL_PROD_FOR_COMPANY = BASE_URL + "/getProductsForCompany";
    public static final String URL_BUY_LEADS = BASE_URL + "/getBuyingLeads";

    public static final String URL_COMP_By_CAT = BASE_URL + "/getCompanyByCategory";
//    //  public static final String URL_OUTLET_MENU_GZIP=BASE_URL+"done-outlet-menu-gzip";
//    public static final String URL_CHECK_ORDER = BASE_URL + "done-check-order";
//    public static final String URL_SAVE_ORDER = BASE_URL + "done-save-order";
//    public static final String URL_GET_OUTLET_BY_COMPANY = BASE_URL + "done-outlets-by-company";
//    public static final String URL_GET_AREA_SUBAREA_FOR_OUTLET = BASE_URL + "done-outlet-subareas";
    public static final String URL_REGISTER_DEVICE = BASE_URL + "/registerGCM";
//    public static final String URL_TRACK_ORDER = BASE_URL + "done-track-order";
//    public static final String URL_FIND_ME = BASE_URL+"done-get-user";

    public static final String FEEDBACK_EMAIL="feedback@tingtongindia.com";
    public static final String ERROR_MESSAGE_INTERNET_UNAVAILABLE = "No internet connection";
    public static final String ERROR_MESSAGE_RECORD_NOT_FOUND = "Sorry, no results found.";
    public static final String ERROR_MESSAGE_GENERIC = "Something went wrong. Please try again.";
    public static final long ERROR_TIMEOUT = 7500l;
    public static final String FAQ_LINK="http://tingtongindia.com/faq.html";

    public static final String INTENT_SCREEN_UPDATE = "intent_screen";
    public static final String SCREEN_ID = "screen_id";
    public static final String ENTITY_LIST = "entity_list";
    public static final String ENTITY_DESCR = "entity_descr";
    public static final String COMPANY_ID = "company_id";
    public static final String OUTLET_PRODUCT_ID = "outlet_product_id";
    public static final String PRODUCT_ID = "product_id";
    public static final String PRODUCT_NAME = "product_name";
    public static final String SUBAREA_ID = "subarea_id";
    public static final String SUBAREA_NAME = "subarea_name";
    public static final String RESULT_SIZE = "result_size";
    public static final String CUST_SIZE = "cust_size";
    public static final String CONST_KEY_SEARCH="key_search";
    public static final String CONST_KEY_REG="key_register";
    public static final String CONST_KEY_POSTREQ="key_req";
    public static final String CONST_KEY_ABOUT="key_about";
    public static final String CONST_KEY_SHARE="key_share";
    public static final String CONST_KEY_RATE="key_rate";
    public static final String CONST_KEY_SEND_FEEDBACK="key_feedback";
    public static final String CONST_KEY_CONTACT_UD="key_contact";
    public static final String CONST_KEY_TANDC="key_t&c";
    public static final String CONST_KEY_BUY_LEADS="key_leads";
    public static final String CONST_KEY_FAQ="key_faqs";
    public static final String CONST_APP_URL="https://play.google.com/store/apps/details?id=com.tingtong";


    public static final String INTENT_NAV_DRAWER = "intent_navigation_drawer";
    public static final String NAV_DRAWER_ACTION = "drawer_action";
    public static final int NAV_DRAWER_ACTON_ENABLE = 1;
    public static final int NAV_DRAWER_ACTON_DISABLE = 2;
    public static final int NAV_DRAWER_ACTON_OPEN = 3;
    public static final int NAV_DRAWER_ACTON_CLOSE = 4;
    public static final int NAV_DRAWER_ACTON_SWITCH_STATUS = 5;


    public static final String INTENT_AB_TITLE = "intent_actionbar_title";
    public static final String AB_TITLE_TEXT = "title_text";
    public static final String AB_SUBTITLE_TEXT = "subtitle_text";

    public static final String INTENT_AB_BUTTON = "intent_home_button";
    public static final String INTENT_AB_LAYOUT_TYPE = "intent_layout_type";

    public static final String AB_BTN = "intent_home_button";
    public static final int AB_BTN_HOME = 1;
    public static final int AB_BTN_BACK = 2;

    public static final int AB_LAYOUT_SEARCH = 101;
    public static final int AB_LAYOUT_NORMAL = 102;
    public static final int AB_LAYOUT_BOTH_PANELS = 103;
    public static final String AB_LAYOUT_TYPE ="ab_layout_type";

    public static final String LATITUDE = "latitude";
    public static final String LONGITUDE = "longitude";

    public static final String ADD_TO_BACKSTACK = "add_to_backstack";
    public static final String BUNDLE_EXTRA = "bundle_extra";

    public static final int RESPONSE_CODE_SUCCESS=0;
    public static final int SCREEN_LOC_SELECTION = 1;
    public static final int SCREEN_COMPANY_LIST = 0;
    public static final int SCREEN_PRODUCT_LIST = 10;
    public static final int SCREEN_COMPANY_DESCR_PAID = 1;
    public static final int SCREEN_COMPANY_DESCR_UNPAID = 2;
    public static final int SCREEN_PRODUCT_DESCR_PAID = 11;
    public static final int SCREEN_PRODUCT_DESCR_UNPAID = 22;
    public static final int SCREEN_PROD_LIST = 3;
    public static final int SCREEN_PROD_DESCR = 5;
    public static final int SCREEN_MAP = 6;
    public static int CURRENT_SCREEN = SCREEN_LOC_SELECTION;

    public static final String CART_REACHED_BY_PROCEED = "reached_by_proceed";

    public static final String DELIVERY_TYPE_DELIVER = "delivery";
    public static final String DELIVERY_TYPE_TAKEAWAY = "takeaway";

    public static final String DATE_DAY = "date_day";
    public static final String DATE_MONTH = "date_month";
    public static final String DATE_YEAR = "date_year";
    public static final int COMPANY_ID_INT = 6872;//6868;//6872;

    public static final long ATTRIBUTE_ID_VEG = 115;
    public static final long ATTRIBUTE_ID_NONVEG = 1;
    public static final long ATTRIBUTE_ID_SPICY = 116;
    public static final long ATTRIBUTE_ID_FAV = 118;
    public static final long ATTRIBUTE_ID_SIGNATURE = 121;
    public static final long ATTRIBUTE_ID_SUPREME = 119;
    public static final long ATTRIBUTE_ID_CLASSIC = 117;
    public static final long ATTRIBUTE_ID_STAR = 120;
    public static final int NUMBER_OF_DAYS_FOR_PREORDER = 3;

    public static final int FILTER_TEXT=1;
    public static final int USE_IN_FILTER_AND_PRODUCT_CELL_AS_TEXT=2;
    public static final int USE_IN_FILTER_AND_PRODUCT_CELL_AS_IMAGE=3;
    public static final int DONT_USE_IN_FILTER_USE_IN_PRODUCT_AS_TEXT=4;
    public static final int DONT_USE_IN_FILTER_USE_IN_PRODUCT_AS_IMAGE=5;

    public static final String GA_SCREEN_OUTLETS = "Outlet Screen";
    public static final String GA_SCREEN_MENU = "Menu Screen";
    public static final String GA_SCREEN_CART = "Cart Screen";
    public static final String GA_SCREEN_PERSONAL_DETAILS = "Personal Details Screen";
    public static final String GA_SCREEN_CONFIRM_ORDER = "Confirm Order Screen";
    public static final String GA_SCREEN_NOTIFICATION_POPUP = "Notification Screen";



    public static String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        } else {
            return capitalize(manufacturer) + " " + model;
        }
    }


    public static String getdevicePlatform() {
        return "android";
    }

    public static String getDeviceVersion() {
        return Integer.toString(Build.VERSION.SDK_INT);
    }


    private static String capitalize( String s) {
        if (s == null || s.length() == 0) {
            return "";
        }
        char first = s.charAt(0);
        if (Character.isUpperCase(first)) {
            return s;
        } else {
            return Character.toUpperCase(first) + s.substring(1);
        }
    }

    public static final String EMAIL_PATTERN =
            "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                    + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    public static void getRepeatingBackground(Context context,  View view ){
        BitmapDrawable bd = (BitmapDrawable) context.getResources().getDrawable(R.drawable.orderlist_bottom);
        bd.setTargetDensity(DisplayMetrics.DENSITY_DEFAULT);
        int width = view.getWidth();
        int intrinsicHeight = bd.getIntrinsicHeight();
        Rect bounds = new Rect(0,0,width,intrinsicHeight);
        bd.setTileModeX(Shader.TileMode.REPEAT);
        bd.setTileModeY(Shader.TileMode.CLAMP);
        bd.setBounds(bounds);
        Bitmap bitmap = Bitmap.createBitmap(bounds.width(), bounds.height(), bd.getBitmap().getConfig());
        Canvas canvas = new Canvas(bitmap);
        bd.draw(canvas);
        BitmapDrawable bitmapDrawable = new BitmapDrawable(bitmap);
        view.setBackgroundDrawable(bitmapDrawable);

    }

    public static List<String> emailsFromDevice(Activity context){
        List<String>emails=new ArrayList<String>();
        Pattern emailPattern = Patterns.EMAIL_ADDRESS; // API level 8+
        Account[] accounts = AccountManager.get(context).getAccounts();
        for (Account account : accounts) {
            if (emailPattern.matcher(account.name).matches()) {
                String possibleEmail = account.name;
                if(!emails.contains(possibleEmail))
                emails.add(possibleEmail);
            }
        }
        Toolbox.writeToLog(emails.toString());
        return emails;
    }



}
