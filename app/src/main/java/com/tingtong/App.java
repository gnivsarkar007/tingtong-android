package com.tingtong;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Build;
import android.os.Environment;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;

import to.done.lib.database.DBManager;
import to.done.lib.images.util.ImageFetcher;

/**
 * Created by HP on 7/23/2014.
 */
public class App extends Application {
    private static DBManager dbManager;
    private static ImageFetcher fetcher;
    static String DB_PATH;
    public static DBManager getDatabaseInstance(Activity context) {

        if (dbManager == null) {
            dbManager = new DBManager(context);
        }

        return dbManager;
    }
    public static ImageFetcher getFetcherInstance(Context context) {

        if (fetcher == null) {
            fetcher = new ImageFetcher(context, 300, 300);
        }

        return fetcher;
    }

        @Override
    public void onCreate() {
        super.onCreate();

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                DB_PATH = getApplicationContext().getFilesDir().getAbsolutePath().replace("files", "databases") + File.separator;
            } else {
                DB_PATH = getApplicationContext().getFilesDir().getPath() + getApplicationContext().getPackageName() + "/databases/";
            }
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }
    public static void writeToSD() {
        try {
            File sd = Environment.getExternalStorageDirectory();

            if (sd.canWrite()) {
                String currentDBPath = "ting_tong.db";
                String backupDBPath = "backupTing.db";
                File currentDB = new File(DB_PATH, currentDBPath);
                File backupDB = new File(sd, backupDBPath);

                if (currentDB.exists()) {
                    FileChannel src = new FileInputStream(currentDB).getChannel();
                    FileChannel dst = new FileOutputStream(backupDB).getChannel();
                    dst.transferFrom(src, 0, src.size());
                    src.close();
                    dst.close();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}


