package com.tingtong.fragments;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.tingtong.App;
import com.tingtong.R;
import com.tingtong.activities.ProductListDisplayActivity;
import com.tingtong.adapters.ProductListAdapter;
import com.tingtong.interfaces.IOnRecieveListChangeListener;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import to.done.lib.Constants;
import to.done.lib.entity.Banner;
import to.done.lib.entity.Product;
import to.done.lib.ui.BannerImageView;
import to.done.lib.utils.Toolbox;

/**
 * Created by HP on 8/10/2014.
 */
public class ProductListFragment extends AbsTingTongFragment implements IOnRecieveListChangeListener {
    private static final String TAG ="ProductListFragment" ;
    @InjectView(R.id.lst_content)
    ListView entityList;
    @InjectView(R.id.txt_header)
    TextView resultsCount;
    @InjectView(R.id.banner)
    BannerImageView banner;
    private ProductListAdapter productsadapter;
    private List<Product> products =null;
    private ProductListDisplayActivity mActivity;
    Banner[] banners;
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        //.getBundle(Constants.BUNDLE_EXTRA);
        getBaseActivity().addListener(this);
 //       Toolbox.writeToLog(TAG +"onAttach");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root=inflater.inflate(R.layout.entity_list_fragment,null);
        ButterKnife.inject(this, root);
        super.onCreateView(inflater,container,savedInstanceState);
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();

 //             Toolbox.writeToLog(TAG +"onResume");
    }
    public void init(){

        Toolbox.changeActionBarLayout(getActivity(), Constants.AB_LAYOUT_SEARCH,isCompanyPage());
        try{
            products =getDbManager().getAllProducts();
            banners=getDbManager().getBanners();
        }catch (Exception e){
            e.printStackTrace();
        }

        if(products !=null && products.size()>0) {

            if (productsadapter == null) {
                productsadapter = new ProductListAdapter(getActivity(), products);

            }

            entityList.setAdapter(productsadapter);
            entityList.setVisibility(View.VISIBLE);
            resultsCount.setText(productsadapter.getCount()+" results found");
        }else{
            resultsCount.setText(R.string.no_results_in_list);
            entityList.setVisibility(View.INVISIBLE);
        }

        if(banners==null) banner.setVisibility(View.GONE);
        else {
            banner.setVisibility(View.VISIBLE);
            banner.setForever(true);
            banner.animate(banners,0);
            banner.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Banner banner= (Banner) view.getTag();
                    if(banner!=null){

                        try{
                            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(banner.getUrl()));
                            getActivity().startActivity(browserIntent);
                        }
                           catch (Exception e){
                             e.printStackTrace();
                             Toolbox.showToastShort(getActivity(),getActivity().getResources().getString(R.string.error_opening_link));
                            }
                    }else Toolbox.showToastShort(getActivity(), getActivity().getResources().getString(R.string.error_opening_link));
                }
            });
        }
        App.writeToSD();

    }

    @Override
    public void listChanged() {
        try{
            products =getDbManager().getAllProducts();//AllCompanies();
            ProductListAdapter prod= (ProductListAdapter) entityList.getAdapter();
            if(products!=null && products.size()>0) {

                prod.setProducts(products);
                prod.notifyDataSetChanged();
                resultsCount.setText(prod.getCount()+" results found");
                entityList.setVisibility(View.VISIBLE);
            }else{
                resultsCount.setText("No results found");
                entityList.setVisibility(View.INVISIBLE);
            }


        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
