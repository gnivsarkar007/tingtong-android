package com.tingtong.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.tingtong.R;
import com.tingtong.adapters.CompanyListadapter;
import com.tingtong.interfaces.IOnRecieveListChangeListener;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import to.done.lib.Constants;
import to.done.lib.entity.Companydetail;
import to.done.lib.utils.Toolbox;

/**
 * Created by HP on 7/25/2014.
 */
public class CompanyListFragment extends AbsTingTongFragment implements IOnRecieveListChangeListener{
    private static final String TAG ="CompanyListFragment" ;
    @InjectView(R.id.lst_content)
    ListView entityList;
    @InjectView(R.id.txt_header)
    TextView resultsCount;
    private CompanyListadapter companyadapter;
    private List<Companydetail> companydetailList =null;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        //.getBundle(Constants.BUNDLE_EXTRA);
        getBaseActivity().addListener(this);
//        Toolbox.writeToLog(TAG +"onAttach");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root=inflater.inflate(R.layout.entity_list_fragment,null);
        ButterKnife.inject(this,root);
        super.onCreateView(inflater,container,savedInstanceState);
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();

    }
    public void init(){

//        dbManager= App.getDatabaseInstance(getActivity());
        Toolbox.changeActionBarLayout(getActivity(),Constants.AB_LAYOUT_SEARCH,isCompanyPage());
        try{
            companydetailList =getDbManager().getAllCompanies();
        }catch (Exception e){
            e.printStackTrace();
        }

            if(companydetailList !=null && companydetailList.size()>0) {

                if (companyadapter == null) {
                    companyadapter = new CompanyListadapter(getActivity(), companydetailList);

                }
                entityList.setAdapter(companyadapter);
                entityList.setVisibility(View.VISIBLE);
                resultsCount.setText(companyadapter.getCount()+" results found");
            }else{
                entityList.setVisibility(View.INVISIBLE);
                resultsCount.setText("No results found");
            }




    }

    @Override
    public void listChanged() {
//        init();
        try{
            companydetailList =getDbManager().getAllCompanies();
            CompanyListadapter comAdappter= (CompanyListadapter) entityList.getAdapter();
            if(companydetailList!=null && companydetailList.size()>0) {
                comAdappter.setCompanies(companydetailList);
                comAdappter.notifyDataSetChanged();
                entityList.setVisibility(View.VISIBLE);
                resultsCount.setText(comAdappter.getCount()+" results found");
            }else{
                entityList.setVisibility(View.INVISIBLE);
                resultsCount.setText("No results found");
            }
            comAdappter.notifyDataSetChanged();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
