package com.tingtong.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tingtong.App;
import com.tingtong.R;

import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.ButterKnife;
import butterknife.InjectView;
import to.done.lib.Constants;
import to.done.lib.entity.Banner;
import to.done.lib.entity.Companydetail;
import to.done.lib.entity.FOB;
import to.done.lib.entity.MOQ;
import to.done.lib.entity.PayT;
import to.done.lib.entity.Product;
import to.done.lib.entity.UserEnquiryRequest;
import to.done.lib.entity.UserEnquiryResp;
import to.done.lib.images.util.ImageFetcher;
import to.done.lib.sync.SyncListener;
import to.done.lib.sync.SyncManager;
import to.done.lib.ui.BannerImageView;
import to.done.lib.ui.CustomDialog;
import to.done.lib.ui.DialogListener;
import to.done.lib.utils.Toolbox;

/**
 * Created by HP on 8/17/2014.
 */
public class ProductDescrFrag extends AbsTingTongFragment {
@InjectView(R.id.txt_entity_name)
    TextView prodName;
    @InjectView(R.id.txt_company_name)
    TextView compName;
   @InjectView(R.id.txt_entity_descr)
   TextView prodDescription;
    @InjectView(R.id.txt_comp_descr)
    TextView compDescr;
    @InjectView(R.id.txt_header_two)
    TextView txtMoq;
    @InjectView(R.id.txt_moq_val)
    TextView txtFob;
    @InjectView(R.id.txt_fob_val)
    TextView txtPayt;
    @InjectView(R.id.img_email)
    ImageView imgCompEnquiry;
    @InjectView(R.id.img_comp_logo)
    ImageView imgProdImage;
    @InjectView(R.id.img_phone)
    ImageView imgPhone;
    @InjectView(R.id.banner)
    BannerImageView banner;
    @InjectView(R.id.comp_click)
    RelativeLayout company_details_holder;
    @InjectView(R.id.txt_view_more)
    TextView view_more;
    Banner[] banners;
    Bundle bundle;
    long product_id;
    Product prod=null;
    MOQ prodMoq=null;
    FOB prodFob=null;
    List<PayT> prodPay=null;
    UserEnquiryRequest req=null;
    Companydetail companydetail=null;
    ProgressDialog pDialog;
    Pattern pattern;
    Matcher matcher;
    ImageFetcher fetcher=null;
    @InjectView(R.id.img_locate)
    ImageView locate_us;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        bundle=getArguments();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root=inflater.inflate(R.layout.product_descr_page,null);
        ButterKnife.inject(this, root);
        super.onCreateView(inflater, container, savedInstanceState);
        return root;

    }

    private View.OnClickListener companyNameClick= new View.OnClickListener() {
        @Override
        public void onClick(View view) {
        if(companydetail!=null){
            if(companydetail.getPaymentStatus().toLowerCase().equals("paid")){
                SyncManager.getProductsByCompanyId(companydetail.getId(),productsForCompany,getActivity(),App.getDatabaseInstance(getActivity()));

            }
        }
        }
    };

    public void init()
    {
        pDialog=new ProgressDialog(getActivity());
        pDialog.setMessage("Sending enquiry...");
        pDialog.setCancelable(false);
        Toolbox.changeActionBarLayout(getActivity(), Constants.AB_LAYOUT_NORMAL,isCompanyPage());
        //dbManager= App.getDatabaseInstance(getActivity());
        fetcher=getFetcherInstance(getBaseActivity());
        product_id=bundle.getLong(Constants.PRODUCT_ID,-1);
        pattern = Pattern.compile(Constants.EMAIL_PATTERN);
        if(product_id==-1) return;
        else{
            banners=getDbManager().getBanners();
            Toolbox.writeToLog("Product id is "+product_id);
            prod=getDbManager().getProductById(product_id);
            companydetail=getDbManager().getCompanyById(prod.getCompanyId());
            prodMoq=getDbManager().getMOQForProduct(product_id);
            prodFob=getDbManager().getFOBForProduct(product_id);
            prodPay=getDbManager().getPaytForProduct(product_id);
          //  App.writeToSD();
            if(prod.getImageUrl()!=null) {
                fetcher.loadImage(prod.getImageUrl(), imgProdImage);
                imgProdImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Constants.createDialogWithImage2(getActivity(), R.layout.dialog_image_only,
                                R.id.img_to_show, R.id.txt_entity_name, prod.getImageUrl(),
                                R.id.txt_entity_descr,
                                prod.getName(), prod.getDescription(), fetcher, false);
                    }
                });

            }

            imgCompEnquiry.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    createSendEnquiryDialog(companydetail.getId());
                }
            });
            if(banners==null) banner.setVisibility(View.GONE);
            else {
                banner.setVisibility(View.VISIBLE);

                banner.setForever(true);
                banner.animate(banners,0);
                banner.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Banner banner= (Banner) view.getTag();
                        if(banner!=null){

                            try{
                                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(banner.getUrl()));
                                getActivity().startActivity(browserIntent);
                            }
                            catch (Exception e){
                                e.printStackTrace();
                                Toolbox.showToastShort(getActivity(),"Error occured while opening the link.");
                            }
                        }else Toolbox.showToastShort(getActivity(),"Error occured while opening the link.");
                    }
                });
            }
            imgPhone.setOnClickListener(callPhone);
            prodName.setText(prod.getName());
            compName.setText(companydetail.getName());
            compName.setOnClickListener(companyNameClick);
            company_details_holder.setOnClickListener(companyNameClick);
            prodDescription.setText(Html.fromHtml(prod.getDescription()));
            if(companydetail.getPaymentStatus().toLowerCase().equals("paid")){
                view_more.setVisibility(View.VISIBLE);
            }else view_more.setVisibility(View.GONE);
            StringBuilder build=new StringBuilder();
            build.append("Company contact number:"+companydetail.getPhoneNos()+"<br/>");
          //  build.append("<p>Company email :"+companydetail.getEmails()+"</p><br/>");
            compDescr.setText(Html.fromHtml(build.toString()));
            if(prodFob!=null)
            txtFob.setText(prodFob.getPrice_value()+" "+prodFob.getPrice()+" "+prodFob.getFobName());
            else txtFob.setText("N/A");
            if(prodMoq!=null)
            txtMoq.setText(prodMoq.getMoqQty()+" "+prodMoq.getMoq());
            else txtMoq.setText("N/A");
            if(prodPay!=null && prodPay.size()>0) {
                StringBuilder prod_pay_terms=new StringBuilder();
                int i=0;
                for(PayT p:prodPay){
                    prod_pay_terms.append(p.getTerms());

                    if(++i==prodPay.size()) prod_pay_terms.append(".");
                    else prod_pay_terms.append(",");

                }
                txtPayt.setText(prod_pay_terms.toString());
            }
            else txtPayt.setText("N/A");

            locate_us.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
//                    int code=GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity().getApplicationContext());
//                    if(code==GooglePlayServices.)
                    Bundle b=new Bundle();
                    b.putLong(Constants.COMPANY_ID,companydetail.getId());
                   Toolbox.writeToLog("Map clicked");
                    Toolbox.changeScreen(getActivity(),Constants.SCREEN_MAP,true,b);
                }
            });
        }

    }


    private View.OnClickListener callPhone= new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            String phn=getPhoneNo(companydetail);
            if(phn!=null){
                callPhone(phn);

            }else Toolbox.showToastShort(getActivity(),"No number to call");
        }
    };

    private void callPhone(String phn){
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:" + phn));
        startActivity(callIntent);
    }
    private String getPhoneNo(Companydetail com) {
        if (com.getCallnos() != null){
            String[] nos = com.getCallnos().split(",");
            if (nos != null && nos.length > 0)
                return nos[0];
            else return null;
        }else return null;
    }
    private void createSendEnquiryDialog(final Long comp_id){
        String email=Constants.emailsFromDevice(getActivity()).get(0);
        final Dialog smDialog= new Dialog(getActivity());
        smDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        smDialog.setContentView(R.layout.send_enquiry_dialog);
        final ImageView imgClose= (ImageView) smDialog.findViewById(R.id.img_close);
        final TextView txtSendEnquiry= (TextView) smDialog.findViewById(R.id.txt_send_enq);
        final EditText edtName= (EditText) smDialog.findViewById(R.id.edt_name);
        final EditText edtEmail= (EditText) smDialog.findViewById(R.id.edt_email);
        if(email!=null)
            edtEmail.setText(email);
        final EditText edtMobile= (EditText) smDialog.findViewById(R.id.edt_mobile);
        final EditText edtSubject= (EditText) smDialog.findViewById(R.id.edt_subject);
        final EditText edtMessage= (EditText) smDialog.findViewById(R.id.edt_msg);
        txtSendEnquiry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(validateField(edtName)&&validateField(edtEmail)&&validateField(edtMobile)&&validateField(edtSubject)&&validateField(edtMessage)){
                    Toolbox.showToastShort(getActivity(),"Request sent succesfully");
                    req= new UserEnquiryRequest();
                    req.setUserSubject(edtSubject.getText().toString());
                    req.setUserEmail(edtEmail.getText().toString());
                    req.setUserMobile(edtMobile.getText().toString());
                    req.setUserMsg(edtMessage.getText().toString());
                    req.setCompanyId(String.valueOf(comp_id));
                    req.setUserName(edtName.getText().toString());
                    req.setProd_name(prod.getName());
                    SyncManager.sendUserEnquiry(req, enquiryListener, getActivity(), null);
                    smDialog.dismiss();
                }
            }
        });
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                smDialog.dismiss();
            }
        });
        smDialog.show();
    }
    private SyncListener enquiryListener= new SyncListener() {
        @Override
        public void onSyncStart() {

            pDialog.setMessage("Sending enquiry...");
            pDialog.show();
        }

        @Override
        public void onSyncProgress(float percentProgress, long requestTimestamp) {

        }

        @Override
        public void onSyncSuccess(String url, Object responseObject, long requestTimestamp) {
            if(pDialog.isShowing())
                pDialog.dismiss();
            UserEnquiryResp resp= (UserEnquiryResp) responseObject;
            CustomDialog.createCustomDialog(getActivity(), resp.getResponseMsg(), "OK", null, null, true, null);
        }

        @Override
        public void onSyncFailure(String url, String reason, long requestTimestamp) {
            if(pDialog.isShowing())
                pDialog.dismiss();

            CustomDialog.createCustomDialog(getActivity(),reason,"Retry??",null,null,true,new DialogListener() {
                @Override
                public void onPositiveBtnClick() {
                    SyncManager.sendUserEnquiry(req,enquiryListener,getActivity(),null);
                }

                @Override
                public void onNegativeBtnClick() {

                }

                @Override
                public void onMiddleBtnClick() {

                }
            });
        }
    };

    SyncListener retryListener=new SyncListener() {
        @Override
        public void onSyncStart() {
            pDialog.show();
        }

        @Override
        public void onSyncProgress(float percentProgress, long requestTimestamp) {

        }

        @Override
        public void onSyncSuccess(String url, Object responseObject, long requestTimestamp) {
            if(pDialog.isShowing())
                pDialog.dismiss();
            UserEnquiryResp resp= (UserEnquiryResp) responseObject;
            CustomDialog.createCustomDialog(getActivity(),resp.getResponseMsg(),"OK",null,null,true,null);

        }

        @Override
        public void onSyncFailure(String url, String reason, long requestTimestamp) {
            CustomDialog.createCustomDialog(getActivity(),reason,"Done",null,null,true,null);

        }
    };

    private boolean validateField(EditText edtField) {
        String text = edtField.getText().toString();
        switch (edtField.getId()) {
            case R.id.edt_email: {
                if (text == null || text.equals("") || text.length() == 0) {
                    edtField.setError("This field cannot be blank!!");
                    return false;
                } else {
                    matcher=pattern.matcher(text);
                    if (matcher.matches()) {

                        edtField.setError(null);
                        return true;
                    } else {
                        edtField.setError("Please enter valid email!!");
                        return false;
                    }

                }
            }

            case R.id.edt_mobile: {
                if (text == null || text.equals("") || text.length() == 0 || text.length() < 10) {
                    edtField.setError("This field cannot be blank!!");
                    return false;
                }
                else if(!StringUtils.isNumeric(text)){
                    edtField.setError("Please enter a valid number!!");
                    return false;
                }
                else {
                    edtField.setError(null);
                    return true;
                }
            }


            default: {
                if (text == null || text.equals("") || text.length() == 0) {
                    edtField.setError("This field cannot be blank!!");
                    return false;
                } else {
                    edtField.setError(null);
                    return true;
                }
            }

        }

    }

    SyncListener productsForCompany= new SyncListener() {
        @Override
        public void onSyncStart() {
            pDialog.setMessage("Fetching company details...");
        if(!pDialog.isShowing())
            pDialog.show();
        }

        @Override
        public void onSyncProgress(float percentProgress, long requestTimestamp) {

        }

        @Override
        public void onSyncSuccess(String url, Object responseObject, long requestTimestamp) {
            if(pDialog.isShowing())
                pDialog.dismiss();
            Bundle b= new Bundle();
            b.putLong(Constants.COMPANY_ID,companydetail.getId());
            Toolbox.changeScreen(getActivity(),Constants.SCREEN_PRODUCT_DESCR_PAID,true,b);

        }

        @Override
        public void onSyncFailure(String url, String reason, long requestTimestamp) {
            if(pDialog.isShowing())
                pDialog.dismiss();
            Toolbox.showToastShort(getActivity(),reason);
        }
    };
}

