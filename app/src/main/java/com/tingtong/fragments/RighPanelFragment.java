package com.tingtong.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.tingtong.R;
import com.tingtong.adapters.CatsExpListAdapter;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by HP on 9/21/2014.
 */
public class RighPanelFragment extends AbsTingTongFragment {
    @InjectView(R.id.exp_list_categories)
    ExpandableListView catList;
    CatsExpListAdapter catsExpListAdapter=null;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root=inflater.inflate(R.layout.categories_side_panel,null);
        ButterKnife.inject(this, root);
        root.setClickable(true);
        super.onCreateView(inflater,container,savedInstanceState);
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    public void init(){
        if(catsExpListAdapter==null || catList.getAdapter()==null){
            catsExpListAdapter=new CatsExpListAdapter(getActivity());
            catList.setAdapter(catsExpListAdapter);
        }else{
            catsExpListAdapter.notifyDataSetChanged();
        }
        catList.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView expandableListView, View view, int i, long l) {
                TextView text= (TextView) view.findViewById(R.id.txt_cat_parent);
                if(text.isSelected()) text.setSelected(false);
                else text.setSelected(true);
                return false;
            }
        });
    }
}
