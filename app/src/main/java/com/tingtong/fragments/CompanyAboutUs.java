package com.tingtong.fragments;


import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.TextView;

import com.tingtong.App;
import com.tingtong.R;

import butterknife.ButterKnife;
import butterknife.InjectView;
import to.done.lib.Constants;
import to.done.lib.database.DBManager;
import to.done.lib.entity.Companydetail;

/**
 * Created by HP on 8/16/2014.
 */
public class CompanyAboutUs extends Fragment implements View.OnClickListener,View.OnTouchListener {
    private DBManager dbManager;
    private Intent startingIntent;
    private Companydetail companydetail;
    private Bundle bundle;
    private long company_id;
    @InjectView(R.id.txt_entity_descr)
   TextView company_descr;
    @InjectView(R.id.child_scroll)
    ScrollView _child_scroll;
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        bundle=getArguments();
    }
View root;
    private void createDescrdialog(){
        final Dialog smDialog= new Dialog(getActivity());
        smDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(smDialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        smDialog.setContentView(R.layout.company_descr_dialog);
        smDialog.setCancelable(true);
        TextView web= (TextView) smDialog.findViewById(R.id.content);
        Button accept= (Button) smDialog.findViewById(R.id.accept);
        Button reject= (Button) smDialog.findViewById(R.id.dont_accept);

        web.setText(companydetail.getDescription());   // now it will not fail here

        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              //  editor.commit();
                smDialog.dismiss();
            }
        });

        reject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                editor.putBoolean(Constants.PREFS_TAND_ACCEPTED,false);
//                editor.commit();

//
            }
        });
        smDialog.show();
        smDialog.getWindow().setAttributes(lp);

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
         root=inflater.inflate(R.layout.company_about_us,null);
        ButterKnife.inject(this, root);
       root.setOnTouchListener(this);
//    root.setOnClickListener(this);
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        init();
    }

    private void init(){
        dbManager= App.getDatabaseInstance(getActivity());
        company_id=bundle.getLong(Constants.COMPANY_ID);
        companydetail=dbManager.getCompanyById(company_id);
        company_descr.setText(companydetail.getDescription());

    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        if(motionEvent.getAction()==MotionEvent.ACTION_DOWN){
            root.getParent().requestDisallowInterceptTouchEvent(true);//requestDisallowParentInterceptTouchEvent(true);
            createDescrdialog();
        }else if(motionEvent.getAction()==MotionEvent.ACTION_UP||motionEvent.getAction()==MotionEvent.ACTION_MOVE || motionEvent.getAction()==MotionEvent.ACTION_CANCEL){
            root.getParent().requestDisallowInterceptTouchEvent(false);
//            requestDisallowParentInterceptTouchEvent(view,false);
        }

        return false;
    }

    private void requestDisallowParentInterceptTouchEvent(View v,boolean disallowIntercept){
        while(v.getParent()!=null && v.getParent() instanceof View){
            if(v.getParent() instanceof ScrollView)
            v.getParent().requestDisallowInterceptTouchEvent(disallowIntercept);

            v=(View)v.getParent();
        }
    }

    @Override
    public void onClick(View view) {
        createDescrdialog();
    }


}
