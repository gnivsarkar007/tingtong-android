package com.tingtong.fragments;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tingtong.R;

import butterknife.ButterKnife;
import butterknife.InjectView;
import to.done.lib.Constants;
import to.done.lib.utils.SharedPreferencesManager;

/**
 * Created by HP on 9/21/2014.
 */
public class LeftPanelFragment extends AbsTingTongFragment {
    public interface IOnFragmentPanelItemClick{
        public void onFragmentClicked(String key);
    }
    IOnFragmentPanelItemClick eventRecieverActivity;


    @InjectView(R.id.txt_2)
    TextView search;
    @InjectView(R.id.txt_3)
    TextView register;
    @InjectView(R.id.txt_4)
    TextView buyReq;
    @InjectView(R.id.txt_5)
    TextView aboutUs;
    @InjectView(R.id.txt_7)
    TextView rateUs;
    @InjectView(R.id.txt_8)
    TextView share;
    @InjectView(R.id.txt_9)
    TextView feedback;
    @InjectView(R.id.txt_10)
    TextView tandc;
    @InjectView(R.id.txt_11)
    TextView leads_count;
    @InjectView(R.id.txt_12)
    TextView faqs;
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try{
            eventRecieverActivity=(IOnFragmentPanelItemClick)activity;
        }catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement IOnFragmentPanelItemClick");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root=inflater.inflate(R.layout.left_panel_layout,null);
        ButterKnife.inject(this, root);
        root.setClickable(true);
        super.onCreateView(inflater,container,savedInstanceState);
        return root;
    }

    @Override
    public void init() {
        final SharedPreferences sharedPreferences = SharedPreferencesManager.getSharedPreferences(getActivity());
        String leads=sharedPreferences.getString(Constants.PREFS_LEADS_COUNT,"");
        leads_count.setText("Buying leads:"+leads);
        leads_count.setOnClickListener(clickListener);
        search.setOnClickListener(clickListener);
        register.setOnClickListener(clickListener);
        buyReq.setOnClickListener(clickListener);
        aboutUs.setOnClickListener(clickListener);
        rateUs.setOnClickListener(clickListener);
        share.setOnClickListener(clickListener);
        feedback.setOnClickListener(clickListener);
        tandc.setOnClickListener(clickListener);
        faqs.setOnClickListener(clickListener);
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    View.OnClickListener clickListener=new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.txt_2:  eventRecieverActivity.onFragmentClicked(Constants.CONST_KEY_SEARCH);
                    break;
                case R.id.txt_3:  eventRecieverActivity.onFragmentClicked(Constants.CONST_KEY_REG);
                    break;
                case R.id.txt_4:  eventRecieverActivity.onFragmentClicked(Constants.CONST_KEY_POSTREQ);
                    break;
                case R.id.txt_5:  eventRecieverActivity.onFragmentClicked(Constants.CONST_KEY_ABOUT);
                    break;
                case R.id.txt_7:  eventRecieverActivity.onFragmentClicked(Constants.CONST_KEY_RATE);
                    break;
                case R.id.txt_8:  eventRecieverActivity.onFragmentClicked(Constants.CONST_KEY_SHARE);
                    break;
                case R.id.txt_9:  eventRecieverActivity.onFragmentClicked(Constants.CONST_KEY_SEND_FEEDBACK);
                    break;
                case R.id.txt_10:  eventRecieverActivity.onFragmentClicked(Constants.CONST_KEY_TANDC);
                    break;

                case R.id.txt_11:  eventRecieverActivity.onFragmentClicked(Constants.CONST_KEY_BUY_LEADS);
                    break;
                case R.id.txt_12:  eventRecieverActivity.onFragmentClicked(Constants.CONST_KEY_FAQ);
                    break;
            }
        }
    };
}
