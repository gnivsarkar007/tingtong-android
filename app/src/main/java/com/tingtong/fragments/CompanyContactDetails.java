package com.tingtong.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;
import android.widget.TextView;

import com.tingtong.App;
import com.tingtong.R;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import to.done.lib.Constants;
import to.done.lib.database.DBManager;
import to.done.lib.entity.Companydetail;
import to.done.lib.entity.Contact_Person;
import to.done.lib.utils.Toolbox;

/**
 * Created by HP on 8/16/2014.
 */
public class CompanyContactDetails extends Fragment implements View.OnTouchListener {
    private DBManager dbManager;
    private Intent startingIntent;
    private Companydetail companydetail;
    private Bundle bundle;
    private long company_id;
    private List<Contact_Person> contacts;
    @InjectView(R.id.txt_ctct_person)
    TextView contact_person_name;
    @InjectView(R.id.txt_contact_phn)
    TextView contact_person_phn;
    @InjectView(R.id.txt_contact_other)
    TextView contact_person_other;
    @InjectView(R.id.txt_contact_address)
    TextView contact_person_address;

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        bundle=getArguments();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root=inflater.inflate(R.layout.company_contact_details,null);
        ButterKnife.inject(this, root);
        root.setOnTouchListener(this);
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        init();
    }

    private void init(){
        dbManager= App.getDatabaseInstance(getActivity());
        company_id=bundle.getLong(Constants.COMPANY_ID);
        App.writeToSD();

        companydetail=dbManager.getCompanyById(company_id);
        contacts=dbManager.getContactsForCompany(company_id);
        StringBuilder contact_names=new StringBuilder();
        if(contacts!=null && contacts.size()>0){
            Toolbox.writeToLog("Contacts for id "+company_id+" list.."+contacts);
            for(int i=0;i<contacts.size();i++){
                contact_names.append(contacts.get(i).getName());
                if(i==contacts.size()-1) contact_names.append(".");
                else contact_names.append(",");
            }
        }

        contact_person_name.setText(contact_names.toString());
        if(companydetail.getMobile()!=null)
        contact_person_phn.setText(companydetail.getMobile());
        else contact_person_phn.setText("");

        if(companydetail.getTelephonefax()!=null)
        contact_person_other.setText(companydetail.getTelephonefax());
        else  contact_person_other.setText("");

    contact_person_address.setText(companydetail.getAddress().toString());

    }


    @Override
    public boolean onTouch(View __v, MotionEvent __event) {
        //Toolbox.showToastShort(getActivity(),"Touched ...1");
        if (__event.getAction() == MotionEvent.ACTION_DOWN) {
            //  Disallow the touch request for parent scroll on touch of child view
            requestDisallowParentInterceptTouchEvent(__v, true);
          //  Toolbox.showToastShort(getActivity(),"Touched...2");
        } else if (__event.getAction() == MotionEvent.ACTION_UP || __event.getAction() == MotionEvent.ACTION_CANCEL) {
            // Re-allows parent events
            requestDisallowParentInterceptTouchEvent(__v, false);
        }
        return false;
//        return false;
    }

    private void requestDisallowParentInterceptTouchEvent(View v,boolean disallowIntercept){
        while(v.getParent()!=null && v.getParent() instanceof View){
            if(v.getParent() instanceof ScrollView)
                v.getParent().requestDisallowInterceptTouchEvent(disallowIntercept);

            v=(View)v.getParent();
        }
    }
}
