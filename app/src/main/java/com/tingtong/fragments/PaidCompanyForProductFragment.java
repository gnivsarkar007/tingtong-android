 package com.tingtong.fragments;

 import android.app.Activity;
 import android.app.Dialog;
 import android.app.ProgressDialog;
 import android.content.Intent;
 import android.net.Uri;
 import android.os.Bundle;
 import android.support.v4.view.ViewPager;
 import android.view.LayoutInflater;
 import android.view.View;
 import android.view.ViewGroup;
 import android.view.Window;
 import android.widget.EditText;
 import android.widget.ImageView;
 import android.widget.RatingBar;
 import android.widget.TextView;

 import com.tingtong.App;
 import com.tingtong.R;
 import com.tingtong.activities.ProductListDisplayActivity;
 import com.tingtong.adapters.DescriptionPagerAdapter;
 import com.tingtong.adapters.ImageGridAdapter;

 import org.apache.commons.lang3.StringUtils;

 import java.util.regex.Matcher;
 import java.util.regex.Pattern;

 import butterknife.ButterKnife;
 import butterknife.InjectView;
 import to.done.lib.Constants;
 import to.done.lib.entity.Companydetail;
 import to.done.lib.entity.Link;
 import to.done.lib.entity.UserEnquiryRequest;
 import to.done.lib.entity.UserEnquiryResp;
 import to.done.lib.sync.SyncListener;
 import to.done.lib.sync.SyncManager;
 import to.done.lib.twowaygrid.com.jess.ui.TwoWayGridView;
 import to.done.lib.ui.CustomDialog;
 import to.done.lib.ui.DialogListener;
 import to.done.lib.ui.PagerSlidingTabStrip;
 import to.done.lib.utils.Toolbox;

 /**
  * Created by HP on 7/25/2014.
  */
 public class PaidCompanyForProductFragment extends AbsTingTongFragment {


     @InjectView(R.id.txt_entity_name)
     TextView compName;
     @InjectView(R.id.txt_entity_city)
     TextView compCity;
     @InjectView(R.id.txt_entity_address)
     TextView compAddr;
     @InjectView(R.id.txt_entity_rating)
     TextView companyRating;
     @InjectView(R.id.descr_pager)
     ViewPager descriptionPager;
     @InjectView(R.id.pager_sliding_tab)
     PagerSlidingTabStrip pagerHeaderStrip;
     @InjectView(R.id.img_email)
     ImageView imgCompEnquiry;
     @InjectView(R.id.gridview)
     TwoWayGridView horizontalGrid;
     @InjectView(R.id.img_phone)
     ImageView imgPhone;
     @InjectView(R.id.img_share_company)
     ImageView shareCompnyDetail;
     @InjectView(R.id.serviceBar)
     RatingBar ratingBar;
     @InjectView(R.id.img_app_link)
     ImageView appLink;
     @InjectView(R.id.img_locate)
     ImageView locate_us;

 //    @InjectView(R.id.)
     private Companydetail companydetail;
     private Bundle bundle;
     private long company_id;

     Pattern pattern;
     Matcher matcher;
     @InjectView(R.id.img_share)
     ImageView img_share;
     @InjectView(R.id.img_other)
     ImageView img_other;

     ImageGridAdapter gridAdapter;
     DescriptionPagerAdapter pagerAdapter=null;
     UserEnquiryRequest req=null;
     ProgressDialog pDialog;


     View.OnClickListener share=new View.OnClickListener() {
         @Override
         public void onClick(View view) {
             StringBuilder builder=new StringBuilder();
             builder.append("Have you checked our products yet? \n"+
                     "Company name- "+companydetail.getName()+"\n");
             if(companydetail.getLinks()!=null && companydetail.getLinks().size()>0){
                 builder.append("Know more- ");
                 int i=0;
                 for(Link link:companydetail.getLinks()){
                     i++;
                     builder.append(link.getLinkUrl());
                     if(i==companydetail.getLinks().size()) builder.append("\n");
                     else{
                         builder.append(" , ");
                     }
                 }
             }
             builder.append("Download ting tong mobile b2b marketplace- http://goo.gl/uY25cZ");
             Intent sendIntent = new Intent();
             sendIntent.setAction(Intent.ACTION_SEND);
             sendIntent.putExtra(Intent.EXTRA_TEXT, builder.toString());
             sendIntent.setType("text/plain");
             getActivity().startActivity(Intent.createChooser(sendIntent, getResources().getText(R.string.share_with)));
         }
     };

     @Override
     public void onAttach(Activity activity) {
         super.onAttach(activity);
         bundle=getArguments();
     }

     @Override
     public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
         View root=inflater.inflate(R.layout.entity_description_fragment_paid,null);
         ButterKnife.inject(this,root);
         super.onCreateView(inflater,container,savedInstanceState);
         return root;
     }

     @Override
     public void onResume() {
         super.onResume();

     }
     public void init(){
         pDialog=new ProgressDialog(getActivity());
         pDialog.setMessage("Sending enquiry...");
         pDialog.setCancelable(false);
//         dbManager= App.getDatabaseInstance(getActivity());

         company_id=bundle.getLong(Constants.COMPANY_ID);
         Toolbox.changeActionBarLayout(getActivity(),Constants.AB_LAYOUT_NORMAL,isCompanyPage());
         companydetail =getDbManager().getCompanyById(company_id);
         pattern = Pattern.compile(Constants.EMAIL_PATTERN);
         ratingBar.setStepSize(0.1f);
         if(companydetail !=null){
             compName.setText(companydetail.getName());
             ratingBar.setRating(companydetail.getRating());
             companyRating.setText(""+companydetail.getRating());
             shareCompnyDetail.setOnClickListener(share);
             compAddr.setText(companydetail.getAddress().toString());

             imgPhone.setOnClickListener(callPhone);
             if(companydetail.getSupplier().toLowerCase().equals("premium")){
                 img_other.setVisibility(View.VISIBLE);
             }else img_other.setVisibility(View.GONE);
             if(companydetail.getVerification().toLowerCase().equals("verified")){
                 img_share.setVisibility(View.VISIBLE);
             }else img_share.setVisibility(View.INVISIBLE);

             if(companydetail.getProducts()!=null){
                 App.writeToSD();
                 if(gridAdapter==null || horizontalGrid.getAdapter()==null){
                     gridAdapter= new ImageGridAdapter(getDbManager().getProductsForCompany(company_id),(ProductListDisplayActivity)getActivity());
                     horizontalGrid.setAdapter(gridAdapter);
                 }
             }
         }
         locate_us.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {
                 Bundle b=new Bundle();
                 b.putLong(Constants.COMPANY_ID,company_id);
                 Toolbox.changeScreen(getActivity(),Constants.SCREEN_MAP,true,b);
             }
         });
         imgCompEnquiry.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {

                 createSendEnquiryDialog(companydetail.getId());
             }
         });
             pagerAdapter=new DescriptionPagerAdapter(getChildFragmentManager(),getActivity(),company_id);
             descriptionPager.setAdapter(pagerAdapter);
             pagerHeaderStrip.setViewPager(descriptionPager);
             pagerHeaderStrip.setTextColor(getActivity().getResources().getColor(R.color.theme_blue));
         if(companydetail.getLinks()!=null && companydetail.getLinks().size()>0) {
             appLink.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View view) {
                     if(companydetail.getLinks()!=null && companydetail.getLinks().size()>0) {
                         appLink.setOnClickListener(new View.OnClickListener() {
                             @Override
                             public void onClick(View view) {
                                 try {
                                     if (companydetail.getLinks() != null && companydetail.getLinks().size() > 0) {
                                         Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(companydetail.getLinks().get(0).getLinkUrl()));
                                         getActivity().startActivity(browserIntent);
                                     }
                                 }catch (Exception e){
                                     e.printStackTrace();
                                     Toolbox.showToastShort(getActivity(),"Error occured while opening the link.");
                                 }
                             }
                         });
                     }else{
                         appLink.setImageResource(R.drawable.img_download_unavailable);
                     }
                 }
             });
         }else{
             appLink.setImageResource(R.drawable.img_download_unavailable);
         }
     }
     private String getPhoneNo(Companydetail com) {
         if (com.getCallnos() != null){
             String[] nos = com.getCallnos().split(",");
             if (nos != null && nos.length > 0)
                 return nos[0];
             else return null;
         }else return null;
     }
     private void callPhone(String phn){
         Intent callIntent = new Intent(Intent.ACTION_CALL);
         callIntent.setData(Uri.parse("tel:" + phn));
         startActivity(callIntent);
     }

     private View.OnClickListener callPhone= new View.OnClickListener() {
         @Override
         public void onClick(View view) {

             String phn=getPhoneNo(companydetail);
             if(phn!=null){
                 callPhone(phn);

             }else Toolbox.showToastShort(getActivity(),"No number to call");
         }
     };

     private void createSendEnquiryDialog(final Long comp_id){
         String email=Constants.emailsFromDevice(getActivity()).get(0);
         final Dialog smDialog= new Dialog(getActivity());
         smDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
         smDialog.setContentView(R.layout.send_enquiry_dialog);
         final ImageView imgClose= (ImageView) smDialog.findViewById(R.id.img_close);
         final TextView txtSendEnquiry= (TextView) smDialog.findViewById(R.id.txt_send_enq);
         final EditText edtName= (EditText) smDialog.findViewById(R.id.edt_name);
         final EditText edtEmail= (EditText) smDialog.findViewById(R.id.edt_email);
         if(email!=null)
             edtEmail.setText(email);
         final EditText edtMobile= (EditText) smDialog.findViewById(R.id.edt_mobile);
         final EditText edtSubject= (EditText) smDialog.findViewById(R.id.edt_subject);
         final EditText edtMessage= (EditText) smDialog.findViewById(R.id.edt_msg);
         txtSendEnquiry.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {
                 if(validateField(edtName)&&validateField(edtEmail)&&validateField(edtMobile)&&validateField(edtSubject)&&validateField(edtMessage)){
                     Toolbox.showToastShort(getActivity(),"Request sent succesfully");
                     req= new UserEnquiryRequest();
                     req.setUserSubject(edtSubject.getText().toString());
                     req.setUserEmail(edtEmail.getText().toString());
                     req.setUserMobile(edtMobile.getText().toString());
                     req.setUserMsg(edtMessage.getText().toString());
                     req.setCompanyId(String.valueOf(comp_id));
                     req.setUserName(edtName.getText().toString());
                     req.setProd_name("");
                     SyncManager.sendUserEnquiry(req, enquiryListener, getActivity(), null);
                     smDialog.dismiss();
                 }
             }
         });
         imgClose.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {
                 smDialog.dismiss();
             }
         });
         smDialog.show();
     }
     private SyncListener enquiryListener= new SyncListener() {
         @Override
         public void onSyncStart() {
             pDialog.show();
         }

         @Override
         public void onSyncProgress(float percentProgress, long requestTimestamp) {

         }

         @Override
         public void onSyncSuccess(String url, Object responseObject, long requestTimestamp) {
             if(pDialog.isShowing())
                 pDialog.dismiss();
             UserEnquiryResp resp= (UserEnquiryResp) responseObject;
             CustomDialog.createCustomDialog(getActivity(), resp.getResponseMsg(), "OK", null, null, true, null);
         }

         @Override
         public void onSyncFailure(String url, String reason, long requestTimestamp) {
             if(pDialog.isShowing())
                 pDialog.dismiss();

             CustomDialog.createCustomDialog(getActivity(),reason,"Retry??",null,null,true,new DialogListener() {
                 @Override
                 public void onPositiveBtnClick() {
                     SyncManager.sendUserEnquiry(req,enquiryListener,getActivity(),null);
                 }

                 @Override
                 public void onNegativeBtnClick() {

                 }

                 @Override
                 public void onMiddleBtnClick() {

                 }
             });
         }
     };

     SyncListener retryListener=new SyncListener() {
         @Override
         public void onSyncStart() {
             pDialog.show();
         }

         @Override
         public void onSyncProgress(float percentProgress, long requestTimestamp) {

         }

         @Override
         public void onSyncSuccess(String url, Object responseObject, long requestTimestamp) {
             if(pDialog.isShowing())
                 pDialog.dismiss();
             UserEnquiryResp resp= (UserEnquiryResp) responseObject;
             CustomDialog.createCustomDialog(getActivity(),resp.getResponseMsg(),"OK",null,null,true,null);

         }

         @Override
         public void onSyncFailure(String url, String reason, long requestTimestamp) {
             CustomDialog.createCustomDialog(getActivity(),reason,"Done",null,null,true,null);

         }
     };

     private boolean validateField(EditText edtField) {
         String text = edtField.getText().toString();
         switch (edtField.getId()) {
             case R.id.edt_email: {
                 if (text == null || text.equals("") || text.length() == 0) {
                     edtField.setError("This field cannot be blank!!");
                     return false;
                 } else {
                     matcher=pattern.matcher(text);
                     if (matcher.matches()) {

                         edtField.setError(null);
                         return true;
                     } else {
                         edtField.setError("Please enter valid email!!");
                         return false;
                     }

                 }
             }

             case R.id.edt_mobile: {
                 if (text == null || text.equals("") || text.length() == 0 || text.length() < 10) {
                     edtField.setError("This field cannot be blank!!");
                     return false;
                 }
                 else if(!StringUtils.isNumeric(text)){
                     edtField.setError("Please enter a valid number!!");
                     return false;
                 }
                 else {
                     edtField.setError(null);
                     return true;
                 }
             }


             default: {
                 if (text == null || text.equals("") || text.length() == 0) {
                     edtField.setError("This field cannot be blank!!");
                     return false;
                 } else {
                     edtField.setError(null);
                     return true;
                 }
             }

         }

     }
 }
