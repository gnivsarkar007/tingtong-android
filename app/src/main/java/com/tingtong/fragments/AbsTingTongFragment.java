package com.tingtong.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tingtong.App;
import com.tingtong.activities.BaseActivity;
import com.tingtong.activities.CompanyListDisplayActivity;

import to.done.lib.database.DBManager;
import to.done.lib.images.util.ImageFetcher;

/**
 * Created by gauravnivsarkar on 12/4/14.
 */
public abstract class AbsTingTongFragment extends Fragment{
    private DBManager dbManager;
    public boolean isCompanyPage() {
        if(getActivity() instanceof CompanyListDisplayActivity)
        return true;
        else return false;
    }

public static ImageFetcher getFetcherInstance(BaseActivity context){
    return BaseActivity.getFetcherInstance(context);
}

    public BaseActivity getBaseActivity(){
        return (BaseActivity)getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        this.setRetainInstance(true);
        dbManager= App.getDatabaseInstance(getActivity());
        init();
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    public DBManager getDbManager() {
        return dbManager;
    }

    public void setDbManager(DBManager dbManager) {
        this.dbManager = dbManager;
    }

    public abstract void init();
}
