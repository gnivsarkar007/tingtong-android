package com.tingtong.fragments;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.View;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.tingtong.App;
import com.tingtong.R;

import to.done.lib.Constants;
import to.done.lib.database.DBManager;
import to.done.lib.entity.Address;
import to.done.lib.entity.Companydetail;

/**
 * Created by HP on 7/27/2014.
 */
public class MapFrag extends SupportMapFragment {
    Companydetail companydetail;
    static String compName;
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        App app=(App)getActivity().getApplication();
        DBManager dbManager=App.getDatabaseInstance(getActivity());

        Long companyid=getArguments().getLong(Constants.COMPANY_ID);
         companydetail =dbManager.getCompanyById(companyid);

if(companydetail !=null && companydetail.getLatitude()!=null && companydetail.getLongitude()!=null){
        GoogleMap map =getMap();
        map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        map.setMyLocationEnabled(true);
    compName=companydetail.getName();

        LocationManager locMan=(LocationManager)getActivity().getSystemService(Context.LOCATION_SERVICE);

        Location lastKnownLoc=null;
        if(locMan!=null)
        {
            lastKnownLoc=locMan.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        }


        LatLng camLatLng=null;

        if(lastKnownLoc!=null)
        {
            camLatLng=new LatLng(lastKnownLoc.getLatitude(),
                    lastKnownLoc.getLongitude());
        }
        if(camLatLng!=null)
        {
            CameraUpdate cam= CameraUpdateFactory.newLatLng(camLatLng);
            map.moveCamera(cam);
        }
//        LatLng latLngPoint = new LatLng(19.141,73.1);
        LatLngBounds.Builder boundsBuilder=new LatLngBounds.Builder();

            if(companydetail.getLatitude()!=null&& companydetail.getLongitude()!=null)
            {
                LatLng latLngPoint = new LatLng(companydetail.getLatitude(), companydetail.getLongitude());
                Address address = companydetail.getAddress();
                String strAddress = address.toString();




                map.addMarker(
                        new MarkerOptions()
                                //.icon(BitmapDescriptorFactory.fromBitmap(bmp))
                                .position(latLngPoint)
                                .title(companydetail.getName())
                                .snippet(strAddress)
//                                .icon(BitmapDescriptorFactory.fromBitmap(drawTextToBitmap(getActivity(), R.drawable.tab_nrm, outlet.getName())))

                );

                boundsBuilder.include(latLngPoint);
//                if(camLatLng==null)
//                    {
//                        camLatLng=latLngPoint;
//                    }
            }



        CameraUpdate camBounds= CameraUpdateFactory.newLatLngBounds(boundsBuilder.build(), 500, 500, 0);
        map.moveCamera(camBounds);



//        CameraUpdate camZoom=CameraUpdateFactory.zoomTo(12);
//        map.moveCamera(camZoom);
    }
    }

    public static Bitmap drawTextToBitmap(Context gContext,int gResId,String gText) {
        Resources resources = gContext.getResources();
        float scale = resources.getDisplayMetrics().density;
        Bitmap bitmap =
                BitmapFactory.decodeResource(resources, R.drawable.img_placeholder);

        android.graphics.Bitmap.Config bitmapConfig =
                bitmap.getConfig();
        if(bitmapConfig == null) {
            bitmapConfig = android.graphics.Bitmap.Config.ARGB_8888;
        }
        bitmap = bitmap.copy(bitmapConfig, true);

        Canvas canvas = new Canvas(bitmap);

        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);

        paint.setColor(Color.BLACK);
        paint.setTextSize((int) (15 * scale));
        paint.setShadowLayer(1f, 0f, 1f, Color.WHITE);

        Rect bounds = new Rect();
        paint.getTextBounds(gText, 0, gText.length(), bounds);
        int x = (bitmap.getWidth() - bounds.width())/2;
        int y = (bitmap.getHeight() + bounds.height())/2;

        canvas.drawText(compName, x * scale, y * scale, paint);

        return bitmap;
    }

}
