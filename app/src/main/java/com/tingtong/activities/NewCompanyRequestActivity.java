package com.tingtong.activities;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.tingtong.App;
import com.tingtong.R;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.ButterKnife;
import butterknife.InjectView;
import to.done.lib.Constants;
import to.done.lib.database.DBManager;
import to.done.lib.entity.Category;
import to.done.lib.entity.NewCompanyRegReq;
import to.done.lib.entity.UserEnquiryResp;
import to.done.lib.sync.SyncListener;
import to.done.lib.sync.SyncManager;
import to.done.lib.ui.CustomDialog;
import to.done.lib.ui.DialogListener;
import to.done.lib.ui.FontEditText;
import to.done.lib.utils.Toolbox;

/**
 * Created by HP on 9/7/2014.
 */
public class NewCompanyRequestActivity extends ActionBarActivity implements SyncListener{
ProgressDialog pDialog;
    Pattern pattern;
    Matcher matcher;
    @InjectView(R.id.txt_prod_details_header)
    TextView txt_prod_details_header;
    @InjectView(R.id.txt_ship_details_header)
    TextView txt_ship_details_header;
    @InjectView(R.id.txt_contact_details_header)
    TextView txt_contact_details_header;
    @InjectView(R.id.lin_details_holder)
    LinearLayout product_details;
    @InjectView(R.id.lin_holder_2)
    LinearLayout shipping_details;
    @InjectView(R.id.lin_contact_details_holder)
    LinearLayout contact_details;
    @InjectView(R.id.img_close)
    ImageView close;
    @InjectView(R.id.txt_buyer)
    TextView txt_buyer;
    @InjectView(R.id.txt_seller)
    TextView txt_seller;
    @InjectView(R.id.txt_both)
    TextView txt_both;
    @InjectView(R.id.spnr_cats)
    Spinner categoriesSpinner;
    @InjectView(R.id.edt_comp_name)
    FontEditText edt_comp_name;
    @InjectView(R.id.edt_comp_ctctperson)
    FontEditText edt_comp_ctctperson;
    @InjectView(R.id.edt_desig)
    FontEditText edt_desig;
    @InjectView(R.id.edt_comp_number)
    FontEditText edt_comp_number;
    @InjectView(R.id.edt_email)
    FontEditText edt_email;
    @InjectView(R.id.edt_address)
    FontEditText edt_address;
    @InjectView(R.id.edt_country)
    FontEditText edt_country;
    @InjectView(R.id.edt_keywords)
    FontEditText keywords;
    @InjectView(R.id.proceed)
     TextView proceed;
    NewCompanyRegReq newCompRequest=null;
    ArrayAdapter<Category>catsSpinnerAdapter=null;
    ArrayAdapter<String>countriesAdapter=null;
    DBManager dbManager;
    View selectedView;
    List<Category> childCategories=null;
    List<String>countries;
    @InjectView(R.id.countries_spinner)
    Spinner countriesSpinner;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_company_req);
        ButterKnife.inject(this);
        getSupportActionBar().hide();
        SyncManager.readFileFromAssets(this,"countries.txt",this);
    }



    FontEditText.EditCancelListener validateOnEdit=new FontEditText.EditCancelListener() {
        @Override
        public void onCancelEvent(FontEditText editText, int cancelCode) {

        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        dbManager= App.getDatabaseInstance(this);
        pDialog=new ProgressDialog(this);
        pDialog.setCancelable(false);
        pDialog.setMessage("Sending request...");
        pattern=Pattern.compile(Constants.EMAIL_PATTERN);
        txt_contact_details_header.setOnClickListener(headerViewClick);
        txt_prod_details_header.setOnClickListener(headerViewClick);
        txt_ship_details_header.setOnClickListener(headerViewClick);
        close.setOnClickListener(headerViewClick);
        txt_buyer.setOnClickListener(viewToggle);
        txt_seller.setOnClickListener(viewToggle);
        txt_both.setOnClickListener(viewToggle);
        proceed.setOnClickListener(proceedListener);

        selectedView=txt_buyer;

        edt_email.addEditCancelListener(validateOnEdit);
        edt_comp_number.addEditCancelListener(validateOnEdit);
        edt_country.addEditCancelListener(validateOnEdit);
        edt_comp_name.addEditCancelListener(validateOnEdit);
        edt_address.addEditCancelListener(validateOnEdit);
        edt_comp_ctctperson.addEditCancelListener(validateOnEdit);
        edt_desig.addEditCancelListener(validateOnEdit);

        childCategories=new ArrayList<Category>(dbManager.getParentCategories());
        if(childCategories!=null) {
            Category c=new Category();
            c.setCategoryName("Select a category");
            childCategories.add(0,c);
            if (categoriesSpinner.getAdapter() == null) {
                catsSpinnerAdapter = new ArrayAdapter<Category>(this, R.layout.cats_spinner_dropdown, childCategories);
                catsSpinnerAdapter.setDropDownViewResource(R.layout.cats_spinner_dropdown);
                categoriesSpinner.setAdapter(catsSpinnerAdapter);
            } else {
                catsSpinnerAdapter.clear();
                for(Category ch: childCategories){
                    catsSpinnerAdapter.add(ch);
                }
                catsSpinnerAdapter.notifyDataSetChanged();
            }
            selectedView.performClick();
        }

//        categoriesSpinner.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//                if(i>0){
//
//                }
//            }
//        });
    }

    private View.OnClickListener viewToggle=new View.OnClickListener() {
        @Override
        public void onClick(View view) {
      selectedView.setSelected(false);
            selectedView=view;
            selectedView.setSelected(true);
        }
    };

    public void toggleVisibility(View view){
        if(view.getVisibility()==View.VISIBLE){
            view.setVisibility(View.GONE);
        }else{
            view.setVisibility(View.VISIBLE);
        }
    }



    private boolean validateField(EditText edtField) {
        String text = edtField.getText().toString();
        switch (edtField.getId()) {
            case R.id.edt_email: {
                if (text == null || text.equals("") || text.length() == 0) {
                    edtField.setError("This field cannot be blank!!");
                    return false;
                } else {
                    matcher=pattern.matcher(text);
                    if (matcher.matches()) {

                        edtField.setError(null);
                        return true;
                    } else {
                        edtField.setError("Please enter valid email!!");
                        return false;
                    }

                }
            }

            case R.id.edt_mobile: {
                if (text == null || text.equals("") || text.length() == 0 || text.length() < 10) {
                    edtField.setError("This field cannot be blank!!");
                    return false;
                }else if(!StringUtils.isNumeric(text)){
                    edtField.setError("Please enter valid number!!");
                    return false;
                }
                else {
                    edtField.setError(null);
                    return true;
                }
            }
            case R.id.edt_comp_number: {
                if (text == null || text.equals("") || text.length() == 0 || text.length() < 10) {
                    edtField.setError("This field cannot be blank!!");
                    return false;
                } else if (!StringUtils.isNumeric(text)) {
                    edtField.setError("Please enter valid number!!");
                    return false;
                } else {
                    edtField.setError(null);
                    return true;
                }

            }

            default: {
                if (text == null || text.equals("") || text.length() == 0) {
                    edtField.setError("This field cannot be blank!!");
                    return false;
                } else {
                    edtField.setError(null);
                    return true;
                }
            }

        }
    }

    private View.OnClickListener headerViewClick= new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            switch (view.getId()){
                case R.id.txt_prod_details_header:
                    toggleVisibility(product_details);
                    break;
                case R.id.txt_ship_details_header:
                    toggleVisibility(shipping_details);
                    break;
                case R.id.txt_contact_details_header:
                    toggleVisibility(contact_details);
                    break;
                case R.id.img_close:
                    NewCompanyRequestActivity.this.finish();
                    break;
            }
        }
    };
    private void beginRequest(){
        if(!pDialog.isShowing()){
            pDialog.show();
        }
    }

    private void finishRequest(String message){
        if(pDialog.isShowing()){
            pDialog.dismiss();
        }
        Toolbox.showToastShort(this, message);
    }

    SyncListener newCompregListener= new SyncListener() {
        @Override
        public void onSyncStart() {
        beginRequest();
        }

        @Override
        public void onSyncProgress(float percentProgress, long requestTimestamp) {

        }

        @Override
        public void onSyncSuccess(String url, Object responseObject, long requestTimestamp) {
            if(pDialog.isShowing())
                pDialog.dismiss();
            UserEnquiryResp resp= (UserEnquiryResp) responseObject;
            CustomDialog.createCustomDialog(NewCompanyRequestActivity.this, resp.getResponseMsg(), "OK", null, null, false, new DialogListener() {
                @Override
                public void onPositiveBtnClick() {
                    NewCompanyRequestActivity.this.finish();
                }

                @Override
                public void onNegativeBtnClick() {

                }

                @Override
                public void onMiddleBtnClick() {

                }
            });
        }

        @Override
        public void onSyncFailure(String url, String reason, long requestTimestamp) {
            if(pDialog.isShowing())
                pDialog.dismiss();

            CustomDialog.createCustomDialog(NewCompanyRequestActivity.this,reason,"OK",null,null,true,null);
        }
    };

View.OnClickListener proceedListener=new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        if(validateField(edt_address) && validateField(edt_comp_name) && validateField(edt_comp_ctctperson)
                && validateField(edt_comp_number) && validateField(edt_desig)
                && validateField(edt_email) && categoriesSpinner.getSelectedItemPosition()!=0 && countriesSpinner.getSelectedItemPosition()!=0) {
            newCompRequest = new NewCompanyRegReq();
            newCompRequest.setAddress(edt_address.getText().toString());
            newCompRequest.setProductKeywords(keywords.getText().toString());
            newCompRequest.setCompanyName(edt_comp_name.getText().toString());
            newCompRequest.setContactNumber(edt_comp_number.getText().toString());
            newCompRequest.setCountry(countriesSpinner.getSelectedItem().toString());
            newCompRequest.setContactPerson(edt_comp_ctctperson.getText().toString());
            newCompRequest.setDesignation(edt_desig.getText().toString());
            newCompRequest.setEmailId(edt_email.getText().toString());
            newCompRequest.setType(((TextView) selectedView).getText().toString());
            newCompRequest.setCategory(categoriesSpinner.getSelectedItem().toString());
            SyncManager.sendNewCompRequest(newCompRequest, newCompregListener, NewCompanyRequestActivity.this, null);
        }else{
            Toolbox.showToastShort(NewCompanyRequestActivity.this,"Data is not proper.Please check all fields.");
        }
        }
};

    @Override
    public void onSyncStart() {

    }

    @Override
    public void onSyncProgress(float percentProgress, long requestTimestamp) {

    }

    @Override
    public void onSyncSuccess(String url, Object responseObject, long requestTimestamp) {

        countries= (List<String>) responseObject;
        Toolbox.writeToLog("countries "+countries.toString());
        if(countries!=null) {
            String c=new String("Select country..");

            countries.add(0,c);
            if (countriesSpinner.getAdapter() == null) {
                countriesAdapter = new ArrayAdapter<String>(this, R.layout.cats_spinner_dropdown, countries);
                countriesAdapter.setDropDownViewResource(R.layout.cats_spinner_dropdown);
                countriesSpinner.setAdapter(countriesAdapter);
            } else {
                countriesAdapter.clear();
                for(String ch: countries){
                    countriesAdapter.add(ch);
                }
                countriesAdapter.notifyDataSetChanged();
            }
        }


//        countriesSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> adapterView) {
//
//            }
//        });


    }

    @Override
    public void onSyncFailure(String url, String reason, long requestTimestamp) {
        NewCompanyRequestActivity.this.finish();
    }
}
