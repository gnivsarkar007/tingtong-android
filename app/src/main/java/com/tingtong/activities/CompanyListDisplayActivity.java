package com.tingtong.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.view.MenuItem;
import android.widget.Toast;

import com.tingtong.R;

import org.jetbrains.annotations.NotNull;

import butterknife.ButterKnife;
import butterknife.InjectView;
import to.done.lib.Constants;
import to.done.lib.database.DBManager;
import to.done.lib.images.util.ImageFetcher;
import to.done.lib.utils.Toolbox;

/**
 * Created by HP on 7/23/2014.
 */
public class CompanyListDisplayActivity extends BaseActivity {

    @InjectView(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    public static String TAG="CompanyListDisplayActivity";
    private DBManager dbManager;
    private Intent startingIntent;
    private boolean isCompanyPage=true;

    private static ImageFetcher fetcher;

    private ProgressDialog pDialog;


    private long back_pressed;
    private Toast toast;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.entity_list_page);
        ButterKnife.inject(this);
        startingIntent=getIntent();

        if(savedInstanceState==null)
        Toolbox.changeScreen(this,Constants.SCREEN_COMPANY_LIST,true,createBundleForScreen(Constants.SCREEN_COMPANY_LIST));

}
public Bundle createBundleForScreen(int screen){
   Bundle bundle = new Bundle();

    switch (screen){
       case Constants.SCREEN_COMPANY_LIST:{
           bundle.putString(Constants.INTENT_EXTRA_ENTITY_KEY,Constants.INTENT_EXTRA_COMPANY_PAGE);
       } break;

       default:break;
   }

    return bundle;
}
    @Override
    protected void onResume() {
        super.onResume();
        pDialog= new ProgressDialog(this);
        pDialog.setMessage(getResources().getString(R.string.search));
        pDialog.setCancelable(false);

    }

    @Override
    protected void onPause() {

        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        localBroadcastManager.unregisterReceiver(screenChangeReciever);
//        localBroadcastManager.unregisterReceiver(actionBarChangeReciever);

    }

    private void replaceContainerWithFrag(Fragment frag, boolean addToBackstack) {
        replaceContainerWithFrag(frag, addToBackstack, null);
    }

    private void replaceContainerWithFrag(Fragment frag, boolean addToBackstack, Bundle b) {


        if (b != null) {
            frag.setArguments(b);
        }
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.right_to_left_enter,R.anim.right_to_left_exit,R.anim.left_to_right_enter,R.anim.left_to_right_exit);
        ft.replace(R.id.container, frag);
        if (addToBackstack)
            ft.addToBackStack("screen_id_" + currentScreenId);
        ft.commit();
    }

    private void replaceContainerWithFrag(Fragment frag, boolean addToBackstack, int animEnter, int animExit, int animEnterPop, int animExitPop, Bundle b) {
        if (b == null)
            b = new Bundle();
        frag.setArguments(b);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(animEnter, animExit, animEnterPop, animExitPop);
        ft.replace(R.id.container, frag);

        if (addToBackstack)
            ft.addToBackStack("screen_id_" + currentScreenId);
        ft.commit();
    }

    private void replaceContainerWithFrag(Fragment frag, boolean addToBackstack, int animEnter, int animExit, int animEnterPop, int animExitPop) {

        replaceContainerWithFrag(frag, addToBackstack, animEnter, animExit, animEnterPop, animExitPop, null);

    }
    @Override
    public void onBackPressed() {
        FragmentManager fm = getSupportFragmentManager();
        if (fm.getBackStackEntryCount() > 1) {
            fm.popBackStack();//Immediate("screen_id_"+0,0);
        } else {
                finish();
    }}

    @Override
    public boolean onOptionsItemSelected(@NotNull MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return false;
        }else if(id==android.R.id.home){
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
    public void beginRequest(){
        if(!pDialog.isShowing()){
            pDialog.show();
        }
    }

    public void finishRequest(String message){
        if(pDialog.isShowing()){
            pDialog.dismiss();
        }
        Toolbox.showToastShort(this,message);
    }

    @Override
    public void backPressHandle() {
        onBackPressed();
    }

}

//    private List<IOnRecieveListChangeListener>subscribers=new LinkedList<IOnRecieveListChangeListener>();
//
//
//    public void addListener(IOnRecieveListChangeListener object){
//        subscribers.add(object);
//
//    }
//    public void removeListener(IOnRecieveListChangeListener object){
//        subscribers.remove(object);
//
//    }
//        private void sendListChangeEvent(){
//            for(IOnRecieveListChangeListener object:subscribers){
//                object.listChanged();
//            }
//        }
//    public static ImageFetcher getFetcherInstance(Context context) {
//
//        if (fetcher == null) {
//            fetcher = new ImageFetcher(context, 300, 300);
//    //        fetcher.setLoadingImage(R.drawable.img_placeholder);
//            fetcher.setImageFadeIn(true);
//        }
//        return fetcher;
//    }
//    private BroadcastReceiver screenChangeReciever= new BroadcastReceiver() {
//        @Override
//        public void onReceive(Context context, Intent intent) {
//            int screen=intent.getIntExtra(Constants.SCREEN_ID,0);
//            currentScreenId = screen;
//            Bundle bundle=intent.getExtras().getBundle(Constants.BUNDLE_EXTRA);
//            boolean addToBackstack=intent.getBooleanExtra(Constants.ADD_TO_BACKSTACK,false);
//            Toolbox.writeToLog(""+TAG+" "+addToBackstack+" "+screen+" "+bundle);
//            switch (screen){
//                case Constants.SCREEN_ENTITY_LIST:{
//                    Fragment elFragment= new CompanyListFragment();
//                    replaceContainerWithFrag(elFragment,addToBackstack,bundle);
//                    //currentScreenId=Constants.SCREEN_ENTITY_LIST;
//
//                }
//                break;
//                case Constants.SCREEN_COMPANY_DESCR_PAID:
//                {
//                    Fragment elFragment= new PaidCompanyDescriptionFragment();
//                    replaceContainerWithFrag(elFragment,addToBackstack,bundle);
//                    //currentScreenId=Constants.SCREEN_COMPANY_DESCR_PAID;
//               }
//                    break;
//                case Constants.SCREEN_ENTITY_DESCR_UNPAID:
//                {
//                    Fragment elFragment= new UnpaidCompanyDescriptionFrag();
//                    replaceContainerWithFrag(elFragment,addToBackstack,bundle);
//                    //urrentScreenId=Constants.SCREEN_ENTITY_DESCR_UNPAID;
//                }
//                break;
//
//                case Constants.SCREEN_MAP:
//                {
//                    Fragment elFragment=new MapFrag();
//                    replaceContainerWithFrag(elFragment,addToBackstack,bundle);
//                } break;
//                default:break;
//            }
//        }
//    };

//    private BroadcastReceiver actionBarChangeReciever=new BroadcastReceiver() {
//        @Override
//        public void onReceive(Context context, Intent intent) {
//
//            int actionbarLayoutType=intent.getIntExtra(Constants.AB_LAYOUT_TYPE,101);
//            Toolbox.writeToLog("Actionbar layout type switch :"+actionbarLayoutType);
//            switch (actionbarLayoutType){
//                case Constants.AB_LAYOUT_NORMAL:{
//                    if(titleLayout==null)
//                        titleLayout=CompanyListDisplayActivity.this.getLayoutInflater().inflate(R.layout.ab_title_layout,null);
//                    ImageView imgBack= (ImageView) titleLayout.findViewById(R.id.img_actionbar_back);
//                    imgBack.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View view) {
//                            CompanyListDisplayActivity.this.onBackPressed();
//                        }
//                    });
//
//                }
//                    getSupportActionBar().setCustomView(titleLayout);
//                    getSupportActionBar().setDisplayShowCustomEnabled(true);
//                    break;
//
//
//                case Constants.AB_LAYOUT_SEARCH:{
//                    if(searchLayout==null)
//                        searchLayout=CompanyListDisplayActivity.this.getLayoutInflater().inflate(R.layout.ab_search_layout,null);
////                    ImageView imgBack= (ImageView) searchLayout.findViewById(R.id.img_actionbar_back);
////                    imgBack.setOnClickListener(new View.OnClickListener() {
////                        @Override
////                        public void onClick(View view) {
////                            CompanyListDisplayActivity.this.onBackPressed();
////                        }
////                    });
//                    final FontEditText edtSearch= (FontEditText) searchLayout.findViewById(R.id.edt_search_bar);
//                    ImageView imgSearch= (ImageView) searchLayout.findViewById(R.id.img_actionbar_search);
//                    imgSearch.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View view) {
//                            if(edtSearch.getVisibility()==View.INVISIBLE){
//                                edtSearch.setVisibility(View.VISIBLE);
//                            }else {
//                                if (edtSearch.getText() == null || edtSearch.getText().toString().equals("") || edtSearch.getText().toString().length() == 0) {
//                                    edtSearch.setVisibility(View.INVISIBLE);
//                                    Toolbox.showToastShort(CompanyListDisplayActivity.this, "Search string cannot be empty.");
//                                }else{
//                                  edtSearch.setVisibility(View.INVISIBLE);
//                                    try {
//                                            SyncManager.getCompanyByName(edtSearch.getText().toString(), queryCompanyByName, CompanyListDisplayActivity.this, App.getDatabaseInstance(CompanyListDisplayActivity.this));
//                                            edtSearch.getText().clear();
//                                    }catch (Exception e){
//                                        e.printStackTrace();
//                                    }
//                                }
//                            }
//
//                        }
//                    });
//                    getSupportActionBar().setCustomView(searchLayout);
//                    getSupportActionBar().setDisplayShowCustomEnabled(true);
//
//                }
//                    break;
//            }
//        }
//
//
//    };
//    public int currentScreenId=-1;
//    private LocalBroadcastManager localBroadcastManager;
//    private View searchLayout=null,titleLayout=null;