package com.tingtong.activities;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

import com.tingtong.App;
import com.tingtong.R;

import org.jetbrains.annotations.NotNull;

import butterknife.ButterKnife;
import butterknife.InjectView;
import to.done.lib.images.util.ImageFetcher;
import to.done.lib.sync.SyncListener;
import to.done.lib.sync.SyncManager;


public class SplashActivity extends ActionBarActivity {
    @InjectView(R.id.img_splash)
    ImageView splash;
    ImageFetcher fetcher;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(Build.VERSION.SDK_INT<16){
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }else{
            View decor=getWindow().getDecorView();
            int uiOptions=View.SYSTEM_UI_FLAG_HIDE_NAVIGATION|View.SYSTEM_UI_FLAG_FULLSCREEN;//View.SYSTEM_UI_FLAG_FULLSCREEN | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
            decor.setSystemUiVisibility(uiOptions);
            getSupportActionBar().hide();
        }
//        getSupportActionBar().setCustomView(R.layout.);
        setContentView(R.layout.activity_splash);
        ButterKnife.inject(this);

//        fetcher= new ImageFetcher(this,300,300);
//        fetcher.loadImage(Constants.SPLASH_URL,splash);

//        CountDownTimer timer = new CountDownTimer(5*1000L,1000L) {
//            @Override
//            public void onTick(long l) {
//                Toolbox.writeToLog("Time to Launch "+l/1000+" sec");
//            }
//
//            @Override
//            public void onFinish() {
//                Intent intent = new Intent(SplashActivity.this, SearchLandingPage.class);
//                startActivity(intent);
//                finish();
//            }
//        };
//        timer.start();

        SyncManager.getCompanyCategories(this,catsSyncListener,App.getDatabaseInstance(this));

    }

    @Override
    protected void onResume() {
        super.onResume();


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.splash, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NotNull MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    SyncListener catsSyncListener= new SyncListener() {
        @Override
        public void onSyncStart() {

        }

        @Override
        public void onSyncProgress(float percentProgress, long requestTimestamp) {

        }

        @Override
        public void onSyncSuccess(String url, Object responseObject, long requestTimestamp) {
            Intent intent = new Intent(SplashActivity.this, SearchLandingPage.class);
            startActivity(intent);
            finish();
        }

        @Override
        public void onSyncFailure(String url, String reason, long requestTimestamp) {
            Intent intent = new Intent(SplashActivity.this, SearchLandingPage.class);
                startActivity(intent);
                finish();
        }
    };
}
