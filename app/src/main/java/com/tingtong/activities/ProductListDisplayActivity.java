package com.tingtong.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.MenuItem;

import com.tingtong.R;

import org.jetbrains.annotations.NotNull;

import butterknife.ButterKnife;
import to.done.lib.Constants;
import to.done.lib.utils.Toolbox;

/**
 * Created by HP on 8/9/2014.
 */
public class ProductListDisplayActivity extends BaseActivity {
    private Intent startingIntent;

    private boolean isCompanyPage=true;
    private ProgressDialog pDialog;

    public boolean isCompanyPage() {
        return true;
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.entity_list_page);
        ButterKnife.inject(this);
        startingIntent=getIntent();

        if(savedInstanceState==null)
            Toolbox.changeScreen(this,Constants.SCREEN_PROD_LIST,true,createBundleForScreen(Constants.SCREEN_PROD_LIST));

//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setHomeAsUpIndicator(R.drawable.img_actionbar_back);
    }
    public Bundle createBundleForScreen(int screen){
        Bundle bundle = new Bundle();

        switch (screen){
            case Constants.SCREEN_PROD_LIST:{
                bundle.putString(Constants.INTENT_EXTRA_ENTITY_KEY,Constants.INTENT_EXTRA_COMPANY_PAGE);
            } break;

            default:break;
        }

        return bundle;
    }
    @Override
    protected void onResume() {
        super.onResume();
        pDialog= new ProgressDialog(this);
        pDialog.setMessage(getResources().getString(R.string.search));
        pDialog.setCancelable(false);

    }

    @Override
    protected void onPause() {

        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }


    @Override
    public void onBackPressed() {
        FragmentManager fm = getSupportFragmentManager();
        if (fm.getBackStackEntryCount() > 1) {
            fm.popBackStack();//Immediate("screen_id_"+0,0);

        } else {
            finish();
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NotNull MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return false;
        }else if(id==android.R.id.home){
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    public void beginRequest(){
        if(!pDialog.isShowing()){
            pDialog.show();
        }
    }



    public void finishRequest(String message){
        if(pDialog.isShowing()){
            pDialog.dismiss();
        }
        Toolbox.showToastShort(this,message);
    }

    @Override
    public void backPressHandle() {
        onBackPressed();
    }


}


//private BroadcastReceiver actionBarChangeReciever=new BroadcastReceiver() {
//        @Override
//        public void onReceive(Context context, Intent intent) {
//
//            int actionbarLayoutType=intent.getIntExtra(Constants.AB_LAYOUT_TYPE,101);
//            Toolbox.writeToLog("Actionbar layout type switch :"+actionbarLayoutType);
//            switch (actionbarLayoutType){
//                case Constants.AB_LAYOUT_NORMAL:{
//                    if(titleLayout==null)
//                        titleLayout=ProductListDisplayActivity.this.getLayoutInflater().inflate(R.layout.ab_title_layout,null);
//                    ImageView imgBack= (ImageView) titleLayout.findViewById(R.id.img_actionbar_back);
//                    imgBack.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View view) {
//                            ProductListDisplayActivity.this.onBackPressed();
//                        }
//                    });
//
//                }
//                getSupportActionBar().setCustomView(titleLayout);
//                getSupportActionBar().setDisplayShowCustomEnabled(true);
//                break;
//
//
//                case Constants.AB_LAYOUT_SEARCH:{
//                    if(searchLayout==null)
//                        searchLayout=ProductListDisplayActivity.this.getLayoutInflater().inflate(R.layout.ab_search_layout,null);
////                    ImageView imgBack= (ImageView) searchLayout.findViewById(R.id.img_actionbar_back);
////                    imgBack.setOnClickListener(new View.OnClickListener() {
////                        @Override
////                        public void onClick(View view) {
////                            CompanyListDisplayActivity.this.onBackPressed();
////                        }
////                    });
//                    final FontEditText edtSearch= (FontEditText) searchLayout.findViewById(R.id.edt_search_bar);
//                    ImageView imgSearch= (ImageView) searchLayout.findViewById(R.id.img_actionbar_search);
//                    imgSearch.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View view) {
//                            if(edtSearch.getVisibility()==View.INVISIBLE){
//                                edtSearch.setVisibility(View.VISIBLE);
//                            }else {
//                                if (edtSearch.getText() == null || edtSearch.getText().toString().equals("") || edtSearch.getText().toString().length() == 0) {
//                                    edtSearch.setVisibility(View.INVISIBLE);
//                                    Toolbox.showToastShort(ProductListDisplayActivity.this, "Search string cannot be empty.");
//                                }else{
//                                    edtSearch.setVisibility(View.INVISIBLE);
//                                    try {
//
//                                        edtSearch.getText().clear();
//                                    }catch (Exception e){
//                                        e.printStackTrace();
//                                    }
//                                }
//                            }
//
//                        }
//                    });
//                    getSupportActionBar().setCustomView(searchLayout);
//                    getSupportActionBar().setDisplayShowCustomEnabled(true);
//
//                }
//                break;
//            }
//        }
//
//
//    };
//    public static ImageFetcher getFetcherInstance(Context context) {
//
//        if (fetcher == null) {
//            fetcher = new ImageFetcher(context, 300, 300);
//    //        fetcher.setLoadingImage(R.drawable.img_placeholder);
//            fetcher.setImageFadeIn(true);
//        }
//        return fetcher;
//    }
//    private BroadcastReceiver screenChangeReciever= new BroadcastReceiver() {
//        @Override
//        public void onReceive(Context context, Intent intent) {
//            int screen=intent.getIntExtra(Constants.SCREEN_ID,0);
//            currentScreenId = screen;
//            Bundle bundle=intent.getExtras().getBundle(Constants.BUNDLE_EXTRA);
//            boolean addToBackstack=intent.getBooleanExtra(Constants.ADD_TO_BACKSTACK,false);
//            Toolbox.writeToLog("" + TAG + " " + addToBackstack + " " + screen + " " + bundle);
//            switch (screen){
//                case Constants.SCREEN_ENTITY_LIST:{
//                    Fragment elFragment= new CompanyListFragment();
//                    replaceContainerWithFrag(elFragment,addToBackstack,bundle);
//                    //currentScreenId=Constants.SCREEN_ENTITY_LIST;
//
//                }
//                break;
//                case Constants.SCREEN_COMPANY_DESCR_PAID:
//                {
//                    Fragment elFragment= new PaidCompanyForProductFragment();
//                    replaceContainerWithFrag(elFragment,addToBackstack,bundle);
//                    //currentScreenId=Constants.SCREEN_COMPANY_DESCR_PAID;
//                }
//                break;
//                case Constants.SCREEN_ENTITY_DESCR_UNPAID:
//                {
//                    Fragment elFragment= new UnpaidCompanyDescriptionFrag();
//                    replaceContainerWithFrag(elFragment,addToBackstack,bundle);
//                    //urrentScreenId=Constants.SCREEN_ENTITY_DESCR_UNPAID;
//                }
//                break;
//
//                case Constants.SCREEN_PROD_LIST:{
//                    Fragment elFragment= new ProductListFragment();
//                    replaceContainerWithFrag(elFragment,addToBackstack,bundle);
//                }
//                break;
//
//                case Constants.SCREEN_PROD_DESCR:{
//                    Fragment elFragment= new ProductDescrFrag();
//                    replaceContainerWithFrag(elFragment,addToBackstack,bundle);
//                }
//                break;
//                case Constants.SCREEN_MAP:
//                {
//                    Fragment elFragment=new MapFrag();
//                    replaceContainerWithFrag(elFragment,addToBackstack,bundle);
//                }
//                break;
//                default:break;
//            }
//        }
//    };

//