package com.tingtong.activities;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tingtong.R;

import org.apache.commons.lang3.StringUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.ButterKnife;
import butterknife.InjectView;
import to.done.lib.Constants;
import to.done.lib.entity.UserBuyRequest;
import to.done.lib.entity.UserEnquiryResp;
import to.done.lib.sync.SyncListener;
import to.done.lib.sync.SyncManager;
import to.done.lib.ui.CustomDialog;
import to.done.lib.ui.DialogListener;
import to.done.lib.ui.FontEditText;
import to.done.lib.utils.Toolbox;

/**
 * Created by HP on 9/6/2014.
 */
public class BuyingRequirementsActivity extends ActionBarActivity {
    Pattern pattern;
    Matcher matcher;
    @InjectView(R.id.txt_prod_details_header)
    TextView txt_prod_details_header;
    @InjectView(R.id.txt_ship_details_header)
    TextView txt_ship_details_header;
    @InjectView(R.id.txt_contact_details_header)
    TextView txt_contact_details_header;
    @InjectView(R.id.lin_details_holder)
    LinearLayout product_details;
    @InjectView(R.id.lin_shipping_details_holder)
    LinearLayout shipping_details;
    @InjectView(R.id.lin_contact_details_holder)
    LinearLayout contact_details;
    @InjectView(R.id.img_close)
    ImageView close;
    @InjectView(R.id.edt_prod_name)
    FontEditText edt_prod_name;
    @InjectView(R.id.edt_prod_quant)
    FontEditText edt_prod_qty;
    @InjectView(R.id.edt_prod_descr)
    FontEditText edt_prod_descr;
    @InjectView(R.id.edt_pay_term)
    FontEditText edt_pay_term;
    @InjectView(R.id.edt_delivery_port)
    FontEditText edt_delivery_port;
    @InjectView(R.id.edt_pack_details)
    FontEditText edt_pack_details;
    @InjectView(R.id.edt_mobile)
    FontEditText edt_mobile;
    @InjectView(R.id.edt_name)
    FontEditText edt_name;
    @InjectView(R.id.edt_email)
    FontEditText edt_email;
    @InjectView(R.id.edt_comp_name)
    FontEditText edt_comp_name;
    @InjectView(R.id.edt_comp_addr)
    FontEditText edt_comp_addr;
    @InjectView(R.id.proceed)
    TextView proceed;
    UserBuyRequest request=null;
    ProgressDialog pDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.post_buying_request);
        ButterKnife.inject(this);
        getSupportActionBar().hide();
    }

    FontEditText.EditCancelListener validations= new FontEditText.EditCancelListener() {
        @Override
        public void onCancelEvent(FontEditText editText, int cancelCode) {
        validateField(editText);
            if(validateField(edt_prod_name) && validateField(edt_prod_qty) &&validateField(edt_prod_descr)){
                txt_prod_details_header.setTextColor(BuyingRequirementsActivity.this.getResources().getColor(R.color.product_price_green));
            }else{
                txt_prod_details_header.setTextColor(BuyingRequirementsActivity.this.getResources().getColor(R.color.theme_blue));
            }
            if(validateField(edt_delivery_port) && validateField(edt_pack_details) &&validateField(edt_pay_term)){
                txt_ship_details_header.setTextColor(BuyingRequirementsActivity.this.getResources().getColor(R.color.product_price_green));
            }else{
                txt_ship_details_header.setTextColor(BuyingRequirementsActivity.this.getResources().getColor(R.color.theme_blue));
            }
            if(validateField(edt_name) && validateField(edt_mobile) && validateField(edt_email) && validateField(edt_comp_addr) && validateField(edt_comp_name)){
                txt_contact_details_header.setTextColor(BuyingRequirementsActivity.this.getResources().getColor(R.color.product_price_green));
            }else{
                txt_contact_details_header.setTextColor(BuyingRequirementsActivity.this.getResources().getColor(R.color.theme_blue));
            }

        }
    };

    private View.OnClickListener headerViewClick= new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            switch (view.getId()){
                case R.id.txt_prod_details_header:
                    toggleVisibility(product_details);
                    break;
                case R.id.txt_ship_details_header:
                    toggleVisibility(shipping_details);
                    break;
                case R.id.txt_contact_details_header:
                    toggleVisibility(contact_details);
                    break;
                case R.id.img_close:
                    BuyingRequirementsActivity.this.finish();
                    break;
            }
        }
    };
    @Override
    protected void onResume() {
        super.onResume();
        pDialog=new ProgressDialog(this);
        pDialog.setCancelable(false);
        pDialog.setMessage("Sending request...");
        pattern=Pattern.compile(Constants.EMAIL_PATTERN);
        txt_contact_details_header.setOnClickListener(headerViewClick);
        txt_prod_details_header.setOnClickListener(headerViewClick);
        txt_ship_details_header.setOnClickListener(headerViewClick);
        close.setOnClickListener(headerViewClick);
        edt_prod_name.addEditCancelListener(validations);
        edt_prod_qty.addEditCancelListener(validations);
        edt_prod_descr.addEditCancelListener(validations);
        edt_delivery_port.addEditCancelListener(validations);
        edt_pay_term.addEditCancelListener(validations);
        edt_pack_details.addEditCancelListener(validations);
        edt_email.addEditCancelListener(validations);
        edt_mobile.addEditCancelListener(validations);
        edt_name.addEditCancelListener(validations);
        edt_comp_name.addEditCancelListener(validations);
        edt_comp_addr.addEditCancelListener(validations);
        proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(validateField(edt_prod_name) && validateField(edt_prod_qty) &&validateField(edt_prod_descr) &&
                        validateField(edt_delivery_port) && validateField(edt_pack_details) &&validateField(edt_pay_term) &&
                        validateField(edt_name) && validateField(edt_mobile) && validateField(edt_email) && validateField(edt_comp_addr) && validateField(edt_comp_name) ){
                    request=new UserBuyRequest();
                    request.setProductName(edt_prod_name.getText().toString());
                    request.setProductQty(edt_prod_qty.getText().toString());
                    request.setDescription(edt_prod_descr.getText().toString());
                    request.setPackingDetail(edt_pack_details.getText().toString());
                    request.setPaymentTerm(edt_pay_term.getText().toString());
                    request.setPortOfDelivery(edt_delivery_port.getText().toString());
                    request.setName(edt_name.getText().toString());
                    request.setEmailId(edt_email.getText().toString());
                    request.setPhoneNumber(edt_mobile.getText().toString());
                    request.setCompanyName(edt_comp_name.getText().toString());
                    request.setAddress(edt_comp_addr.getText().toString());
                    SyncManager.sendUserBuyRequest(request, sendReq, BuyingRequirementsActivity.this, null);
                }
            }
        });
    }

    private void toggleVisibility(View view){
        if(view.getVisibility()==View.VISIBLE){
            view.setVisibility(View.GONE);
        }else{
            view.setVisibility(View.VISIBLE);
        }
    }



    private boolean validateField(EditText edtField) {
        String text = edtField.getText().toString();
        switch (edtField.getId()) {
            case R.id.edt_email: {
                if (text == null || text.equals("") || text.length() == 0) {
                    edtField.setError("This field cannot be blank!!");
                    return false;
                } else {
                    matcher=pattern.matcher(text);
                    if (matcher.matches()) {

                        edtField.setError(null);
                        return true;
                    } else {
                        edtField.setError("Please enter valid email!!");
                        return false;
                    }

                }
            }

            case R.id.edt_mobile: {
                if (text == null || text.equals("") || text.length() == 0 || text.length() < 10) {
                    edtField.setError("This field cannot be blank!!");
                    return false;
                }else if(!StringUtils.isNumeric(text)){
                    edtField.setError("Please enter valid number!!");
                    return false;
                }
                else {
                    edtField.setError(null);
                    return true;
                }
            }


            default: {
                if (text == null || text.equals("") || text.length() == 0) {
                    edtField.setError("This field cannot be blank!!");
                    return false;
                } else {
                    edtField.setError(null);
                    return true;
                }
            }

        }

    }
    private void beginRequest(){
        if(!pDialog.isShowing()){
            pDialog.show();
        }
    }

    private void finishRequest(String message){
        if(pDialog.isShowing()){
            pDialog.dismiss();
        }
        if(message!=null)
        Toolbox.showToastShort(this, message);
    }

    SyncListener sendReq= new SyncListener() {
        @Override
        public void onSyncStart() {
            beginRequest();
        }

        @Override
        public void onSyncProgress(float percentProgress, long requestTimestamp) {

        }

        @Override
        public void onSyncSuccess(String url, Object responseObject, long requestTimestamp) {
            UserEnquiryResp resp= (UserEnquiryResp) responseObject;
            finishRequest(null);
            CustomDialog.createCustomDialog(BuyingRequirementsActivity.this, resp.getResponseMsg(), "OK", null, null, true, new DialogListener() {
                @Override
                public void onPositiveBtnClick() {
                    BuyingRequirementsActivity.this.finish();
                }

                @Override
                public void onNegativeBtnClick() {

                }

                @Override
                public void onMiddleBtnClick() {

                }
            });
        }

        @Override
        public void onSyncFailure(String url, String reason, long requestTimestamp) {
        finishRequest(reason);
        }
    };
}
