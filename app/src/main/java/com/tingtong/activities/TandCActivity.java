package com.tingtong.activities;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;
import android.widget.Button;

import com.tingtong.R;

/**
 * Created by HP on 10/31/2014.
 */
public class TandCActivity extends Activity {
    WebView web;
    Button accept,reject;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(android.R.style.Theme_Dialog);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tandc_dialog_layout);

         web= (WebView) findViewById(R.id.content);
         accept= (Button) findViewById(R.id.accept);
         reject= (Button) findViewById(R.id.dont_accept);
        web.loadUrl("file:///android_asset/tandc.html");
    }

}
