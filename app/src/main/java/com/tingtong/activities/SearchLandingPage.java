package com.tingtong.activities;

import android.app.AlertDialog;
import android.app.Application;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.tingtong.App;
import com.tingtong.R;
import com.tingtong.fragments.LeftPanelFragment;
import com.tingtong.fragments.RighPanelFragment;

import org.jetbrains.annotations.NotNull;

import java.io.InputStream;
import java.security.MessageDigest;

import butterknife.ButterKnife;
import butterknife.InjectView;
import to.done.lib.Constants;
import to.done.lib.database.DBManager;
import to.done.lib.entity.ResponseSearchByProductName;
import to.done.lib.entity.ResponseSearchCompanyByName;
import to.done.lib.sync.SyncListener;
import to.done.lib.sync.SyncManager;
import to.done.lib.ui.CustomDialog;
import to.done.lib.ui.DialogListener;
import to.done.lib.ui.FontTextView;
import to.done.lib.utils.GCMRegisterManager;
import to.done.lib.utils.SharedPreferencesManager;
import to.done.lib.utils.Toolbox;

/**
 * Created by HP on 7/23/2014.
 */
public class SearchLandingPage extends ActionBarActivity implements LeftPanelFragment.IOnFragmentPanelItemClick {

    @InjectView(R.id.edt_company_search)
    EditText edt_search_box;

    @InjectView(R.id.txt_product_search)
    FontTextView txt_search;
    @InjectView(R.id.txt_buying_request)
    FontTextView txt_buy;

    @InjectView(R.id.txt_new_request)
    FontTextView txt_new_request;

    @InjectView(R.id.rg_search_type)
    RadioGroup searchType;

    @InjectView(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    ImageView drawerHandle;
    private Application app;
    private DBManager dbManager;
    private ProgressDialog pDialog;
    Fragment right=null,left=null;
    SharedPreferences preferencesManager;
    SharedPreferences.Editor editor;
    boolean proceed;
    public static final int ACCEPT=101,REJECT=102,TANDC=100;

    private RadioGroup.OnCheckedChangeListener checkedChangeListener=new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup radioGroup, int i) {

        }
    };
    ImageView rightPanelHandle;
    public Application getApp() {
        return app;
    }

    public void setApp(Application app) {
        this.app = app;
    }

    public DBManager getDbManager() {
        return dbManager;
    }
    public void setDbManager(DBManager dbManager) {
        this.dbManager = dbManager;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        GCMRegisterManager.registerToGCM(this, getResources().getString(R.string.gcm_sender_id));

        super.onCreate(savedInstanceState);
//        getSupportActionBar().hide();
        if (getIntent().hasExtra("message")) {
            String msg = getIntent().getStringExtra("message");
            String title = getIntent().getStringExtra("title");
            Log.e("Main", "received msg:" + msg);
            showGCMPopup(title, msg);
        }

        setContentView(R.layout.landing_search_page);
 //       String message=AssetJSONFile("tandc.txt",this);

        ButterKnife.inject(this);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        View titleLayout=this.getLayoutInflater().inflate(R.layout.ab_both_panels_layout,null);
        rightPanelHandle=(ImageView)titleLayout.findViewById(R.id.img_right_panel_handle);
        getSupportActionBar().setCustomView(titleLayout);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.img_drawer_icon);
        preferencesManager = SharedPreferencesManager.getSharedPreferences(this);
        //databaseInstance = DBManager.getInstance(getActivity());
        editor = preferencesManager.edit();
        proceed=preferencesManager.getBoolean(Constants.PREFS_TAND_ACCEPTED,false);
        if(!proceed){
            createTermsAndConditionsDialog();
        }

    }


    private void createTermsAndConditionsDialog(){

        final Dialog smDialog= new Dialog(this);
        smDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(smDialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        smDialog.setContentView(R.layout.tandc_dialog_layout);
        smDialog.setCancelable(false);
        WebView web= (WebView) smDialog.findViewById(R.id.content);
        Button accept= (Button) smDialog.findViewById(R.id.accept);
        Button reject= (Button) smDialog.findViewById(R.id.dont_accept);

        web.loadUrl("file:///android_asset/tandc.html");   // now it will not fail here

        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editor.putBoolean(Constants.PREFS_TAND_ACCEPTED,true);
                editor.commit();
                smDialog.dismiss();
            }
        });

        reject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              editor.putBoolean(Constants.PREFS_TAND_ACCEPTED,false);
              editor.commit();
              SearchLandingPage.this.finish();
//
            }
        });
        smDialog.show();
        smDialog.getWindow().setAttributes(lp);

//        CustomDialog.createCustomDialog(this,text,"Agree","Dont Agree",null,false,new DialogListener() {
//            @Override
//            public void onPositiveBtnClick() {
//                editor.putBoolean(Constants.PREFS_TAND_ACCEPTED,true);
//                editor.commit();
//
//            }
//
//            @Override
//            public void onNegativeBtnClick() {
//                editor.putBoolean(Constants.PREFS_TAND_ACCEPTED,false);
//                editor.commit();
//                SearchLandingPage.this.finish();
//
//            }
//
//            @Override
//            public void onMiddleBtnClick() {
//
//            }
//        });

    }

    private void createAboutUsDialog() {

        final Dialog smDialog = new Dialog(this);
        smDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(smDialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        smDialog.setContentView(R.layout.about_us);
        smDialog.setCancelable(true);
        WebView web = (WebView) smDialog.findViewById(R.id.content);
        Button accept = (Button) smDialog.findViewById(R.id.accept);
        Button reject = (Button) smDialog.findViewById(R.id.dont_accept);

        web.loadUrl("file:///android_asset/about_us.html");   // now it will not fail here

        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                editor.putBoolean(Constants.PREFS_TAND_ACCEPTED, true);
//                editor.commit();
                smDialog.dismiss();
            }
        });

        reject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editor.putBoolean(Constants.PREFS_TAND_ACCEPTED, false);
                editor.commit();
                SearchLandingPage.this.finish();
//
            }
        });
        smDialog.show();
        smDialog.getWindow().setAttributes(lp);
    }

    public static String AssetJSONFile (String filename, Context context)  {
       try {
           AssetManager manager = context.getAssets();
           InputStream file = manager.open(filename);
           byte[] formArray = new byte[file.available()];
           file.read(formArray);
           file.close();

           return new String(formArray);
       }catch (Exception e){
           e.printStackTrace();
           return null;
       }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Log.e("Main", "onNewIntent()");
        if (intent.hasExtra("message")) {
            String msg = intent.getStringExtra("message");
            String title = intent.getStringExtra("title");
            showGCMPopup(title, msg);
        }

    }


    public void showGCMPopup(String title, String msg) {
//        GCMPopupDialogFragment gcmDialog = new GCMPopupDialogFragment();
//        Bundle bundle = new Bundle();
//        bundle.putString("message", msg);
//        gcmDialog.setArguments(bundle);
//        gcmDialog.show(getSupportFragmentManager(), "");
 //       GoogleAnalyticsManager.sendScreenView(this, Constants.GA_SCREEN_NOTIFICATION_POPUP);
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.gcm_popup_dialog);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(false);

        Button okButton = (Button) dialog.findViewById(R.id.gcm_popup_ok_button);
        TextView gcm_popup_text = (TextView) dialog.findViewById(R.id.gcm_popup_text);
        TextView gcm_popup_title_text = (TextView) dialog.findViewById(R.id.gcm_popup_title_text);

        if (msg != null) {

            gcm_popup_text.setText(msg);
        }
        if (title != null)
            gcm_popup_title_text.setText(title);

        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();

    }


    public void findHash(){
        PackageInfo info;
        try {

            info = getPackageManager().getPackageInfo(
                    "com.tingtong", PackageManager.GET_SIGNATURES);

            for (android.content.pm.Signature signature : info.signatures) {
                MessageDigest md;
                md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String something = new String(Base64.encode(md.digest(), 0));
                Log.e("Hash key", something);
                System.out.println("Hash key" + something);
            }

        } catch (Exception e) {
            Log.e("exception", e.toString());
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (checkPlayServices()) {
            drawerLayout.setScrimColor(Color.TRANSPARENT);
            rightPanelHandle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (drawerLayout.isDrawerOpen(Gravity.RIGHT)) {
                        drawerLayout.closeDrawer(Gravity.RIGHT);
                    } else drawerLayout.openDrawer(Gravity.RIGHT);

                    if (drawerLayout.isDrawerOpen(Gravity.LEFT)) {
                        drawerLayout.closeDrawer(Gravity.LEFT);
                    }
                }
            });
            dbManager = App.getDatabaseInstance(this);
            txt_search.setOnClickListener(prodSearch);
            pDialog = new ProgressDialog(this);
            pDialog.setMessage(getResources().getString(R.string.search));
            pDialog.setCancelable(false);
            txt_buy.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(SearchLandingPage.this, BuyingRequirementsActivity.class);
                    startActivity(intent);
                }
            });
            txt_new_request.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(SearchLandingPage.this, NewCompanyRequestActivity.class);
                    startActivity(intent);
                }
            });

            if (right == null) {
                right = new RighPanelFragment();
                replaceContainerWithFrag(right, false, R.id.right_drawer);
            }

            if (left == null) {
                left = new LeftPanelFragment();
                replaceContainerWithFrag(left, false, R.id.left_drawer);

            }

//        drawerHandle.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if(drawerLayout.isDrawerOpen(Gravity.LEFT)){
//                    drawerLayout.closeDrawer(Gravity.LEFT);
//                }else{
//                    drawerLayout.openDrawer(Gravity.LEFT);
//            }
//            }
//        });
        }
    }
//    drawerLayout.setDrawerListener(new DrawerLayout.DrawerListener() {
//
//        @Override
//        public void onDrawerSlide(View drawerView, float slideOffset) {
//
//            if(slideOffset<0.5)
//            {
//                ObjectAnimator.ofFloat(drawerHandle, "translationX", -drawerHandle.getWidth() * slideOffset).start();
//            }
//
//        }
//
//        @Override
//        public void onDrawerOpened(View drawerView) {
//        }
//
//        @Override
//        public void onDrawerClosed(View drawerView) {
//
//        }
//
//        @Override
//        public void onDrawerStateChanged(int newState) {
//        }
//    });

    @Override
    public boolean onOptionsItemSelected(@NotNull MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return false;
        }else if(id==android.R.id.home){
            if(drawerLayout.isDrawerOpen(Gravity.LEFT)){
                drawerLayout.closeDrawer(Gravity.LEFT);
            }else{
                drawerLayout.openDrawer(Gravity.LEFT);
            }
        }
        return super.onOptionsItemSelected(item);
    }


    private void beginRequest(){
        if(!pDialog.isShowing()){
            pDialog.show();
        }
    }

    private void finishRequest(String message){
        if(pDialog.isShowing()){
            pDialog.dismiss();
        }
        Toolbox.showToastShort(this,message);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

//    private View.OnClickListener compSearch= new View.OnClickListener() {
//        @Override
//        public void onClick(View view) {
//      try {
//          if(validateTextView(edt_company_seach))
//          SyncManager.getCompanyByName(edt_company_seach.getText().toString(), queryCompanyByName, SearchLandingPage.this, dbManager);
//else{
//              Toolbox.showToastShort(SearchLandingPage.this,"Search string cannot be Empty!!");
//          }
//      }catch (Exception e){
//          e.printStackTrace();
//      }
//        }
//    };
    private View.OnClickListener prodSearch= new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            try {
                if(validateTextView(edt_search_box)) {
                    switch (searchType.getCheckedRadioButtonId()) {
                        case R.id.radio_product:
                            SyncManager.getProductByName(edt_search_box.getText().toString(), queryProductByName, SearchLandingPage.this, dbManager);
                            break;
                        case R.id.radio_company:
                            SyncManager.getCompanyByName(edt_search_box.getText().toString(), queryCompanyByName, SearchLandingPage.this, dbManager);
                            break;
                    }
                }else{
                    Toolbox.showToastShort(SearchLandingPage.this, "Search string cannot be Empty!!");
                }

            }catch (Exception e){
                e.printStackTrace();
            }
        }
    };
    private SyncListener queryCompanyByName= new SyncListener() {
        @Override
        public void onSyncStart() {
        beginRequest();
        }

        @Override
        public void onSyncProgress(float percentProgress, long requestTimestamp) {

        }

        @Override
        public void onSyncSuccess(String url, Object responseObject, long requestTimestamp) {
            pDialog.dismiss();
            ResponseSearchCompanyByName resp=(ResponseSearchCompanyByName)responseObject;
//            finishRequest(resp.getResponseMsg());
            Intent intent=new Intent(SearchLandingPage.this, CompanyListDisplayActivity.class);
            intent.putExtra(Constants.INTENT_EXTRA_ENTITY_KEY,Constants.INTENT_EXTRA_COMPANY_PAGE);
            startActivity(intent);
        }

        @Override
        public void onSyncFailure(String url, String reason, long requestTimestamp) {
            pDialog.dismiss();
            finishRequest(reason);
            Intent intent=new Intent(SearchLandingPage.this, CompanyListDisplayActivity.class);
            intent.putExtra(Constants.INTENT_EXTRA_ENTITY_KEY,Constants.INTENT_EXTRA_COMPANY_PAGE);
            startActivity(intent);
        }
    };

    private SyncListener queryProductByName= new SyncListener() {
        @Override
        public void onSyncStart() {
            beginRequest();
        }

        @Override
        public void onSyncProgress(float percentProgress, long requestTimestamp) {

        }

        @Override
        public void onSyncSuccess(String url, Object responseObject, long requestTimestamp) {
            pDialog.dismiss();
            ResponseSearchByProductName resp=(ResponseSearchByProductName)responseObject;
            finishRequest(resp.getResponseMsg());
            App.writeToSD();
            Intent intent=new Intent(SearchLandingPage.this, ProductListDisplayActivity.class);
            intent.putExtra(Constants.INTENT_EXTRA_ENTITY_KEY,Constants.INTENT_EXTRA_COMPANY_PAGE);
            startActivity(intent);
        }

        @Override
        public void onSyncFailure(String url, String reason, long requestTimestamp) {
            pDialog.dismiss();
            finishRequest(reason);
        }
    };

    private boolean validateTextView(EditText edt){
        return (edt.getText().toString()!=null && edt.getText().toString().length()>0 && !" ".equalsIgnoreCase(edt.getText().toString()));

    }

    private void replaceContainerWithFrag(Fragment frag, boolean addToBackstack,int layoutToReplace) {
        replaceContainerWithFrag(frag, addToBackstack, null,layoutToReplace);
    }

    private void replaceContainerWithFrag(Fragment frag, boolean addToBackstack, Bundle b,int layoutToReplace) {


        if (b != null) {
            frag.setArguments(b);
        }
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.right_to_left_enter,R.anim.right_to_left_exit,R.anim.left_to_right_enter,R.anim.left_to_right_exit);
        ft.replace(layoutToReplace, frag);
//        if (addToBackstack)
//            ft.addToBackStack("screen_id_" + currentScreenId);

        ft.commitAllowingStateLoss();
    }

    private void replaceContainerWithFrag(Fragment frag, boolean addToBackstack, int animEnter, int animExit, int animEnterPop, int animExitPop, Bundle b,int layoutToReplace) {
        if (b == null)
            b = new Bundle();
        frag.setArguments(b);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(animEnter, animExit, animEnterPop, animExitPop);
        ft.replace(layoutToReplace, frag);

//        if (addToBackstack)
//            ft.addToBackStack("screen_id_" + currentScreenId);
//
        ft.commit();
    }

    private void replaceContainerWithFrag(Fragment frag, boolean addToBackstack, int animEnter, int animExit, int animEnterPop, int animExitPop,int layoutToReplace) {

        replaceContainerWithFrag(frag, addToBackstack, animEnter, animExit, animEnterPop, animExitPop, null,layoutToReplace);

    }


    @Override
    public void onFragmentClicked(String key) {

        final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
        if(key.equals(Constants.CONST_KEY_SEARCH)){

        }else if(key.equals(Constants.CONST_KEY_REG)){
            txt_new_request.performClick();
        }else if(key.equals(Constants.CONST_KEY_POSTREQ)){
            txt_buy.performClick();
        }else if(key.equals(Constants.CONST_KEY_ABOUT)){
            createAboutUsDialog();

        }else if(key.equals(Constants.CONST_KEY_SHARE)){
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT, "Check out TingTong on Google Playstore: https://play.google.com/store/apps/details?id"+appPackageName);
            sendIntent.setType("text/plain");
            startActivity(Intent.createChooser(sendIntent, getResources().getText(R.string.share_with)));

        }else if(key.equals(Constants.CONST_KEY_RATE)){

            try {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
            } catch (android.content.ActivityNotFoundException anfe) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + appPackageName)));
            }

        }else if(key.equals(Constants.CONST_KEY_SEND_FEEDBACK)){
            Intent intent = new Intent (Intent.ACTION_VIEW , Uri.parse("mailto:" + Constants.FEEDBACK_EMAIL));
            intent.putExtra(Intent.EXTRA_SUBJECT, "Feedback for TingTong.");
            //intent.putExtra(Intent.EXTRA_TEXT, "");
            startActivity(intent);

        }else if(key.equals(Constants.CONST_KEY_TANDC)){
            createTermsAndConditionsDialog();
        }else if(key.equals(Constants.CONST_KEY_BUY_LEADS)){
            Intent in=new Intent(this,BuyingLeadsActivity.class);
            startActivity(in);
        }else if(key.equals(Constants.CONST_KEY_FAQ)){
            Intent in=new Intent(this,FAQActivity.class);
            startActivity(in);
        }
        drawerLayout.closeDrawer(Gravity.LEFT);
    }

    private boolean checkPlayServices() {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
 //       return true;
        if (status != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(status)) {
                showErrorDialog(status);
            } else {
                Toast.makeText(this, "This device is not supported.",
                        Toast.LENGTH_LONG).show();
                finish();
            }
            return false;
        }
        return true;
    }

    void showErrorDialog(int code) {
        GooglePlayServicesUtil.getErrorDialog(code, this,
                REQUEST_CODE_RECOVER_PLAY_SERVICES).show();
    }

    static final int REQUEST_CODE_RECOVER_PLAY_SERVICES = 1001;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CODE_RECOVER_PLAY_SERVICES:
                if (resultCode == RESULT_CANCELED) {
                    Toast.makeText(this, "Google Play Services must be installed.",
                            Toast.LENGTH_SHORT).show();
                    finish();
                }
                return;
            case TANDC:{
                if(resultCode==ACCEPT){
                    editor.putBoolean(Constants.PREFS_TAND_ACCEPTED,true);
                    editor.commit();

                }else if(resultCode==REJECT){
                    editor.putBoolean(Constants.PREFS_TAND_ACCEPTED,false);
                    editor.commit();
                    finish();
                }
                break;
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
//
//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        // Check which request we're responding to
//        if (requestCode == PICK_CONTACT_REQUEST) {
//            // Make sure the request was successful
//            if (resultCode == RESULT_OK) {
//                // The user picked a contact.
//                // The Intent's data Uri identifies which contact was selected.
//
//                // Do something with the contact here (bigger example below)
//            }
//        }
//    }

    Toast toast;
    private long back_pressed;
    AlertDialog dialog;
    @Override
    public void onBackPressed() {
        boolean left_drawer, right_drawer;

        if (drawerLayout.isDrawerOpen(Gravity.LEFT)) {
            drawerLayout.closeDrawer(Gravity.LEFT);
            left_drawer = false;
        } else left_drawer = true;
        if (drawerLayout.isDrawerOpen(Gravity.RIGHT)) {
            drawerLayout.closeDrawer(Gravity.RIGHT);
            right_drawer = false;
        } else right_drawer = true;

        if (left_drawer && right_drawer) {
            if (back_pressed + 2000 > System.currentTimeMillis()) {
                finish();
            } else {
                if (toast != null) {
                    toast.cancel();
                }

//                toast = Toast.makeText(this, "Press back again to exit", Toast.LENGTH_SHORT);
             dialog=CustomDialog.createCustomDialog(this,"Are you sure you want to quit?","Yes","No",null,false,new DialogListener() {
                    @Override
                    public void onPositiveBtnClick() {
                        SearchLandingPage.this.finish();
                    }

                    @Override
                    public void onNegativeBtnClick() {
                        return;
                    }

                    @Override
                    public void onMiddleBtnClick() {

                    }
                });
//                toast.show();
            }
            back_pressed = System.currentTimeMillis();
        }else{
            return;
        }
    }
}

