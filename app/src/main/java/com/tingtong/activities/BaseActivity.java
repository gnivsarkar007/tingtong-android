package com.tingtong.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import com.tingtong.App;
import com.tingtong.R;
import com.tingtong.fragments.CompanyListFragment;
import com.tingtong.fragments.MapFrag;
import com.tingtong.fragments.PaidCompanyDescriptionFragment;
import com.tingtong.fragments.PaidCompanyForProductFragment;
import com.tingtong.fragments.ProductDescrFrag;
import com.tingtong.fragments.ProductListFragment;
import com.tingtong.fragments.UnpaidCompanyDescriptionFrag;
import com.tingtong.interfaces.IOnRecieveListChangeListener;

import java.util.LinkedList;
import java.util.List;

import to.done.lib.Constants;
import to.done.lib.entity.ResponseSearchByProductName;
import to.done.lib.entity.ResponseSearchCompanyByName;
import to.done.lib.images.util.ImageFetcher;
import to.done.lib.sync.SyncListener;
import to.done.lib.sync.SyncManager;
import to.done.lib.ui.FontEditText;
import to.done.lib.utils.Toolbox;

/**
 * Created by HP on 10/5/2014.
 */
public abstract class BaseActivity extends ActionBarActivity {

    public static final String TAG = "BaseActivity";
    public int currentScreenId=-1;
    private LocalBroadcastManager localBroadcastManager;
    private View searchLayout=null,titleLayout=null;
    private static ImageFetcher fetcher;
    private List<IOnRecieveListChangeListener> subscribers=new LinkedList<IOnRecieveListChangeListener>();


    public void addListener(IOnRecieveListChangeListener object){
        subscribers.add(object);

    }
    public void removeListener(IOnRecieveListChangeListener object){
        subscribers.remove(object);

    }
    private void sendListChangeEvent(){
        for(IOnRecieveListChangeListener object:subscribers){
            object.listChanged();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        localBroadcastManager=LocalBroadcastManager.getInstance(getApplicationContext());
        localBroadcastManager.registerReceiver(screenChangeReciever,new IntentFilter(Constants.INTENT_SCREEN_UPDATE));
        localBroadcastManager.registerReceiver(actionBarChangeReciever,new IntentFilter(Constants.INTENT_AB_LAYOUT_TYPE));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.img_actionbar_back);
    }

    private void replaceContainerWithFrag(Fragment frag, boolean addToBackstack) {
        replaceContainerWithFrag(frag, addToBackstack, null);
    }

    private void replaceContainerWithFrag(Fragment frag, boolean addToBackstack, Bundle b) {


        if (b != null) {
            frag.setArguments(b);
        }
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.right_to_left_enter,R.anim.right_to_left_exit,R.anim.left_to_right_enter,R.anim.left_to_right_exit);
        ft.replace(R.id.container, frag);
        if (addToBackstack)
            ft.addToBackStack("screen_id_" + currentScreenId);
        ft.commit();
    }

    private void replaceContainerWithFrag(Fragment frag, boolean addToBackstack, int animEnter, int animExit, int animEnterPop, int animExitPop, Bundle b) {
        if (b == null)
            b = new Bundle();
        frag.setArguments(b);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(animEnter, animExit, animEnterPop, animExitPop);
        ft.replace(R.id.container, frag);

        if (addToBackstack)
            ft.addToBackStack("screen_id_" + currentScreenId);
        ft.commit();
    }

    private void replaceContainerWithFrag(Fragment frag, boolean addToBackstack, int animEnter, int animExit, int animEnterPop, int animExitPop) {

        replaceContainerWithFrag(frag, addToBackstack, animEnter, animExit, animEnterPop, animExitPop, null);

    }
    public static ImageFetcher getFetcherInstance(Context context) {

        if (fetcher == null) {
            fetcher = new ImageFetcher(context, 300, 300);
            //        fetcher.setLoadingImage(R.drawable.img_placeholder);
            fetcher.setImageFadeIn(true);
        }
        return fetcher;
    }



    private BroadcastReceiver screenChangeReciever= new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int screen=intent.getIntExtra(Constants.SCREEN_ID,0);
            currentScreenId = screen;
            Bundle bundle=intent.getExtras().getBundle(Constants.BUNDLE_EXTRA);
            boolean addToBackstack=intent.getBooleanExtra(Constants.ADD_TO_BACKSTACK,false);
            Toolbox.writeToLog("" + TAG + " " + addToBackstack + " " + screen + " " + bundle);
            switch (screen){
                case Constants.SCREEN_COMPANY_LIST:{
                    Fragment elFragment= new CompanyListFragment();
                    replaceContainerWithFrag(elFragment,addToBackstack,bundle);
                    //currentScreenId=Constants.SCREEN_ENTITY_LIST;

                }
                break;
                case Constants.SCREEN_COMPANY_DESCR_PAID:
                {
                    Fragment elFragment= new PaidCompanyDescriptionFragment();
                    replaceContainerWithFrag(elFragment,addToBackstack,bundle);
                    //currentScreenId=Constants.SCREEN_COMPANY_DESCR_PAID;
                }
                break;
                case Constants.SCREEN_COMPANY_DESCR_UNPAID:
                {
                    Fragment elFragment= new UnpaidCompanyDescriptionFrag();
                    replaceContainerWithFrag(elFragment,addToBackstack,bundle);
                    //urrentScreenId=Constants.SCREEN_ENTITY_DESCR_UNPAID;
                }
                break;

                case Constants.SCREEN_MAP:
                {
                    Fragment elFragment=new MapFrag();
                    replaceContainerWithFrag(elFragment,addToBackstack,bundle);
                } break;


                case Constants.SCREEN_PRODUCT_DESCR_PAID:
                {
                    Fragment elFragment= new PaidCompanyForProductFragment();
                    replaceContainerWithFrag(elFragment,addToBackstack,bundle);
                    //currentScreenId=Constants.SCREEN_COMPANY_DESCR_PAID;
                }
                break;

                case Constants.SCREEN_PROD_LIST:{
                    Fragment elFragment= new ProductListFragment();
                    replaceContainerWithFrag(elFragment,addToBackstack,bundle);
                }
                break;

                case Constants.SCREEN_PROD_DESCR:{
                    Fragment elFragment= new ProductDescrFrag();
                    replaceContainerWithFrag(elFragment,addToBackstack,bundle);
                }
                break;

                default:break;
            }
        }
    };

    private BroadcastReceiver actionBarChangeReciever=new BroadcastReceiver() {
        @Override
        public void onReceive(final Context context, Intent intent) {

            int actionbarLayoutType=intent.getIntExtra(Constants.AB_LAYOUT_TYPE,101);
            final boolean isCompanyPage=intent.getBooleanExtra(Constants.SCREEN_ID,true);
            Toolbox.writeToLog("Actionbar layout type switch :"+actionbarLayoutType);
            switch (actionbarLayoutType){
                case Constants.AB_LAYOUT_NORMAL:{
                    if(titleLayout==null)
                        titleLayout= LayoutInflater.from(context).inflate(R.layout.ab_title_layout, null);
                    ImageView imgBack= (ImageView) titleLayout.findViewById(R.id.img_actionbar_back);
                    imgBack.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            backPressHandle();
                        }
                    });

                }
                getSupportActionBar().setCustomView(titleLayout);
                getSupportActionBar().setDisplayShowCustomEnabled(true);
                break;


                case Constants.AB_LAYOUT_SEARCH:{
                    if(searchLayout==null)
                        searchLayout=LayoutInflater.from(context).inflate(R.layout.ab_search_layout,null);

                    final FontEditText edtSearch= (FontEditText) searchLayout.findViewById(R.id.edt_search_bar);
                    ImageView imgSearch= (ImageView) searchLayout.findViewById(R.id.img_actionbar_search);
                    imgSearch.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if(edtSearch.getVisibility()==View.INVISIBLE){
                                edtSearch.setVisibility(View.VISIBLE);
                            }else {
                                if (edtSearch.getText() == null || edtSearch.getText().toString().equals("") || edtSearch.getText().toString().length() == 0) {
                                    edtSearch.setVisibility(View.INVISIBLE);
                                    Toolbox.showToastShort(context, "Search string cannot be empty.");
                                }else{
                                    edtSearch.setVisibility(View.INVISIBLE);
                                    try {
                                        if(isCompanyPage)
                                        SyncManager.getCompanyByName(edtSearch.getText().toString(), queryCompanyByName, BaseActivity.this, App.getDatabaseInstance(BaseActivity.this));
                                        else
                                        SyncManager.getProductByName(edtSearch.getText().toString(), queryProductByName, BaseActivity.this, App.getDatabaseInstance(BaseActivity.this));

                                        edtSearch.getText().clear();
                                    }catch (Exception e){
                                        e.printStackTrace();
                                    }
                                }
                            }

                        }
                    });
                    getSupportActionBar().setCustomView(searchLayout);
                    getSupportActionBar().setDisplayShowCustomEnabled(true);

                }
                break;
            }
        }


    };


    private SyncListener queryCompanyByName= new SyncListener() {
        @Override
        public void onSyncStart() {
            beginRequest();
        }

        @Override
        public void onSyncProgress(float percentProgress, long requestTimestamp) {

        }

        @Override
        public void onSyncSuccess(String url, Object responseObject, long requestTimestamp) {
            ResponseSearchCompanyByName resp=(ResponseSearchCompanyByName)responseObject;
            finishRequest(resp.getResponseMsg());
            sendListChangeEvent();

        }

        @Override
        public void onSyncFailure(String url, String reason, long requestTimestamp) {
            finishRequest(reason);
            sendListChangeEvent();
        }
    };


    private SyncListener queryProductByName= new SyncListener() {
        @Override
        public void onSyncStart() {
            beginRequest();
        }

        @Override
        public void onSyncProgress(float percentProgress, long requestTimestamp) {

        }

        @Override
        public void onSyncSuccess(String url, Object responseObject, long requestTimestamp) {
            ResponseSearchByProductName resp=(ResponseSearchByProductName)responseObject;
            finishRequest(resp.getResponseMsg());
            sendListChangeEvent();

        }

        @Override
        public void onSyncFailure(String url, String reason, long requestTimestamp) {
            finishRequest(reason);
            sendListChangeEvent();
        }
    };
    public abstract void beginRequest();

    public abstract void finishRequest(String message);

    public abstract void backPressHandle();

    @Override
    protected void onDestroy() {
        super.onDestroy();
        localBroadcastManager.unregisterReceiver(screenChangeReciever);
        localBroadcastManager.unregisterReceiver(actionBarChangeReciever);

    }
}
