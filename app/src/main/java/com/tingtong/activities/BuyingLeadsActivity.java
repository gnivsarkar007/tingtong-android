package com.tingtong.activities;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;

import com.tingtong.App;
import com.tingtong.R;
import com.tingtong.adapters.BuyingLeadsAdapter;

import butterknife.ButterKnife;
import butterknife.InjectView;
import to.done.lib.database.DBManager;
import to.done.lib.entity.ResponseBuyLeads;
import to.done.lib.sync.SyncListener;
import to.done.lib.sync.SyncManager;
import to.done.lib.utils.Toolbox;

/**
 * Created by gauravnivsarkar on 11/28/14.
 */
public class BuyingLeadsActivity extends ActionBarActivity implements LoaderManager.LoaderCallbacks {
    private static final int LIST_ID=0;

    @InjectView(R.id.list_buying_leads)
    ListView buying_leads_list;
    @InjectView(R.id.edt_filter_textbox)
    EditText searchBox;
    @InjectView(R.id.img_close)
    ImageView close;
    BuyingLeadsAdapter leadsAdapter;
    DBManager dbManager;
    ProgressDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.buying_leads_activity);
        ButterKnife.inject(this);
        getSupportActionBar().hide();
        pDialog=new ProgressDialog(this);
        pDialog.setMessage("Fetching details...");
        pDialog.setCancelable(false);
        SyncManager.getBuyingLeads(getLeads,this, App.getDatabaseInstance(this));
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BuyingLeadsActivity.this.finish();
            }
        });
        searchBox.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                if(leadsAdapter!=null) leadsAdapter.getFilter().filter(charSequence);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    public void startRequest(){
        if(!pDialog.isShowing())
            pDialog.show();
    }
    public void finishRequest(String reason){
        if(reason!=null)
        Toolbox.showToastShort(this,reason);
        pDialog.dismiss();
    }

    SyncListener getLeads=new SyncListener() {
        @Override
        public void onSyncStart() {
                  startRequest();
        }

        @Override
        public void onSyncProgress(float percentProgress, long requestTimestamp) {

        }

        @Override
        public void onSyncSuccess(String url, Object responseObject, long requestTimestamp) {
            ResponseBuyLeads resp= (ResponseBuyLeads) responseObject;
            finishRequest(null);
            if(leadsAdapter==null) {
                leadsAdapter = new BuyingLeadsAdapter(BuyingLeadsActivity.this);
                buying_leads_list.setAdapter(leadsAdapter);
            }
            else{
                leadsAdapter.setLeads();
            }

        }

        @Override
        public void onSyncFailure(String url, String reason, long requestTimestamp) {
        finishRequest(reason);
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public Loader onCreateLoader(int id, Bundle args) {
        return null;
    }

    @Override
    public void onLoadFinished(Loader loader, Object data) {

    }

    @Override
    public void onLoaderReset(Loader loader) {

    }
}
