package com.tingtong.adapters;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.tingtong.fragments.CompanyAboutUs;
import com.tingtong.fragments.CompanyContactDetails;

import to.done.lib.Constants;

/**
 * Created by HP on 8/16/2014.
 */
public class DescriptionPagerAdapter extends FragmentPagerAdapter {
    Activity context;
    private String[] mTitles={ "About", "Contact Us" };
    private Long company_id;
    public DescriptionPagerAdapter(FragmentManager fm,Activity context,Long company_id) {
        super(fm);
        this.context=context;
        this.company_id=company_id;
       // Toolbox.showToastShort(this.context,"DescriptionPagerAdapter");

    }

    @Override
    public CharSequence getPageTitle(int position) {
      //  Toolbox.showToastShort(context,"getPageTitle"+position);
        return mTitles[position];
    }


    @Override
    public Fragment getItem(int position) {
        //Toolbox.showToastShort(context,"getItem"+position);
        Fragment f=null;
        Bundle b= new Bundle();
        switch (position){
            case 0:
                b.putLong(Constants.COMPANY_ID,company_id);
                f= new CompanyAboutUs();
                f.setArguments(b);
                break;

            case 1:
                b.putLong(Constants.COMPANY_ID,company_id);
                f=new CompanyContactDetails();
                f.setArguments(b);
                break;
            }
        return f;
    }

    @Override
    public int getCount() {
        //Toolbox.showToastShort(context,"getCount");
        return mTitles.length;
    }
}
