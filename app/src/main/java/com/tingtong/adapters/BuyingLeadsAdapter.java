package com.tingtong.adapters;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.tingtong.App;
import com.tingtong.R;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import butterknife.ButterKnife;
import butterknife.InjectView;
import to.done.lib.database.DBManager;
import to.done.lib.entity.BuyingLead;

/**
 * Created by gauravnivsarkar on 11/29/14.
 */
public class BuyingLeadsAdapter extends BaseAdapter implements Filterable{
    Context mContext;
    DBManager dbMan;
    List<BuyingLead>leads;
    Dialog detailsDialog;
    TextView txt_prodname,txt_prodQty,txt_prodCountry,txt_prodPayTerms;
    public BuyingLeadsAdapter(Activity context){
        mContext=context;
        dbMan= App.getDatabaseInstance(context);
        leads=new ArrayList<BuyingLead>();
        if(dbMan.getBuyLeads()!=null)
        leads.addAll(dbMan.getBuyLeads());
    }


    public List<BuyingLead> getLeads() {
        return leads;
    }

    public void setLeads() {
        this.leads = dbMan.getBuyLeads();
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return leads.size();
    }

    @Override
    public BuyingLead getItem(int i) {
        return leads.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        if(view==null){
            view= LayoutInflater.from(mContext).inflate(R.layout.buy_leads_text,null);
            holder=new ViewHolder(view);
            view.setTag(holder);

        }else holder= (ViewHolder) view.getTag();
        holder.lead.setText(getItem(i).getProductName());
        holder.lead.setTag(getItem(i));
        holder.lead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BuyingLead lead= (BuyingLead) view.getTag();
                createDescriptionDialog(lead);
            }
        });

        return view;
    }

    @Override
    public Filter getFilter() {
        return filter;
    }

    private Filter filter=new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence charSequence) {
            FilterResults results=new FilterResults();
            if(charSequence==null || charSequence.length()==0 ){
                results.values=dbMan.getBuyLeads();
                results.count=dbMan.getBuyLeads().size();
            }else{
                Set<BuyingLead> leadSet=new HashSet<BuyingLead>();
               //
                for(BuyingLead lead:leads){
                    if(lead.getProductName().toLowerCase().startsWith(charSequence.toString().toLowerCase())){
                        leadSet.add(lead);
                    }
                    if(lead.getProductName().toLowerCase().endsWith(charSequence.toString().toLowerCase())){
                        leadSet.add(lead);
                    }
                    if(lead.getProductName().toLowerCase().contains(charSequence.toString().toLowerCase())){
                        leadSet.add(lead);
                    }
                }
                List<BuyingLead>filteresLeads=new ArrayList<BuyingLead>(leadSet);
                results.values= filteresLeads;
                results.count=filteresLeads.size();
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
            if(filterResults.count==0) notifyDataSetInvalidated();
            else {
                leads= (List<BuyingLead>) filterResults.values;
                notifyDataSetChanged();
            }
        }
    };

    static class ViewHolder{
        @InjectView(R.id.txt_lead)
        TextView lead;
        public ViewHolder(View v){
            ButterKnife.inject(this, v);
        }

    }

    private void createDescriptionDialog(BuyingLead lead){
       if(lead==null) return;
     //   WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        if(detailsDialog==null)
        {
            detailsDialog=new Dialog(mContext);
            detailsDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            detailsDialog.setContentView(R.layout.buy_leads_descr_layout);
            detailsDialog.setCancelable(true);
            txt_prodname= (TextView) detailsDialog.findViewById(R.id.txt_prod_name);
            txt_prodQty= (TextView) detailsDialog.findViewById(R.id.txt_qty);
            txt_prodCountry= (TextView) detailsDialog.findViewById(R.id.txt_pay_terms);
            txt_prodPayTerms= (TextView) detailsDialog.findViewById(R.id.txt_country);
            detailsDialog.setCanceledOnTouchOutside(true);
            detailsDialog.setCancelable(true);

        }
        if(lead.getProductName()!=null)
        txt_prodname.setText(lead.getProductName());
        else txt_prodname.setText(R.string.txt_not_available);
        if(lead.getProductQty()!=null)
        txt_prodQty.setText(lead.getProductQty());
        else txt_prodQty.setText(R.string.txt_not_available);
        if(lead.getPortOfDelivery()!=null)
        txt_prodCountry.setText(lead.getPaymentTerm());
        else txt_prodCountry.setText(R.string.txt_not_available);
        if(lead.getPaymentTerm()!=null)
        txt_prodPayTerms.setText(lead.getPortOfDelivery());
        else txt_prodPayTerms.setText(R.string.txt_not_available);
        detailsDialog.show();

    }


}
