package com.tingtong.adapters;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.tingtong.R;
import com.tingtong.activities.CompanyListDisplayActivity;
import com.tingtong.activities.ProductListDisplayActivity;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.ButterKnife;
import butterknife.InjectView;
import to.done.lib.Constants;
import to.done.lib.entity.Product;
import to.done.lib.entity.UserEnquiryRequest;
import to.done.lib.entity.UserEnquiryResp;
import to.done.lib.images.util.ImageFetcher;
import to.done.lib.sync.SyncListener;
import to.done.lib.sync.SyncManager;
import to.done.lib.ui.CustomDialog;
import to.done.lib.ui.DialogListener;
import to.done.lib.utils.Toolbox;

/**
 * Created by HP on 7/29/2014.
 */
public class ImageGridAdapter extends BaseAdapter{
    List<Product> productsList;
    Activity context;
    LayoutInflater inflater;
    ImageFetcher fetcher;
    CompanyListDisplayActivity compAct=null;
    ProductListDisplayActivity prodAct=null;
    private Pattern pattern;
    private Matcher matcher;
    ProgressDialog pDialog;
    UserEnquiryRequest req;
    public List<Product> getProductsList() {
        return productsList;
    }

    public void setProductsList(List<Product> productsList) {
        this.productsList = productsList;
    }

    public ImageGridAdapter(List<Product>prods,Activity ctx){
        context=ctx;
        Toolbox.writeToLog("Size of products here "+prods.size());

        productsList=new ArrayList<Product>(prods);

        inflater=LayoutInflater.from(context);
        pDialog=new ProgressDialog(context);
        pDialog.setMessage("Sending enquiry...");
        pDialog.setCancelable(false);
        pattern = Pattern.compile(Constants.EMAIL_PATTERN);

        if(ctx instanceof CompanyListDisplayActivity)
        fetcher= CompanyListDisplayActivity.getFetcherInstance(context);
        else if(ctx instanceof  ProductListDisplayActivity)
            fetcher= ProductListDisplayActivity.getFetcherInstance(context);
    }
    @Override
    public int getCount() {
        if(productsList!=null)
        return productsList.size();
        return 0;
    }

    @Override
    public Product getItem(int i) {
        if(productsList!=null)
            return productsList.get(i);
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
      ViewHolder holder;
       final int k=i;
        if(view==null){
            view=inflater.inflate(R.layout.grid_element,null);
            holder= new ViewHolder(view);
            view.setTag(holder);
        }else {
            holder = (ViewHolder) view.getTag();
        }
        Toolbox.writeToLog("ImageGridAdapter ","Product "+getItem(i).getName()+" URL "+getItem(i).getImageUrl());
        if(getItem(i).getImageUrl()!=null)
        fetcher.loadImage(getItem(i).getImageUrl(),holder.imgProductImage);

        holder.imgProductImage.setTag(getItem(i));
        holder.imgProductImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Product p= (Product) view.getTag();
                if(p.getImageUrl()!=null)
                Constants.createDialogWithImage(context,R.layout.layout_image_dialog,
                        R.id.img_to_show,R.id.txt_entity_name,
                        R.id.txt_entity_descr,R.id.btn_send
                        ,getItem(k).getImageUrl(),
                        getItem(k).getName(),getItem(k).getDescription(),fetcher,true,sednenq,p);
                else Toolbox.showToastShort(context,"No image available");
            }
        });
        return view;
    }
    private View.OnClickListener sednenq=new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Product pr= (Product) view.getTag();
            createSendEnquiryDialog(pr);
        }
    };

    private void createSendEnquiryDialog(final Product p){
        String email=Constants.emailsFromDevice(context).get(0);
        final Dialog smDialog= new Dialog(context);
        smDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        smDialog.setContentView(R.layout.send_enquiry_dialog);
        final ImageView imgClose= (ImageView) smDialog.findViewById(R.id.img_close);
        final TextView txtSendEnquiry= (TextView) smDialog.findViewById(R.id.txt_send_enq);
        final EditText edtName= (EditText) smDialog.findViewById(R.id.edt_name);
        final EditText edtEmail= (EditText) smDialog.findViewById(R.id.edt_email);
        if(email!=null)
            edtEmail.setText(email);
        final EditText edtMobile= (EditText) smDialog.findViewById(R.id.edt_mobile);
        final EditText edtSubject= (EditText) smDialog.findViewById(R.id.edt_subject);
        final EditText edtMessage= (EditText) smDialog.findViewById(R.id.edt_msg);
        txtSendEnquiry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(validateField(edtName)&&validateField(edtEmail)&&validateField(edtMobile)&&validateField(edtSubject)&&validateField(edtMessage)){
                    Toolbox.showToastShort(context,"Request sent succesfully");
                    req= new UserEnquiryRequest();
                    req.setUserSubject(edtSubject.getText().toString());
                    req.setUserEmail(edtEmail.getText().toString());
                    req.setUserMobile(edtMobile.getText().toString());
                    req.setUserMsg(edtMessage.getText().toString());
                    req.setCompanyId(String.valueOf(p.getCompanyId()));
                    req.setUserName(edtName.getText().toString());
                    req.setProd_name(p.getName());
                    SyncManager.sendUserEnquiry(req, enquiryListener, context, null);
                    smDialog.dismiss();
                }
            }
        });
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                smDialog.dismiss();
            }
        });
        smDialog.show();
    }
    private SyncListener enquiryListener= new SyncListener() {
        @Override
        public void onSyncStart() {
            pDialog.show();
        }

        @Override
        public void onSyncProgress(float percentProgress, long requestTimestamp) {

        }

        @Override
        public void onSyncSuccess(String url, Object responseObject, long requestTimestamp) {
            if(pDialog.isShowing())
                pDialog.dismiss();
            UserEnquiryResp resp= (UserEnquiryResp) responseObject;
            CustomDialog.createCustomDialog(context, resp.getResponseMsg(), "OK", null, null, true, null);
        }

        @Override
        public void onSyncFailure(String url, String reason, long requestTimestamp) {
            if(pDialog.isShowing())
                pDialog.dismiss();

            CustomDialog.createCustomDialog(context,reason,"Retry??",null,null,true,new DialogListener() {
                @Override
                public void onPositiveBtnClick() {
                    SyncManager.sendUserEnquiry(req,enquiryListener,context,null);
                }

                @Override
                public void onNegativeBtnClick() {

                }

                @Override
                public void onMiddleBtnClick() {

                }
            });
        }
    };

    static class ViewHolder{
        @InjectView(R.id.img_prod)
        ImageView imgProductImage;
        public ViewHolder(View v){
            ButterKnife.inject(this, v);
        }
    }

    private boolean validateField(EditText edtField) {
        String text = edtField.getText().toString();
        switch (edtField.getId()) {
            case R.id.edt_email: {
                if (text == null || text.equals("") || text.length() == 0) {
                    edtField.setError("This field cannot be blank!!");
                    return false;
                } else {
                    matcher=pattern.matcher(text);
                    if (matcher.matches()) {

                        edtField.setError(null);
                        return true;
                    } else {
                        edtField.setError("Please enter valid email!!");
                        return false;
                    }

                }
            }

            case R.id.edt_mobile: {
                if (text == null || text.equals("") || text.length() == 0 || text.length() < 10) {
                    edtField.setError("This field cannot be blank!!");
                    return false;
                }
                else if(!StringUtils.isNumeric(text)){
                    edtField.setError("Please enter valid number!!");
                    return false;

                }
                else {
                    edtField.setError(null);
                    return true;
                }
            }


            default: {
                if (text == null || text.equals("") || text.length() == 0) {
                    edtField.setError("This field cannot be blank!!");
                    return false;
                } else {
                    edtField.setError(null);
                    return true;
                }
            }

        }

    }
}
