package com.tingtong.adapters;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.tingtong.App;
import com.tingtong.R;
import com.tingtong.activities.ProductListDisplayActivity;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.ButterKnife;
import butterknife.InjectView;
import to.done.lib.Constants;
import to.done.lib.database.DBManager;
import to.done.lib.entity.Companydetail;
import to.done.lib.entity.Product;
import to.done.lib.entity.UserEnquiryRequest;
import to.done.lib.entity.UserEnquiryResp;
import to.done.lib.sync.SyncListener;
import to.done.lib.sync.SyncManager;
import to.done.lib.ui.CustomDialog;
import to.done.lib.ui.DialogListener;
import to.done.lib.utils.Toolbox;

/**
 * Created by HP on 8/10/2014.
 */
public class ProductListAdapter extends BaseAdapter {
    private Activity context;
    private List<Product> products;
    private LayoutInflater inflater;
    private DBManager dbManager;
    private HashMap<Long,Companydetail>companiesById;
    Pattern pattern;
    Matcher matcher;
    ProgressDialog pDialog;
    UserEnquiryRequest req;
    public ProductListAdapter(Activity ctx,List<Product>cmps){
        context=ctx;
        pDialog=new ProgressDialog(context);
        pDialog.setMessage("Sending enquiry...");
        pDialog.setCancelable(false);
        pattern = Pattern.compile(Constants.EMAIL_PATTERN);

        products= new ArrayList<Product>(cmps);
        inflater=LayoutInflater.from(context);
        dbManager= App.getDatabaseInstance(context);
        companiesById=new HashMap<Long,Companydetail>();
        for(Product p:products){
            Companydetail cmp=dbManager.getCompanyById(p.getCompanyId());
            companiesById.put(cmp.getId(),cmp);
        }

    }
    @Override
    public int getCount() {
        if(products!=null)
            return products.size();
        return 0;
    }

    @Override
    public Product getItem(int i) {
        if(products!=null)
            return products.get(i);
        return null;
    }

    public Activity getContext() {
        return context;
    }

    public void setContext(Activity context) {
        this.context = context;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {

        this.products = products;
        companiesById.clear();
        for(Product p:products){
            Companydetail cmp=dbManager.getCompanyById(p.getCompanyId());
            companiesById.put(cmp.getId(),cmp);
        }
    }

    public HashMap<Long, Companydetail> getCompaniesById() {
        return companiesById;
    }

    public void setCompaniesById(HashMap<Long, Companydetail> companiesById) {
        this.companiesById = companiesById;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        final Product prod=getItem(i);
        Companydetail companydetail=companiesById.get(prod.getCompanyId());
        ViewHolder holder;
        if(view==null){
            view=inflater.inflate(R.layout.product_list_item,null);
            holder= new ViewHolder(view);
            view.setTag(holder);
        }else{
            holder= (ViewHolder)view.getTag();
        }

        holder.txtProdName.setText(prod.getName());
        holder.txtCompanyName.setText(companydetail.getName());
        holder.txtCompanyCity.setText(companydetail.getAddress().getCity());
        holder.txtCompanyPhone.setText(getPhoneNo(companydetail));

        if(companydetail.getPaymentStatus().toLowerCase().equals("paid")){
            holder.imgCompanyPayStatus.setVisibility(View.VISIBLE);
        }else holder.imgCompanyPayStatus.setVisibility(View.INVISIBLE);
//        if(companydetail.getLinks()!=null && companydetail.getLinks().size()>0){
//            holder.imgCompanyCompAppURL.setImageResource(R.drawable.img_download_available);
//        }else{
//            holder.imgCompanyCompAppURL.setImageResource(R.drawable.img_download_unavailable);
//        }
        if(prod.getImageUrl()!=null){
            ProductListDisplayActivity.getFetcherInstance(context).loadImage(prod.getImageUrl(),holder.imgCompanyLogo);
        }else{
            prod.setImageUrl(Constants.SPLASH_URL);
            ProductListDisplayActivity.getFetcherInstance(context).loadImage(prod.getImageUrl(),holder.imgCompanyLogo);
        }
        holder.txtCompanyEnquiry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Companydetail com= (Companydetail) view.getTag();
                createSendEnquiryDialog(com.getId(),prod.getName());
            }
        });
        holder.txtProdName.setTag(prod);
        holder.imgCompanyLogo.setTag(prod);
        holder.txtCompanyEnquiry.setTag(companydetail);
        holder.txtProdName.setOnClickListener(prodPageChngeListener);
        holder.imgCompanyLogo.setOnClickListener(prodPageChngeListener);
        return view;
    }

    private View.OnClickListener prodPageChngeListener=new View.OnClickListener() {
        @Override
        public void onClick(View view) {
        Product prod= (Product) view.getTag();
            Bundle b=new Bundle();
            b.putLong(Constants.PRODUCT_ID,prod.getId());
            Toolbox.changeScreen(context,Constants.SCREEN_PROD_DESCR,true,b);
        }
    };

    private void createSendEnquiryDialog(final Long comp_id, final String prod_id){
        String email=Constants.emailsFromDevice(context).get(0);
        final Dialog smDialog= new Dialog(context);
        smDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        smDialog.setContentView(R.layout.send_enquiry_dialog);
        final ImageView imgClose= (ImageView) smDialog.findViewById(R.id.img_close);
        final TextView txtSendEnquiry= (TextView) smDialog.findViewById(R.id.txt_send_enq);
        final EditText edtName= (EditText) smDialog.findViewById(R.id.edt_name);
        final EditText edtEmail= (EditText) smDialog.findViewById(R.id.edt_email);
        if(email!=null)
            edtEmail.setText(email);
        final EditText edtMobile= (EditText) smDialog.findViewById(R.id.edt_mobile);
        final EditText edtSubject= (EditText) smDialog.findViewById(R.id.edt_subject);
        final EditText edtMessage= (EditText) smDialog.findViewById(R.id.edt_msg);
        txtSendEnquiry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(validateField(edtName)&&validateField(edtEmail)&&validateField(edtMobile)&&validateField(edtSubject)&&validateField(edtMessage)){
                    Toolbox.showToastShort(context, context.getString(R.string.succesful_request));
                    req= new UserEnquiryRequest();
                    req.setUserSubject(edtSubject.getText().toString());
                    req.setUserEmail(edtEmail.getText().toString());
                    req.setUserMobile(edtMobile.getText().toString());
                    req.setUserMsg(edtMessage.getText().toString());
                    req.setCompanyId(String.valueOf(comp_id));
                    req.setUserName(edtName.getText().toString());
                    req.setProd_name(prod_id.toString());
                    SyncManager.sendUserEnquiry(req, enquiryListener, context, null);
                    smDialog.dismiss();
                }
            }
        });
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                smDialog.dismiss();
            }
        });
        smDialog.show();
    }
    private SyncListener enquiryListener= new SyncListener() {
        @Override
        public void onSyncStart() {
            pDialog.show();
        }

        @Override
        public void onSyncProgress(float percentProgress, long requestTimestamp) {

        }

        @Override
        public void onSyncSuccess(String url, Object responseObject, long requestTimestamp) {
            if(pDialog.isShowing())
                pDialog.dismiss();
            UserEnquiryResp resp= (UserEnquiryResp) responseObject;
            CustomDialog.createCustomDialog(context, resp.getResponseMsg(), "OK", null, null, true, null);
        }

        @Override
        public void onSyncFailure(String url, String reason, long requestTimestamp) {
            if(pDialog.isShowing())
                pDialog.dismiss();

            CustomDialog.createCustomDialog(context,reason,"Retry??",null,null,true,new DialogListener() {
                @Override
                public void onPositiveBtnClick() {
                    SyncManager.sendUserEnquiry(req,enquiryListener,context,null);
                }

                @Override
                public void onNegativeBtnClick() {

                }

                @Override
                public void onMiddleBtnClick() {

                }
            });
        }
    };

    SyncListener retryListener=new SyncListener() {
        @Override
        public void onSyncStart() {
            pDialog.show();
        }

        @Override
        public void onSyncProgress(float percentProgress, long requestTimestamp) {

        }

        @Override
        public void onSyncSuccess(String url, Object responseObject, long requestTimestamp) {
            if(pDialog.isShowing())
                pDialog.dismiss();
            UserEnquiryResp resp= (UserEnquiryResp) responseObject;
            CustomDialog.createCustomDialog(context,resp.getResponseMsg(),"OK",null,null,true,null);

        }

        @Override
        public void onSyncFailure(String url, String reason, long requestTimestamp) {
            CustomDialog.createCustomDialog(context,reason,"Done",null,null,true,null);

        }
    };



    private boolean validateField(EditText edtField) {
        String text = edtField.getText().toString();
        switch (edtField.getId()) {
            case R.id.edt_email: {
                if (text == null || text.equals("") || text.length() == 0) {
                    edtField.setError("This field cannot be blank!!");
                    return false;
                } else {
                    matcher=pattern.matcher(text);
                    if (matcher.matches()) {

                        edtField.setError(null);
                        return true;
                    } else {
                        edtField.setError("Please enter valid email!!");
                        return false;
                    }

                }
            }

            case R.id.edt_mobile: {
                if (text == null || text.equals("") || text.length() == 0 || text.length() < 10) {
                    edtField.setError("This field cannot be blank!!");
                    return false;
                }
                else if(!StringUtils.isNumeric(text)){
                    edtField.setError("Please enter valid phone number!!");
                    return false;
                }
                else {
                    edtField.setError(null);
                    return true;
                }
            }


            default: {
                if (text == null || text.equals("") || text.length() == 0) {
                    edtField.setError("This field cannot be blank!!");
                    return false;
                } else {
                    edtField.setError(null);
                    return true;
                }
            }

        }

    }



static class ViewHolder{
        @InjectView(R.id.img_company_image)
        ImageView imgCompanyLogo;
        @InjectView(R.id.txt_prod_name)
        TextView txtProdName;
        @InjectView(R.id.txt_cmpny_name)
        TextView txtCompanyName;
        @InjectView(R.id.txt_cmpny_enquiry)
        TextView txtCompanyEnquiry;
        @InjectView(R.id.txt_comp_city)
        TextView txtCompanyCity;
        @InjectView(R.id.txt_comp_phn)
        TextView txtCompanyPhone;
        @InjectView(R.id.img_company_pay_status)
        ImageView imgCompanyPayStatus;
        @InjectView(R.id.img_company_app_url)
        ImageView imgCompanyCompAppURL;
        public ViewHolder(View v){
            ButterKnife.inject(this, v);
        }
    }

    private String getPhoneNo(Companydetail com) {
        if (com.getCallnos() != null){
            String[] nos = com.getCallnos().split(",");
            if (nos != null && nos.length > 0)
                return nos[0];
            else return "";
        }else return "";
    }

}
