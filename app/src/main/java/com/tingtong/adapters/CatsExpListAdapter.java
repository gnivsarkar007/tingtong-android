package com.tingtong.adapters;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.tingtong.App;
import com.tingtong.R;
import com.tingtong.activities.CompanyListDisplayActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import to.done.lib.database.DBManager;
import to.done.lib.entity.Category;
import to.done.lib.sync.SyncListener;
import to.done.lib.sync.SyncManager;

/**
 * Created by HP on 9/21/2014.
 */
public class CatsExpListAdapter extends BaseExpandableListAdapter {
    List<Category>parents;
    List<Category>children;
    HashMap<Long,List<Category>>childrenWithParent;
    Activity context;
    DBManager dbManager;
    LayoutInflater inflater;
    ProgressDialog pDialog;

    public CatsExpListAdapter(Activity ctx){
        context=ctx;
        dbManager= App.getDatabaseInstance(context);
        parents=new ArrayList<Category>(dbManager.getParentCategories());
        children=new ArrayList<Category>();
        childrenWithParent=new HashMap<Long,List<Category>>();
        for(Category c:parents){
            childrenWithParent.put(c.getId(),dbManager.getChildCategories(c.getId()));
        }
        inflater=LayoutInflater.from(context);
        pDialog=new ProgressDialog(context);
        pDialog.setMessage("Getting companies...");
        pDialog.setCancelable(false);
    }
    @Override
    public int getGroupCount() {
        if(parents!=null) return parents.size();
        return 0;
    }

    @Override
    public int getChildrenCount(int i) {
       Long key=getGroup(i).getId();
        int size=childrenWithParent.get(key).size();
        return size;
    }

    @Override
    public Category getGroup(int i) {
        return parents.get(i);
    }

    @Override
    public Category getChild(int i, int i2) {
        Long key=getGroup(i).getId();
        Category child=childrenWithParent.get(key).get(i2);
        return child;
    }

    @Override
    public long getGroupId(int i) {
        return 0;
    }

    @Override
    public long getChildId(int i, int i2) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int i, boolean b, View view, ViewGroup viewGroup) {
        ViewGroupParent holder=null;
        if(view==null){
            view=inflater.inflate(R.layout.cat_group_element,null);
            holder= new ViewGroupParent(view);
            view.setTag(holder);
        }else{
            holder= (ViewGroupParent) view.getTag();
        }
        holder.parent.setText(getGroup(i).getCategoryName());
        return view;
    }

    @Override
    public View getChildView(int i, int i2, boolean b, View view, ViewGroup viewGroup) {
        ViewGroupChild holder=null;
        if(view==null){
            view=inflater.inflate(R.layout.cat_child_element,null);
            holder= new ViewGroupChild(view);
            view.setTag(holder);
        }else{
            holder= (ViewGroupChild) view.getTag();
        }
        holder.child.setTag(getChild(i,i2));
        holder.child.setText(getChild(i,i2).getCategoryName());
        holder.child.setOnClickListener(childClick);
        return view;
    }

    View.OnClickListener childClick=new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Category cat= (Category) view.getTag();
            SyncManager.getCompanyByCategory(cat.getId(),getComps,context,dbManager);
        }
    };

    SyncListener getComps=new SyncListener() {
        @Override
        public void onSyncStart() {
                pDialog.show();
        }

        @Override
        public void onSyncProgress(float percentProgress, long requestTimestamp) {

        }

        @Override
        public void onSyncSuccess(String url, Object responseObject, long requestTimestamp) {
            pDialog.dismiss();
            Intent in=new Intent(context, CompanyListDisplayActivity.class);
            context.startActivity(in);
        }

        @Override
        public void onSyncFailure(String url, String reason, long requestTimestamp) {
            pDialog.dismiss();
            Intent in=new Intent(context, CompanyListDisplayActivity.class);
            context.startActivity(in);
        }
    };

    @Override
    public boolean isChildSelectable(int i, int i2) {
        return true;
    }

static class ViewGroupParent{
    @InjectView(R.id.txt_cat_parent)
    TextView parent;
    ViewGroupParent(View v){
        ButterKnife.inject(this, v);
    }
}
    static class ViewGroupChild{
        @InjectView(R.id.txt_cat_child)
        TextView child;
        ViewGroupChild(View v){
            ButterKnife.inject(this, v);
        }
    }


}
